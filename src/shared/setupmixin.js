import { status, configMode } from "@/shared/define";
import { GetObjArr, GetObjVal, GetLastUpdate, ObjCopyValue, GetObjValJoin } from "@/shared/utils";
export default {
    data() {
        return {
            status: { lists: status, current: status.filter(v => v.value != 'C'), selected: [status[0]] },
            mode: "Add",
            modal: false,
            modalDel: false,
            data: [],
            saveData: [],
            errMsg: '',
            configMode: configMode
        }
    },
    computed: {
        config() {
            return configMode[this.mode].class;
        },
        savedMsg() {
            return this.errMsg ? this.errMsg : this.$i18n.messages[this.$i18n.locale]['dic']['saved'];
        },
        deletedMsg() {
            return this.errMsg ? this.errMsg : this.$i18n.messages[this.$i18n.locale]['dic']['deleted'];
        },
        statusFilter() {
            return GetObjArr(this.status.selected, 'value');
        }
    },
    methods: {
        GetStatus(val) {
            return GetObjVal(this.status.lists.find(d => d.value === val), 'text');
        },
        Saved() {
            this.$root.AlertMessage(this.errMsg ? "error" : "success", this.savedMsg);
            this.modal = this.errMsg != '';
            return this.errMsg == '';
        },
        Deleted() {
            this.$root.AlertMessage(this.errMsg ? "error" : "success", this.deletedMsg);
            return this.errMsg == '';
        },
        SetErrorMsg(res, fields, gendata) {
            this.errMsg = "";
            if (Array.isArray(res)) {
                res.forEach((d, i) => {
                    try {
                        if (typeof gendata === 'function') gendata(d);
                        else this.GenData(d);
                    } catch (e) { };
                    if (d._result._status == "F") {
                        this.errMsg = this.errMsg.concat("#", (i + 1), " ", GetObjValJoin(d, fields, ' | '), ' : ', d._result._message, '\n');
                    }
                });
            }
        },
        GenData(x) {
            x.lastupdate = GetLastUpdate(x);
        },
        SetData(res, data, fields, remove, gendata) {
            this.SetErrorMsg(res, fields, gendata);
            if (remove) this.$root.AlertMessage(this.errMsg ? "error" : "success", this.deletedMsg);
            else this.$root.AlertMessage(this.errMsg ? "error" : "success", this.savedMsg);
            if (!this.errMsg) {
                if (Array.isArray(res)) {
                    res.forEach(x => {
                        let tmp = data.find(y => y.id == x.id);
                        if (tmp) data.splice(data.indexOf(tmp), 1);
                        if (!remove) {
                            data.splice(0, 0, x);
                        }
                    });
                }
                return true;
            }
            return false;
        },
        SetDataUpdate(res, data, fields, remove, gendata) {
            this.SetErrorMsg(res, fields, gendata);
            if (remove) this.$root.AlertMessage(this.errMsg ? "error" : "success", this.deletedMsg);
            else this.$root.AlertMessage(this.errMsg ? "error" : "success", this.savedMsg);
            if (!this.errMsg) {
                if (Array.isArray(res)) {
                    res.forEach(x => {
                        let tmp = data.find(y => y.id == x.id);
                        if (tmp) {
                            if (remove) data.splice(data.indexOf(tmp), 1);
                            else {
                                ObjCopyValue(tmp, x);
                            }
                        } else {
                            data.push(x);
                        }
                    });
                }
                return true;
            }
            return false;
        },
        UpdateData(res, data) {
            if (Array.isArray(res)) {
                res.forEach(x => {
                    let tmp = data.find(y => y.id == x.id);
                    if (tmp) {
                        ObjCopyValue(tmp, x);
                    }
                });
            }
        }
    }
}