export const status = [
    { text: "Active", value: "A" },
    { text: "Inactive", value: "I" },
    { text: "Cancel", value: "C" }
];

export const yes_no = [
    { text: "Yes", value: true },
    { text: "No", value: false }
];

export const multivalue = { id: 0, description: "[Multiple values]", _label: "[Multiple values]" };

export const textColor = ['none', 'primary', 'warning', 'info', 'success', 'danger'];

export const configMode = {
    Add: { class: "primary" },
    Edit: { class: "warning" },
    Clone: { class: "success" },
    View: { class: "info" },
    Delete: { class: "danger" },
    Approved: { class: "success" },
    NotApproved: { class: "warning" }
};

export const themes = {
    default: "/themes/defualt.css",
    dark: "/themes/dark.css"
};

export const cost = [
    { text: 'STD Cost', value: false },
    { text: 'PD Cost', value: true }
];

export const urgent = [
    { text: 'Not Urgent', value: false },
    { text: 'Urgent', value: true }
];

export const direction = [
    { text: 'ASC', value: 'asc' },
    { text: 'DESC', value: 'desc' }
];

export const aggregationTypes = {
    sum: 1,
    count: 2,
    avg: 3,
    min: 4,
    max: 5,
    currency: 6,
    percent: 7,
    distinct: 8,
};

export const productCategory = {
    nets: 'N',
    twine: 'T',
    completed: 'P'
}

export const freghtRate = {
    container: 'C',
    weight: 'W'
}

export const tradeTerm = {
    FOB: 'FOB',
    CAF: 'CAF',
    CIF: 'CIF'
}

export const deliveryType = {
    lot: 'L',
    compile: 'C'
}

export const remarkGroupType = {
    Document: 'D',
    CommissionApproved: 'C',
    PromotionApproved: 'M',
    QuotationApproved: 'Q',
    OrderApproved: 'O'
}

export const statusFlagGroupType = {
    Approval: 'A',
    Document: 'D'
}

export const documentStatus = {
    WorkInProgress: 'W',
    PendingForApproval: 'P',
    Approved: 'A',
    NotApproved: 'N',
    PendingForNext: 'I',
    Success: 'S'
}

export const documentStatusIcon = {
    WorkInProgress: 'spinner',
    PendingForApproval: 'hourglass-half',
    Approved: 'check-square-o',
    NotApproved: 'info-circle',
    PendingForNext: 'share-square',
    Success: 'flag-checkered'
}

export const notifyGroupType = {
    QM: '/sales/quotation/manage',
    QA: '/sales/quotation/approval',
    PM: '/sales/proformainvoice/manage',
    MM: '/setup/commission/manage',
    MA: '/setup/commission/approval',
    TM: '/setup/promotion/manage',
    TA: '/setup/promotion/approval',
}

export const table = {
    sxsPromotionDetail: 1,
    sxsCommissionValue: 2,
    sxtQuotationItems: 3,
    sxtOrderItems: 4
}