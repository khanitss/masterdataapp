import Vue from 'vue'

import axios from './instance';
import { GetObjVal } from "@/shared/utils";
import api from './api'

export default {
    mixins: [api],
    data() {
        return {
            dateformat: { day: 'numeric', month: '2-digit', year: 'numeric' },
        }
    },
    methods: {
        Call(o) {
            let urlx = !o.url ? process.env.VUE_APP_API : o.url;
            urlx = urlx + (!o.path ? '' : o.path);

            if (o.method.toUpperCase() === 'GET') {
                if (o.data) {
                    let key = Object.keys(o.data);
                    urlx += "?";
                    for (let i = 0; i < key.length; i++) {
                        let tmp = o.data[key[i]];
                        if (Array.isArray(tmp)) {
                            if (tmp.length > 0) {
                                for (let j = 0; j < tmp.length; j++) {
                                    urlx += key[i] + "=" + (!tmp[j] ? '' : tmp[j]);
                                    if (tmp.length > j + 1) urlx += "&";
                                }
                            } else { urlx += key[i] + "=" + (!o.data[key[i]] ? '' : o.data[key[i]]); }
                        } else { urlx += key[i] + "=" + (!o.data[key[i]] ? '' : o.data[key[i]]); }
                        if (key.length > i + 1) urlx += "&";
                    }
                }
            }
            axios({
                headers: {
                    'content-type': 'application/json',
                    'Access-Control-Allow-Origin': '*',
                    AccessToken: this.$localStorage.get('AccessToken'),
                    Token: this.$localStorage.get('Token'),
                },
                method: o.method.toUpperCase(),
                url: urlx,
                data: o.data ? o.data : {},
                withCredentials: true
            })
                .then(res => {
                    o.callback(res.data);
                })
                .catch((error) => {
                    if (/^O0/.test(GetObjVal(error, 'response.data.code'))) {
                        this.$localStorage.remove("Token");
                        this.$localStorage.remove("User");
                        if (/^[/]user[/]/.test(window.location.pathname)) {
                            window.location.href = '/user/login';
                        } else {
                            if (window.location.pathname) {
                                window.location.href = '/user/login?path=' + window.location.pathname;
                            }
                        }
                        //this.$router.push('/member/signin');
                    } else {
                        if (GetObjVal(error, 'response.data.message')) {
                            // this.$root.AlertMessage('error', GetObjVal(error, 'response.data.message'));
                        } else {
                            // this.$root.AlertMessage('error', 'Unable to connect to server.');
                        }
                    }
                });
        }
    }
}