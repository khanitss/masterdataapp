//import Vue from 'vue'

//------- Dialog -------//
/* export function AlertMessage(type, msg = '') {
  return new Promise((resolve, reject) => {
    msg = msg ? msg : ' ';
    swal(type, msg, type.toLowerCase()).then((ok) => {
      resolve(ok);
    });
  })
}; */

// export function AlertMessage(type, msg = '', title = "") {

//   let time = 5000;
//   if (type == 'S' || type.toLowerCase() == 'success') {
//     type = 'success';
//     title = !title ? 'SUCCESS' : title;
//   } else if (type == 'F' || type.toLowerCase() == 'error') {
//     type = 'error';
//     title = !title ? 'ERROR' : title;
//     time = 60000;
//   }
//   else if (type == 'W' || type.toLowerCase() == 'warning') {
//     type = 'warn';
//     title = !title ? 'WARNING' : title;
//     time = 1000;
//   } else {

//   }

//   Vue.notify({
//     type: type,
//     title: title,
//     duration: time,
//     text: msg
//   });
// };

// export function ConfirmDialog(title, msg, dangerMode) {
//   return new Promise((resolve, reject) => {
//     swal({
//       title: title,
//       text: msg,
//       icon: "warning",
//       buttons: true,
//       dangerMode: dangerMode
//     })
//       .then((ok) => {
//         resolve(ok);
//       });
//   })
// };

//------- Object -------//
// export function Copy(src) {
//   return JSON.parse(JSON.stringify(src));
// }

export function ObjCopy(src) {
  //return Object.assign({}, src);
  return JSON.parse(JSON.stringify(src));
};

export function ObjArrDel(obj, indexDel) {
  for (var i = indexDel.length - 1; i >= 0; i--) {
    obj.splice(indexDel[i], 1);
  }
};

export function ObjResetValue(obj, val) {
  Object.keys(obj).forEach(key => {
    /*if (typeof obj[key] === 'object' && !Array.isArray(obj[key]) && obj[key] !== null) {
      obj[key] = {};
    } else*/
    if (val !== undefined) {
      obj[key] = val;
    } else {
      if (Array.isArray(obj[key])) {
        obj[key] = [];
      } else {
        obj[key] = val;
      }
    }
  });
};

export function ObjCopyValue(obj, src, option = 0) { // 0 == src, 1 == obj
  Object.keys(option ? obj : src).forEach(key => {
    SetObjVal(obj, key, ObjCopy(src[key]));
  });
};

export function ObjClearValue(obj) {
  if (typeof obj === 'object' && obj !== null) {
    Object.keys(obj).forEach(key => {
      if (!ObjClearValue(obj[key])) {
        obj[key] = null;
      }
    });
    return true;
  }
  return false;
};

export function ObjArrSort(objArr, key, direction = 'asc') {
  direction = direction == 'asc' ? '>' : '<';
  let sort = "a.".concat(key, ' ', direction, ' ', 'b.', key, ' ? ', '1', ' : ', 'a.', key, ' == ', 'b.', key, ' ? ', '0', ' : ', '-1');
  sort = eval("(a, b) => { return " + sort + " }");
  return objArr.sort(sort);
}

/*export function GetObjVal(keys, obj) { //keys ex : 'a.b.c'
  let objtmp = Copy(obj);
  keys.split('.').forEach((t) => {
    if (objtmp) {
      objtmp = objtmp[t];
    } else { objtmp = null; }
  });
  return objtmp;
}*/

export function GetObjVal(object, path, defaultValue) {
  path = ''.concat(path).trim();
  defaultValue = typeof defaultValue === "undefined" ? null : defaultValue;
  let obj = object ? object : {};
  if (!IsNull(path)) {
    let keys = path.split(".");
    keys.forEach(function (key) {
      if (
        obj !== null &&
        typeof obj[key] !== "undefined" &&
        obj[key] !== null
      ) {
        obj = obj[key];
      } else {
        obj = defaultValue;
        return;
      }
    });
  }
  return obj;
}

export function SetObjVal(obj, keys, value) { // set(['a', 'b', 'c'], 1)  =  { a: { b: { c: 1 } } }
  obj = obj || {};
  keys = typeof keys === 'string' ? keys.match(/[$]?\w+/g) : Array.prototype.slice.apply(keys);
  keys.reduce((obj, key, index) => {
    obj[key] = index === keys.length - 1 ? value : typeof obj[key] === 'object' && !IsNull(obj) && !IsNull(obj[key]) ? obj[key] : {};
    return obj[key];
  }, obj);
  return obj;
}

export function GetObjValJoin(obj, keys = ['code', 'description'], symbol = ' : ', notEqual = null, defaultValue) {
  if (typeof obj == 'object') {
    if (Array.isArray(keys)) {
      let tmp = '';
      keys.forEach(k => {
        let v = GetObjVal(obj, k, null);
        if (!IsNull(v) && v != notEqual) {
          if (IsNull(tmp)) tmp = tmp.concat(v);
          else tmp = tmp.concat(symbol, v);
        }
      });
      if (!IsNull(tmp)) return tmp;
    } else {
      let tmp = '';
      Object.keys(obj).forEach(key => {
        if (!IsNull(obj[key]) && obj[key] != notEqual) {
          if (IsNull(tmp)) tmp = tmp.concat(obj[key]);
          else tmp = tmp.concat(symbol, obj[key]);
        }
      });
      if (!IsNull(tmp)) return tmp;
    }
  }
  return defaultValue;
}

export function GetValue(val, defaultValue) {
  if (!IsNull(val)) {
    return val;
  }
  return defaultValue;
}

//------- date time -------//
export function CreateDateTime(str) {
  if (!str) {
    return null;
  } else {
    return new Date(str);
  }
};

export function GetMonth(val) {
  val = val + 1;
  return val < 10 ? '0' + val : val;
}

export function GetDay(val) {
  return val < 10 ? '0' + val : val;
}

export function GetDate(d) {
  if (typeof d != 'object') {
    d = CreateDateTime(d);
  }
  try {
    return d.getFullYear() + '-' + GetMonth(d.getMonth()) + '-' + GetDay(d.getDate());
  } catch (ex) { }
  return null;
}

export function GetDateView(str) {
  let d = CreateDateTime(str);
  if (d !== null) {
    return d.getDate() + '/' + GetMonth(d.getMonth()) + '/' + d.getFullYear();
  }
  return "";
}

export function GetDatePrint(str) {
  let d = CreateDateTime(str);
  if (d !== null) {
    return d.getDate() + ' ' + d.toLocaleString('en-us', { month: 'long' }) + ' ' + d.getFullYear();
  }
  return "";
}

export function GetDateTimeView(str) {
  let d = CreateDateTime(str);
  if (d !== null) {
    return d.getDate() + '/' + GetMonth(d.getMonth()) + '/' + d.getFullYear() + ' ' + (d.getHours() < 10 ? '0' + d.getHours() : d.getHours()) + ':' + (d.getMinutes() < 10 ? '0' + d.getMinutes() : d.getMinutes());
  }
  return "";
}

export function GetDateTimeNumber(d, resetHour) { // resetHour = true 00:00:00, false = current hour
  let tmp = null;
  if (typeof d == 'string') {
    d = CreateDateTime(d);
  }
  try {
    if (resetHour) {
      tmp = d.setHours(0, 0, 0, 0);
    } else {
      tmp = d.getTime();
    }
  } catch (ex) {

  }
  return tmp;
};

export function dateDiff(startDate, endDate = new Date().getTime()) { //lone
  let diff = Math.abs(startDate - endDate);
  return secondToText(diff) + ' ago';
};

export function secondToText(second) {
  let days = Math.floor(second / (60 * 60 * 24 * 1000));
  let hours = Math.floor(second / (60 * 60 * 1000)) - (days * 24);
  let minutes = Math.floor(second / (60 * 1000)) - ((days * 24 * 60) + (hours * 60));
  let seconds = Math.floor(second / 1000) - ((days * 24 * 60 * 60) + (hours * 60 * 60) + (minutes * 60));

  if (days > 0) {
    let week = Math.floor(days / 7);
    if (week > 0) {
      let month = Math.floor(week / 4);
      if (month > 0) return month + (month == 1 ? ' month' : ' months');
      return week + (week == 1 ? ' week' : ' weeks');
    }
    return days + (days == 1 ? ' day' : ' days');
  } else {
    if (hours > 0) {
      return hours + (hours == 1 ? ' hour' : ' hours');
    } else {
      if (minutes < 1) {
        return seconds + (seconds == 1 ? ' second' : ' seconds');
      } else {
        return minutes + (minutes == 1 ? ' minute' : ' minutes');
      }
    }
  }
}

export function GetRunningCode(num, digi) {
  let codeTmp = "";
  let max = 10 + 26;
  let val = num = Number(num);
  do {
    num = val % max;
    val = parseInt(val / max);
    codeTmp = num < 10 ? String(num) : String.fromCharCode(num + 55) + codeTmp;
  } while (val > 0);
  for (let i = codeTmp.length; i < digi; i++) {
    codeTmp = "0" + codeTmp;
  }
  return codeTmp;
};

export function GetCodeRunning(num, digi) {
  let codeTmp = num + "";
  for (let i = codeTmp.length; i < digi; i++) {
    codeTmp = "0" + codeTmp;
  }
  return codeTmp;
};

export function GetObjArr(obj, keys, defaultValue) {
  if (Array.isArray(obj)) {
    if (keys) return obj.map(x => GetObjVal(x, keys));
    else return obj;
  }
  if (keys) return GetObjVal(obj, keys) ? [GetObjVal(obj, keys)] : defaultValue;
  return [obj];
};

export function GetArrObj(arr, keys) {
  if (Array.isArray(arr)) {
    if (keys) return arr.map(x => GetObjVal(x, keys));
    else return arr[arr.length - 1];
  }
  if (keys) return GetObjVal(arr, keys);
  return arr;
};

export function GetLastUpdate(obj) {
  return GetDateView(obj.timestamp) + (!obj.updateBy ? '' : " - " + obj.updateBy);
};

export function GetApprovedBy(obj) {
  return GetDateView(obj.timestamp) + (!obj.by ? '' : " - " + obj.by);
};

export function GetTimeLastUpdate(obj) {
  return GetDateTimeView(obj.timestamp) + (!obj.updateBy ? '' : " - " + obj.updateBy);
};

export function GetLastUpdateBy(obj) {
  return GetDateView(obj.timestamp) + (!obj.by ? '' : " - " + obj.by);
};

export function GetTimeLastUpdateBy(obj) {
  return GetDateTimeView(obj.timestamp) + (!obj.by ? '' : " - " + obj.by);
};

export function GetRangeSeq(arr) {
  let tmp = ObjCopy(arr);
  let lastSeq = tmp[0];
  for (let i = 1; i < tmp.length; i++) {
    if (lastSeq + 1 == tmp[i]) {
      lastSeq = tmp[i];
      tmp[i] = tmp[i + 1] == lastSeq + 1 ? null : tmp[i];
    } else {
      lastSeq = tmp[i];
    }
  }
  return tmp.join(',').replace(/[,]{2,}/g, ' - ');
};

export function FileToBase64(file) {
  return new Promise((resolve, reject) => {
    let reader = new FileReader();
    reader.onloadend = () => {
      resolve(reader.result);
    }
    reader.readAsDataURL(file);
  })
};

export function FileToArrayBuffer(file) {
  return new Promise((resolve, reject) => {
    let reader = new FileReader();
    reader.onloadend = () => {
      resolve(reader.result);
    }
    reader.readAsArrayBuffer(file);
  })
};

export function saveExcelFile(csvContent, fileName) {
  let D = document;
  let a = D.createElement('a');
  let strMimeType = 'application/octet-stream;charset=utf-8';
  let rawFile;

  if ("download" in a) {
    let blob = new Blob(["", csvContent], { type: strMimeType });
    rawFile = URL.createObjectURL(blob);
    a.setAttribute("download", fileName + ".xlsx");
  } else {
    rawFile = "data:" + strMimeType + "," + encodeURIComponent(csvContent);
    a.setAttribute("target", "_blank");
  }
  a.href = rawFile;
  a.setAttribute("style", "display:none;");
  D.body.appendChild(a);
  setTimeout(() => {
    if (a.click) {
      a.click();
    } else if (document.createEvent) {
      var eventObj = document.createEvent("MouseEvents");
      eventObj.initEvent("click", true, true);
      a.dispatchEvent(eventObj);
    }
    D.body.removeChild(a);
  }, 100);
};

export function outerHTML(node) {
  return node.outerHTML || (function (n) {
    var div = document.createElement('div'), h;
    div.appendChild(n);
    h = div.innerHTML; div = null; return h;
  })(node);
}

export function addTag(name, attributes, sync) {
  let headEl = document.getElementsByTagName('head')[0];
  let el = document.createElement(name), attrName;
  for (attrName in attributes) {
    el.setAttribute(attrName, attributes[attrName]);
  }
  sync ? document.write(outerHTML(el)) : headEl.appendChild(el);
};

//chk
export function IsNull(val) {
  return val === undefined || val === '' || ''.concat(val).trim() === '' || val === null;
}

export function genShippingMark(data, dynamic) {
  let canvas = document.createElement('canvas');
  canvas.width = 364;
  canvas.height = 364;

  let ctx = canvas.getContext("2d");
  ctx.clearRect(0, 0, canvas.width, canvas.height);
  ctx.font = "normal 14pt Arial";
  ctx.textBaseline = "bottom";
  ctx.textAlign = "start";
  ctx.fillText('{DATE}', 10, 26.561);

  if (data.showMadeinThai) {
    ctx.textAlign = "end";
    ctx.fillText('MADE IN THAILAND', 354.8, 26.561);
  }

  ctx.textAlign = "center";
  ctx.font = "normal 12pt Arial";
  ctx.fillText(`- ${data.code} SHIPPING MARK -`, 182.448, 62.097);

  ctx.font = "bold 20pt Arial";
  // ctx.fillText('B/NO._________________', 182.448, 346.344);
  ctx.fillText('B/NO..................................', 182.448, 346.344);

  ctx.beginPath();
  // top left      
  ctx.moveTo(89.8, 31.217);
  ctx.lineTo(10, 31.217);
  ctx.lineTo(10, 72.783);

  // top right
  ctx.moveTo(275, 31.217);
  ctx.lineTo(354.8, 31.217);
  ctx.lineTo(354.8, 72.783);

  // bottom left
  ctx.moveTo(10, 313.217);
  ctx.lineTo(10, 354);
  ctx.lineTo(89.8, 354);

  // bottom right
  ctx.moveTo(275, 354);
  ctx.lineTo(354, 354);
  ctx.lineTo(354, 313.217);
  ctx.stroke();

  let horizontal = {
    L: 6.834,
    C: 182.448,
    R: 357.967
  };
  let vertical = {
    T: 107,
    M: 193,
    B: 287
  };
  let line = 60, maxWidth = 350;

  ctx.textBaseline = "middle";
  ctx.textAlign = data.textHorAlign == 'L' ? 'left' : data.textHorAlign == 'R' ? 'right' : 'center';

  if (data.description) {
    let tmp = (dynamic ? data.description.replace(/{DYNAMIC}/g, dynamic) : data.description).split(/[\n]/g);
    tmp.forEach((d, i) => {
      let fontSize = 40;
      do {
        ctx.font = `bold ${fontSize}pt Arial`;
        fontSize--;
      } while (ctx.measureText(d).width > maxWidth);

      let ver = vertical[data.textVerAlign];
      if (data.textVerAlign == 'T') ver = ver + (line * i);
      else if (data.textVerAlign == 'B') ver = ver - (line * ((tmp.length - 1) - i));
      else {
        let m = (tmp.length / 2) - 0.5;
        if (i < m) {
          ver = ver - ((line * ((tmp.length - 1) - i - i)) / 2);
        } else if (i > m) {
          ver = ver + ((line * (i == 2 && tmp.length == 4 ? 1 : i)) / 2);
        }
      }
      ctx.fillText(d, horizontal[data.textHorAlign], ver);
    });
  }
  return canvas.toDataURL();
};

// math
export function number(value, decimal) {
  if (isNaN(value) || IsNull(value)) return null
  if (decimal) {
    value = Number(value) == 0 ? value.toString() : Round(Number(value), decimal).toFixed(decimal);
  } else {
    value = value.toString();
  }
  const val = value.split('.');
  if (val.length > 1) {
    return val[0].replace(/\B(?=(\d{3})+(?!\d))/g, ',') + '.' + val[1];
  }
  return value.replace(/\B(?=(\d{3})+(?!\d))/g, ',');
};


export function Round(num, places = 0) {
  return +(Math.round(num + "e+" + places) + "e-" + places);
};