export default {
    data() {
        return {
            api: {
                //  !LOGIN
                OauthAccess: (o) => {
                    Object.assign(o, { method: 'get', path: 'v1/Oauth/Access' });
                    this.Call(o);
                },
                OauthLogin: (o) => {
                    Object.assign(o, { method: 'post', path: 'v1/Oauth/Login' });
                    this.Call(o);
                },
                OauthLogout: (o) => {
                    Object.assign(o, { method: 'post', path: 'v1/Oauth/Logout' });
                    this.Call(o);
                },

                //  !REGISTER
                VTemploy1Search: (o) => {
                    Object.assign(o, { method: 'post', path: 'v1/vTemploy1/Search' });
                    this.Call(o);
                },
                UserSearch: (o) => {
                    Object.assign(o, { method: 'post', path: 'v1/User/SearchRegis' });
                    this.Call(o);
                },
                UserRegister: (o) => {
                    Object.assign(o, { method: 'post', path: 'v1/User/Save' });
                    this.Call(o);
                },

                //  !Standardpack
                View_master_s_labelSearch: (o) => {
                    Object.assign(o, { method: 'post', path: 'v1/standardpack/View_master_s_label/Search' });
                    this.Call(o);
                },
                View_master_s_labelSearchID: (o) => {
                    Object.assign(o, { method: 'post', path: 'v1/standardpack/View_master_s_label/SearchID' });
                    this.Call(o);
                },
                View_master_u_labelSearchLabelCd: (o) => {
                    Object.assign(o, { method: 'post', path: 'v1/standardpack/View_master_s_label/SearchLabelCd' });
                    this.Call(o);
                },
                View_master_s_labelSearchActive: (o) => {
                    Object.assign(o, { method: 'post', path: 'v1/standardpack/View_master_s_label/SearchActive' });
                    this.Call(o);
                },
                Master_s_lableSave: (o) => {
                    Object.assign(o, { method: 'post', path: 'v1/standardpack/Master_s_label/Save' });
                    this.Call(o);
                },
                Master_s_lableMove: (o) => {
                    Object.assign(o, { method: 'post', path: 'v1/standardpack/Master_s_label/Move' });
                    this.Call(o);
                },
                Master_s_lableEdit: (o) => {
                    Object.assign(o, { method: 'post', path: 'v1/standardpack/Master_s_label/Edit' });
                    this.Call(o);
                },

                //  ! รหัส 10 หลักที่ดึงมาจากระบบจัดซื้อ
                View_itemSearch: (o) => {
                    Object.assign(o, { method: 'post', path: 'v1/standardpack/View_item/SearchItem' });
                    this.Call(o);
                },
                //  !รวมรหัส 10 หลักค้นหาโดยประเภทใช้ 4 หลักเเรก /* 0808% */
                View_itemSearchAllItemCode: (o) => {
                    Object.assign(o, { method: 'post', path: 'v1/standardpack/View_item/SearchAllItemCode' });
                    this.Call(o);
                },
                View_itemSearchItemCode: (o) => {
                    Object.assign(o, { method: 'post', path: 'v1/standardpack/View_item/SearchItemCode' });
                    this.Call(o);
                },


                //  !Standardpack -- Label Spec
                View_master_s_LabelSpecSearch: (o) => {
                    Object.assign(o, { method: 'post', path: 'v1/standardpack/View_master_s_LabelSpec/Search' });
                    this.Call(o);
                },
                View_master_s_labelspecSearchbyCus: (o) => {
                    Object.assign(o, { method: 'post', path: 'v1/standardpack/View_master_s_LabelSpec/SearchbyCus' });
                    this.Call(o);
                },
                View_master_s_LabelSpecSearchByID: (o) => {
                    Object.assign(o, { method: 'post', path: 'v1/standardpack/View_master_s_LabelSpec/SearchByID' });
                    this.Call(o);
                },
                Master_s_labelspecSave: (o) => {
                    Object.assign(o, { method: 'post', path: 'v1/standardpack/Master_s_labelspec/Save' });
                    this.Call(o);
                },
                Master_s_labelspecMove: (o) => {
                    Object.assign(o, { method: 'post', path: 'v1/standardpack/Master_s_labelspec/Move' });
                    this.Call(o);
                },
                Master_s_labelspecUpdate: (o) => {
                    Object.assign(o, { method: 'post', path: 'v1/standardpack/Master_s_labelspec/Update' });
                    this.Call(o);
                },

                //  !Standardpack -- Line Label
                View_master_s_linelabelSearch: (o) => {
                    Object.assign(o, { method: 'post', path: 'v1/standardpack/View_master_s_linelabel/Search' });
                    this.Call(o);
                },
                View_master_s_linelabelSearchMaxID: (o) => {
                    Object.assign(o, { method: 'post', path: 'v1/standardpack/View_master_s_linelabel/SearchMaxID' });
                    this.Call(o);
                },
                View_master_s_linelabelSearchID: (o) => {
                    Object.assign(o, { method: 'post', path: 'v1/standardpack/View_master_s_linelabel/SearchID' });
                    this.Call(o);
                },
                View_master_s_linelabelSearchID_D: (o) => {
                    Object.assign(o, { method: 'post', path: 'v1/standardpack/View_master_s_linelabel/SearchID_D' });
                    this.Call(o);
                },
                View_master_s_linelabelSearchID_D_U: (o) => {
                    Object.assign(o, { method: 'post', path: 'v1/standardpack/View_master_s_linelabel/SearchID_D_U' });
                    this.Call(o);
                },
                Master_s_linelabelSave: (o) => {
                    Object.assign(o, { method: 'post', path: 'v1/standardpack/Master_s_linelabel/Save' });
                    this.Call(o);
                },
                Master_s_linelabelSaveD: (o) => {
                    Object.assign(o, { method: 'post', path: 'v1/standardpack/Master_s_linelabel/SaveD' });
                    this.Call(o);
                },
                Master_s_linelabelMove: (o) => {
                    Object.assign(o, { method: 'post', path: 'v1/standardpack/Master_s_linelabel/Move' });
                    this.Call(o);
                },
                Master_s_linelabelEdit: (o) => {
                    Object.assign(o, { method: 'post', path: 'v1/standardpack/Master_s_linelabel/Edit' });
                    this.Call(o);
                },
                View_master_s_linesubjectSearch: (o) => {
                    Object.assign(o, { method: 'post', path: 'v1/standardpack/View_master_s_linesubject/Search' });
                    this.Call(o);
                },
                
                //  !Customer
                View_master_s_cutomerSearch: (o) => {
                    Object.assign(o, { method: 'post', path: 'v1/Customer/View_master_s_cutomer/Search' });
                    this.Call(o);
                },
               
                View_master_s_cutomerSearchCusname: (o) => {
                    Object.assign(o, { method: 'post', path: 'v1/Customer/View_master_s_cutomer/SearchCusName' });
                    this.Call(o);
                },
               
               
                //  !Product
                View_master_s_knottypeSearch: (o) => {
                    Object.assign(o, { method: 'post', path: 'v1/Product/View_master_s_knottype/Search' });
                    this.Call(o);
                },
                View_master_s_linetypeSearch: (o) => {
                    Object.assign(o, { method: 'post', path: 'v1/Product/View_master_s_linetype/Search' });
                    this.Call(o);
                },
                View_master_s_pqgradeSearch: (o) => {
                    Object.assign(o, { method: 'post', path: 'v1/Product/View_master_s_pqgrade/Search' });
                    this.Call(o);
                },
                View_master_s_productsizeSearch: (o) => {
                    Object.assign(o, { method: 'post', path: 'v1/Product/View_master_s_productsize/Search' });
                    this.Call(o);
                },
                View_master_s_producttypeSearch: (o) => {
                    Object.assign(o, { method: 'post', path: 'v1/Product/View_master_s_producttype/Search' });
                    this.Call(o);
                },
                View_master_s_stretchingtypeSearch: (o) => {
                    Object.assign(o, { method: 'post', path: 'v1/Product/View_master_s_stretchingtype/Search' });
                    this.Call(o);
                },
                View_master_s_colorSearch: (o) => {
                    Object.assign(o, { method: 'post', path: 'v1/Product/View_master_s_color/Search' });
                    this.Call(o);
                },
                View_master_v_rumtypeSearch: (o) => {
                    Object.assign(o, { method: 'post', path: 'v1/Product/View_master_v_rumtype/Search' });
                    this.Call(o);
                },
                View_master_v_bagorderSearch: (o) => {
                    Object.assign(o, { method: 'post', path: 'v1/Product/View_master_v_bagorder/Search' });
                    this.Call(o);
                },
                
                //  !Standardpack -- Canvas
                Master_s_CanvasSearchView: (o) => {
                    Object.assign(o, { method: 'post', path: 'v1/standardpack/Master_s_Canvas/SearchView' });
                    this.Call(o);
                },
                Master_s_CanvasSearch: (o) => {
                    Object.assign(o, { method: 'post', path: 'v1/standardpack/Master_s_Canvas/Search' });
                    this.Call(o);
                },
                Master_s_CanvasSearchU: (o) => {
                    Object.assign(o, { method: 'post', path: 'v1/standardpack/Master_s_Canvas/SearchU' });
                    this.Call(o);
                },
                Master_s_CanvasSearchID: (o) => {
                    Object.assign(o, { method: 'post', path: 'v1/standardpack/Master_s_Canvas/SearchID' });
                    this.Call(o);
                },
                Master_s_CanvasSave: (o) => {
                    Object.assign(o, { method: 'post', path: 'v1/standardpack/Master_s_Canvas/Save' });
                    this.Call(o);
                },
                Master_s_CanvasMove: (o) => {
                    Object.assign(o, { method: 'post', path: 'v1/standardpack/Master_s_Canvas/Move' });
                    this.Call(o);
                },

                //  !Standardpack -- Bar color
                Master_s_BarcolorSearchView: (o) => {
                    Object.assign(o, { method: 'post', path: 'v1/standardpack/Master_s_Barcolor/SearchView' });
                    this.Call(o);
                },
                Master_s_BarcolorSearch: (o) => {
                    Object.assign(o, { method: 'post', path: 'v1/standardpack/Master_s_Barcolor/Search' });
                    this.Call(o);
                },
                Master_s_BarcolorSearchU: (o) => {
                    Object.assign(o, { method: 'post', path: 'v1/standardpack/Master_s_Barcolor/SearchU' });
                    this.Call(o);
                },
                Master_s_BarcolorSearchID: (o) => {
                    Object.assign(o, { method: 'post', path: 'v1/standardpack/Master_s_Barcolor/SearchID' });
                    this.Call(o);
                },
                Master_s_BarcolorSave: (o) => {
                    Object.assign(o, { method: 'post', path: 'v1/standardpack/Master_s_Barcolor/Save' });
                    this.Call(o);
                },
                Master_s_BarcolorMove: (o) => {
                    Object.assign(o, { method: 'post', path: 'v1/standardpack/Master_s_Barcolor/Move' });
                    this.Call(o);
                },

                //  !Standardpack -- Canvas and Barcolor
                Master_s_CanvasnBarSearch: (o) => {
                    Object.assign(o, { method: 'post', path: 'v1/standardpack/Master_s_CanvasnBar/Search' });
                    this.Call(o);
                },
                Master_s_CanvasnBarSearchU: (o) => {
                    Object.assign(o, { method: 'post', path: 'v1/standardpack/Master_s_CanvasnBar/SearchU' });
                    this.Call(o);
                },
                Master_s_CanvasnBarSearchActive: (o) => {
                    Object.assign(o, { method: 'post', path: 'v1/standardpack/Master_s_CanvasnBar/SearchActive' });
                    this.Call(o);
                },
                Master_s_CanvasnBarSearchUsed: (o) => {
                    Object.assign(o, { method: 'post', path: 'v1/standardpack/Master_s_CanvasnBar/SearchUsed' });
                    this.Call(o);
                },
                Master_s_CanvasnBarSave: (o) => {
                    Object.assign(o, { method: 'post', path: 'v1/standardpack/Master_s_CanvasnBar/Save' });
                    this.Call(o);
                },
                Master_s_CanvasnBarMove: (o) => {
                    Object.assign(o, { method: 'post', path: 'v1/standardpack/Master_s_CanvasnBar/Move' });
                    this.Call(o);
                },

                //  !Standardpack -- Canvas and Barcolor Spec
                Master_s_CanvasnBarSpecSearch: (o) => {
                    Object.assign(o, { method: 'post', path: 'v1/standardpack/Master_s_CanvasnBarSpec/Search' });
                    this.Call(o);
                },
                Master_s_CanvasnBarSpecSearchCancel: (o) => {
                    Object.assign(o, { method: 'post', path: 'v1/standardpack/Master_s_CanvasnBarSpec/SearchCancel' });
                    this.Call(o);
                },
                Master_s_CanvasnBarSpecSave: (o) => {
                    Object.assign(o, { method: 'post', path: 'v1/standardpack/Master_s_CanvasnBarSpec/Save' });
                    this.Call(o);
                },
                Master_s_CanvasnBarSpecMove: (o) => {
                    Object.assign(o, { method: 'post', path: 'v1/standardpack/Master_s_CanvasnBarSpec/Move' });
                    this.Call(o);
                },
                
                //  !Standardpack -- Strap
                Master_s_StrapSearchView : (o) => {
                    Object.assign(o, { method: 'post', path: 'v1/standardpack/Master_s_Strap/SearchView' });
                    this.Call(o);
                },
                Master_s_StrapSearchCancel: (o) => {
                    Object.assign(o, { method: 'post', path: 'v1/standardpack/Master_s_Strap/SearchCancel' });
                    this.Call(o);
                },
                Master_s_StrapSearch : (o) => {
                    Object.assign(o, { method: 'post', path: 'v1/standardpack/Master_s_Strap/Search' });
                    this.Call(o);
                },
                Master_s_StrapSave: (o) => {
                    Object.assign(o, { method: 'post', path: 'v1/standardpack/Master_s_Strap/Save' });
                    this.Call(o);
                },
                Master_s_StrapMove: (o) => {
                    Object.assign(o, { method: 'post', path: 'v1/standardpack/Master_s_Strap/Move' });
                    this.Call(o);
                },

                //  !Standardpack -- StrapSpec
                Master_s_StrapSpecSearch : (o) => {
                    Object.assign(o, { method: 'post', path: 'v1/standardpack/Master_s_StrapSpec/Search' });
                    this.Call(o);
                },
                Master_s_StrapSpecSearchCancel: (o) => {
                    Object.assign(o, { method: 'post', path: 'v1/standardpack/Master_s_StrapSpec/SearchCancel' });
                    this.Call(o);
                },
                Master_s_StrapSpecSave: (o) => {
                    Object.assign(o, { method: 'post', path: 'v1/standardpack/Master_s_StrapSpec/Save' });
                    this.Call(o);
                },
                Master_s_StrapSpecMove: (o) => {
                    Object.assign(o, { method: 'post', path: 'v1/standardpack/Master_s_StrapSpec/Move' });
                    this.Call(o);
                },

                //  !Standardpack -- Sticker
                Master_s_StickerSearchView : (o) => {
                    Object.assign(o, { method: 'post', path: 'v1/standardpack/Master_s_Sticker/SearchView' });
                    this.Call(o);
                },
                Master_s_StickerSearchCancel: (o) => {
                    Object.assign(o, { method: 'post', path: 'v1/standardpack/Master_s_Sticker/SearchCancel' });
                    this.Call(o);
                },
                Master_s_StickerSearch : (o) => {
                    Object.assign(o, { method: 'post', path: 'v1/standardpack/Master_s_Sticker/Search' });
                    this.Call(o);
                },
                Master_s_StickerSave: (o) => {
                    Object.assign(o, { method: 'post', path: 'v1/standardpack/Master_s_Sticker/Save' });
                    this.Call(o);
                },
                Master_s_StickerMove: (o) => {
                    Object.assign(o, { method: 'post', path: 'v1/standardpack/Master_s_Sticker/Move' });
                    this.Call(o);
                },

                
                //  !Standardpack -- StickerSpec
                Master_s_StickerSpecSearch: (o) => {
                    Object.assign(o, { method: 'post', path: 'v1/standardpack/Master_s_StickerSpec/Search' });
                    this.Call(o);
                },
                Master_s_StickerSpecSearchCancel: (o) => {
                    Object.assign(o, { method: 'post', path: 'v1/standardpack/Master_s_StickerSpec/SearchCancel' });
                    this.Call(o);
                },
                Master_s_StickerSpecSave: (o) => {
                    Object.assign(o, { method: 'post', path: 'v1/standardpack/Master_s_StickerSpec/Save' });
                    this.Call(o);
                },
                Master_s_StickerSpecMove: (o) => {
                    Object.assign(o, { method: 'post', path: 'v1/standardpack/Master_s_StickerSpec/Move' });
                    this.Call(o);
                },

                //  !Standardpack -- Fold
                Master_s_FoldSearchView : (o) => {
                    Object.assign(o, { method: 'post', path: 'v1/standardpack/Master_s_Fold/SearchView' });
                    this.Call(o);
                },
                Master_s_FoldSearchCancel: (o) => {
                    Object.assign(o, { method: 'post', path: 'v1/standardpack/Master_s_Fold/SearchCancel' });
                    this.Call(o);
                },
                Master_s_FoldSearch : (o) => {
                    Object.assign(o, { method: 'post', path: 'v1/standardpack/Master_s_Fold/Search' });
                    this.Call(o);
                },
                Master_s_FoldSave: (o) => {
                    Object.assign(o, { method: 'post', path: 'v1/standardpack/Master_s_Fold/Save' });
                    this.Call(o);
                },
                Master_s_FoldMove: (o) => {
                    Object.assign(o, { method: 'post', path: 'v1/standardpack/Master_s_Fold/Move' });
                    this.Call(o);
                },


                //  !Standardpack -- FoldSpec
                Master_s_FoldSpecSearch : (o) => {
                    Object.assign(o, { method: 'post', path: 'v1/standardpack/Master_s_FoldSpec/Search' });
                    this.Call(o);
                },
                Master_s_FoldSpecSearchbyCus : (o) => {
                    Object.assign(o, { method: 'post', path: 'v1/standardpack/Master_s_FoldSpec/SearchbyCus' });
                    this.Call(o);
                },
                Master_s_FoldSpecSearchbyId : (o) => {
                    Object.assign(o, { method: 'post', path: 'v1/standardpack/Master_s_FoldSpec/SearchByID' });
                    this.Call(o);
                },
                Master_s_FoldSpecSearchCancel : (o) => {
                    Object.assign(o, { method: 'post', path: 'v1/standardpack/Master_s_FoldSpec/SearchCancelbyID' });
                    this.Call(o);
                },
                Master_s_FoldSpecSave : (o) => {
                    Object.assign(o, { method: 'post', path: 'v1/standardpack/Master_s_FoldSpec/Save' });
                    this.Call(o);
                },
                Master_s_FoldSpecMove : (o) => {
                    Object.assign(o, { method: 'post', path: 'v1/standardpack/Master_s_FoldSpec/Move' });
                    this.Call(o);
                },

                //  !Standardpack -- RumFold
                Master_s_RumFoldSearchView : (o) => {
                    Object.assign(o, { method: 'post', path: 'v1/standardpack/Master_s_RumFold/SearchView' });
                    this.Call(o);
                },
                Master_s_RumFoldSearchCancel: (o) => {
                    Object.assign(o, { method: 'post', path: 'v1/standardpack/Master_s_RumFold/SearchCancel' });
                    this.Call(o);
                },
                Master_s_RumFoldSearch : (o) => {
                    Object.assign(o, { method: 'post', path: 'v1/standardpack/Master_s_RumFold/Search' });
                    this.Call(o);
                },
                Master_s_RumFoldSave: (o) => {
                    Object.assign(o, { method: 'post', path: 'v1/standardpack/Master_s_RumFold/Save' });
                    this.Call(o);
                },
                Master_s_RumFoldMove: (o) => {
                    Object.assign(o, { method: 'post', path: 'v1/standardpack/Master_s_RumFold/Move' });
                    this.Call(o);
                },


                //  !Standardpack -- RumFoldSpec
                Master_s_RumFoldSpecSearch : (o) => {
                    Object.assign(o, { method: 'post', path: 'v1/standardpack/Master_s_RumFoldSpec/Search' });
                    this.Call(o);
                },
                Master_s_RumFoldSpecSearchCancel: (o) => {
                    Object.assign(o, { method: 'post', path: 'v1/standardpack/Master_s_RumFoldSpec/SearchCancel' });
                    this.Call(o);
                },
                Master_s_RumFoldSpecSave: (o) => {
                    Object.assign(o, { method: 'post', path: 'v1/standardpack/Master_s_RumFoldSpec/Save' });
                    this.Call(o);
                },
                Master_s_RumFoldSpecMove: (o) => {
                    Object.assign(o, { method: 'post', path: 'v1/standardpack/Master_s_RumFoldSpec/Move' });
                    this.Call(o);
                },


                //  !Standardpack -- Bag
                Master_s_BagSearch : (o) => {
                    Object.assign(o, { method: 'post', path: 'v1/standardpack/Master_s_Bag/Search' });
                    this.Call(o);
                },
                Master_s_BagSearchActive : (o) => {
                    Object.assign(o, { method: 'post', path: 'v1/standardpack/Master_s_Bag/SearchActive' });
                    this.Call(o);
                },
                Master_s_BagSearchCancel: (o) => {
                    Object.assign(o, { method: 'post', path: 'v1/standardpack/Master_s_Bag/SearchCancel' });
                    this.Call(o);
                },
                Master_s_BagSave: (o) => {
                    Object.assign(o, { method: 'post', path: 'v1/standardpack/Master_s_Bag/Save' });
                    this.Call(o);
                },
                Master_s_BagMove: (o) => {
                    Object.assign(o, { method: 'post', path: 'v1/standardpack/Master_s_Bag/Move' });
                    this.Call(o);
                },

                 //  !Standardpack -- BagSpec
                 Master_s_BagSpecSearch : (o) => {
                    Object.assign(o, { method: 'post', path: 'v1/standardpack/Master_s_BagSpec/Search' });
                    this.Call(o);
                },
                Master_s_BagSpecSearchCus : (o) => {
                    Object.assign(o, { method: 'post', path: 'v1/standardpack/Master_s_BagSpec/SearchCus' });
                    this.Call(o);
                },
                Master_s_BagSpecSave : (o) => {
                    Object.assign(o, { method: 'post', path: 'v1/standardpack/Master_s_BagSpec/Save' });
                    this.Call(o);
                },
                Master_s_BagSpecMove : (o) => {
                    Object.assign(o, { method: 'post', path: 'v1/standardpack/Master_s_BagSpec/Move' });
                    this.Call(o);
                },

                //  !Standardpack -- Rope
                Master_s_RopeSearch : (o) => {
                    Object.assign(o, { method: 'post', path: 'v1/standardpack/Master_s_Rope/Search' });
                    this.Call(o);
                },
                Master_s_RopeSearchActive : (o) => {
                    Object.assign(o, { method: 'post', path: 'v1/standardpack/Master_s_Rope/SearchActive' });
                    this.Call(o);
                },
                Master_s_RopeSave: (o) => {
                    Object.assign(o, { method: 'post', path: 'v1/standardpack/Master_s_Rope/Save' });
                    this.Call(o);
                },
                Master_s_RopeMove: (o) => {
                    Object.assign(o, { method: 'post', path: 'v1/standardpack/Master_s_Rope/Move' });
                    this.Call(o);
                },

                //  !Standardpack -- RopeSpec
                Master_s_RopeSpecSearch : (o) => {
                    Object.assign(o, { method: 'post', path: 'v1/standardpack/Master_s_RopeSpec/Search' });
                    this.Call(o);
                },
                Master_s_RopeSpecSearchCus : (o) => {
                    Object.assign(o, { method: 'post', path: 'v1/standardpack/Master_s_RopeSpec/SearchCus' });
                    this.Call(o);
                },
                Master_s_RopeSpecSave: (o) => {
                    Object.assign(o, { method: 'post', path: 'v1/standardpack/Master_s_RopeSpec/Save' });
                    this.Call(o);
                },
                Master_s_RopeSpecMove: (o) => {
                    Object.assign(o, { method: 'post', path: 'v1/standardpack/Master_s_RopeSpec/Move' });
                    this.Call(o);
                },

                // !Get Label Code New
                View_master_s_labelSearchSearchLastID: (o) => {
                    Object.assign(o, { method: 'post', path: 'v1/standardpack/View_master_s_label/SearchLastID' });
                    this.Call(o);
                },
                Master_s_AcczoneLabelCodeSearch: (o) => {
                    Object.assign(o, { method: 'post', path: 'v1/Master_s_AcczoneLabelCode/Search' });
                    this.Call(o);
                },
                Master_s_LabelGradeCodeSearch: (o) => {
                    Object.assign(o, { method: 'post', path: 'v1/Master_s_LabelGrade/Search' });
                    this.Call(o);
                },
                Master_s_LabelOwnerSearch: (o) => {
                    Object.assign(o, { method: 'post', path: 'v1/Master_s_LabelOwner/Search' });
                    this.Call(o);
                },
                Master_s_LabelPaperTypeSearch: (o) => {
                    Object.assign(o, { method: 'post', path: 'v1/Master_s_LabelPaperType/Search' });
                    this.Call(o);
                },
                Master_v_AcczoneSearch: (o) => {
                    Object.assign(o, { method: 'post', path: 'v1/Master_v_Acczone/Search' });
                    this.Call(o);
                },

            }
        }
    }
}
