import 'core-js/stable'
import Vue from 'vue'
import App from './App'
import router from './router'
import CoreuiVue from '@coreui/vue'
import { freeSet as icons } from '@coreui/icons'
import store from './store'
import { BootstrapVue, IconsPlugin } from 'bootstrap-vue'

import axios from 'axios'
import VueAxios from 'vue-axios'
Vue.use(VueAxios, axios)

import Multiselect from '@/components/multiselect'
Vue.component('multiselect', Multiselect)

import core from '@/shared/core';
Vue.mixin(core);

import VueLocalStorage from 'vue-localstorage'
Vue.use(VueLocalStorage);

import Vuelidate from 'vuelidate'
Vue.use(Vuelidate);
// import Datepicker from 'vuejs-datepicker';
import DatePicker from 'vue2-datepicker';
  import 'vue2-datepicker/index.css';

import VueProgressBar from 'vue-progressbar'
const options = {
    color: '#ffc107',
    failedColor: '#f92525',
    thickness: '3px',
    transition: {
        speed: '1s', //0.2s
        opacity: '0.6s', //0.6s
        termination: 600
    },
    autoRevert: true,
    location: 'top',
    inverse: false
}
Vue.use(VueProgressBar, options);

// Install BootstrapVue
Vue.use(BootstrapVue) 
// Optionally install the BootstrapVue icon components plugin
Vue.use(IconsPlugin)
Vue.config.performance = true
Vue.use(CoreuiVue)
Vue.prototype.$log = console.log.bind(console)

new Vue({
  el: '#app',
  router,
  store,
  icons,
  template: '<App/>',
  components: {
    App,
    // Datepicker,
    DatePicker
  }
})
