import Multiselect from "vue-multiselect";
import {
  GetObjVal,
  SetObjVal,
  ObjCopy,
  ObjResetValue,
  ObjCopyValue,
  GetObjValJoin,
  GetObjArr,
  GetLastUpdate,
  GetDate,
  GetDateView,
  FileToBase64,
} from "@/shared/utils";
import setupmixin from "@/shared/setupmixin";
import swal from "sweetalert";
import DatePicker from "vue2-datepicker";
import "vue2-datepicker/index.css";

export default {
  data() {
    return {
      HReport: true,
      itemsTabs1: true,
      itemsTabs2: false,
      itemsTabs3: false,

      openAddModal: false,
      openEditModal: false,
      openViewModal: false,
      openDeleteModal: false,

      SaleOptions: [
        { value: "0", name: "ขายในประเทศ" },
        { value: "1", name: "ขายต่างประเทศ" },
      ],
      SaleValue: {},
      CustomerOptions: {
        lists: [],
        selected: null,
      },
      CasvasnBarOptions: {
        lists: [],
        selected: null,
      },
      search: {
        cuscod_id: "",
        canvasnbar_cd: "",
        txtsearch: "",
      },
      checked: [],
      checkedSpec: "",

      dataCanvasnBarSpec: [],
      dataCancelCanvasnBarSpec: [],

      dataActive: [],
      dataCanvasandBarSpecCount: 0,
      pageA: 1,
      perPageA: 10,
      pagesA: [],

      dataInactive: [],
      dataICanvasandBarSpecCount: 0,
      pageI: 1,
      perPageI: 5,
      pagesI: [],

      dataCancel: [],
      dataCCanvasandBarSpecCount: 0,
      pageC: 1,
      perPageC: 5,
      pagesC: [],

      defaultExDate: null,
      currentDate: null,
      thisNowDate: "",

      insertSpec: {
        canvasnbarspec_st: "",
        canvasnbarspec_en: "",
      },
      viewSpec: {
        tsale: "",
        customer: "",
        canvasnbar: "",
        canvasnbarspec_st: "",
        canvasnbarspec_en: "",
      },
      editSpec: {
        tsale: "",
        customer: "",
        canvasnbar: "",
        canvasnbarspec_st: "",
        canvasnbarspec_en: "",
      },
      deleteSpec: {
        customer: "",
        canvasnbar: "",
      },
      ShowsubData: false,

      ProductTypeOptions: {
        lists: [],
        selected: null,
      },

      ProductSizeMinOptions: {
        lists: [],
        selected: null,
      },
      ProductSizeMaxOptions: {
        lists: [],
        selected: null,
      },

      LabelOptions: {
        lists: [],
        selected: null,
      },
    };
  },
  components: {
    Multiselect,
    DatePicker,
  },
  created() {
    let nowDate = new Date();
    this.currentDate = GetDate(nowDate.setDate(nowDate.getDate()));
    this.thisNowDate = GetDate(nowDate.setDate(nowDate.getDate()));
    this.defaultExDate = "2000-01-01";

    this.insertSpec.canvasnbarspec_st = this.currentDate;
    this.insertSpec.canvasnbarspec_en = this.defaultExDate;

    this.$root.api.Master_s_CanvasnBarSearchActive({
      data: {},
      callback: (res) => {
        res.forEach((x) => (x._label = GetObjValJoin(x, ["canvasnbar_cd"])));
        this.CasvasnBarOptions.lists = res;
      },
    });

    this.$root.api.View_master_s_labelSearchActive({
      data: {},
      callback: (res) => {
        res.forEach((x) => (x._label = GetObjValJoin(x, ["label_cd"])));
        this.LabelOptions.lists = res;
      },
    });

    this.$root.api.View_master_s_producttypeSearch({
      data: {},
      callback: (res) => {
        res.forEach(
          (x) => (x._label = GetObjValJoin(x, ["producttypecode", "desc1"]))
        );
        this.ProductTypeOptions.lists = res;
      },
    });
  },
  mounted() {},
  methods: {
    checkedMinMax() {
      var prodSizeMin = this.ProductSizeMinOptions.selected;
      var prodSizeMax = this.ProductSizeMaxOptions.selected;

      if (prodSizeMin != null && prodSizeMax != null) {
        console.log(
          Number(prodSizeMax.twine_no) < Number(prodSizeMin.twine_no)
        );
        if (Number(prodSizeMax.twine_no) < Number(prodSizeMin.twine_no)) {
          swal("ผิดพลาด!", "เบอร์ใยสูงสุดต้องมากกว่าเบอร์ใยต่ำสุด", "error");
          this.ProductSizeMinOptions.selected = null;
          this.ProductSizeMaxOptions.selected = null;
        } else if (
          Number(prodSizeMax.twine_no) == Number(prodSizeMin.twine_no)
        ) {
          // // console.log(Number(prodSizeMax.line_no) < Number(prodSizeMin.line_no));
          if (Number(prodSizeMax.line_no) < Number(prodSizeMin.line_no)) {
            swal("ผิดพลาด!", "เบอร์ใยสูงสุดต้องมากกว่าเบอร์ใยต่ำสุด", "error");
            this.ProductSizeMinOptions.selected = null;
            this.ProductSizeMaxOptions.selected = null;
          }
        }
      }
    },
    checkedMinMaxinEdit() {
      var old_prodSizemin = [];
      var old_prodSizemax = [];

      var prodSizeMin = this.ProductSizeMinOptions.selected;
      var prodSizeMax = this.ProductSizeMaxOptions.selected;
      console.log("Max.twine_no", Number(prodSizeMax.twine_no));
      console.log("Min.twine_no", Number(prodSizeMin.twine_no));
      if (prodSizeMin != null && prodSizeMax != null) {
        if (Number(prodSizeMax.twine_no) < Number(prodSizeMin.twine_no)) {
          swal("ผิดพลาด!", "เบอร์ใยสูงสุดต้องมากกว่าเบอร์ใยต่ำสุด", "error");
          this.$root.api.View_master_s_productsizeSearch({
            data: { product_size: this.editViewFoldSpec[0].twinesize_min },
            callback: (res) => {
              res.forEach(
                (x) => (x._label = GetObjValJoin(x, ["product_size"]))
              );
              this.ProductSizeMinOptions.selected = res[0];
            },
          });

          this.$root.api.View_master_s_productsizeSearch({
            data: { product_size: this.editViewFoldSpec[0].twinesize_max },
            callback: (res) => {
              res.forEach(
                (x) => (x._label = GetObjValJoin(x, ["product_size"]))
              );
              this.ProductSizeMaxOptions.selected = res[0];
            },
          });
        } else if (
          Number(prodSizeMax.twine_no) == Number(prodSizeMin.twine_no)
        ) {
          // // console.log(Number(prodSizeMax.line_no) < Number(prodSizeMin.line_no));
          if (Number(prodSizeMax.line_no) < Number(prodSizeMin.line_no)) {
            swal("ผิดพลาด!", "เบอร์ใยสูงสุดต้องมากกว่าเบอร์ใยต่ำสุด", "error");
            this.$root.api.View_master_s_productsizeSearch({
              data: { product_size: this.editViewFoldSpec[0].twinesize_min },
              callback: (res) => {
                res.forEach(
                  (x) => (x._label = GetObjValJoin(x, ["product_size"]))
                );
                this.ProductSizeMinOptions.selected = res[0];
              },
            });

            this.$root.api.View_master_s_productsizeSearch({
              data: { product_size: this.editViewFoldSpec[0].twinesize_max },
              callback: (res) => {
                res.forEach(
                  (x) => (x._label = GetObjValJoin(x, ["product_size"]))
                );
                this.ProductSizeMaxOptions.selected = res[0];
              },
            });
          }
        }
      }
    },
    showHideRow(row) {
      $("#" + row).toggle();
    },
    FindCustomer() {
      this.CustomerOptions.selected = null;
      this.$root.api.View_master_s_cutomerSearch({
        data: { tsale: this.SaleValue.value },
        callback: (res) => {
          res.forEach(
            (x) => (x._label = GetObjValJoin(x, ["cuscod", "fname"]))
          );
          this.CustomerOptions.lists = res;
        },
      });
    },
    SearchCanvasandBarSpec() {
      this.dataCanvasnBarSpec = [];
      this.dataCancelCanvasnBarSpec = [];
      this.dataCanvasandBarSpecCount = 0;
      this.dataICanvasandBarSpecCount = 0;
      this.dataCCanvasandBarSpecCount = 0;
      this.checked = [];
      var x = document.getElementsByClassName("hidden_row");
      for (var i = 0; i < x.length; i++) {
        x[i].style.display = "none";
      }
      this.$root.api.Master_s_CanvasnBarSpecSearch({
        data: {
          cus_cod: this.search.cuscod_id,
          canvasnbar_cd: this.search.canvasnbar_cd,
          txtsearch: this.search.txtsearch,
        },
        callback: (res) => {
          if (res.length == 0) {
            swal({
              title: "ผิดพลาด",
              text: "ไม่พบรายการเงื่อนไขที่ค้นหา",
              icon: "warning",
              button: "ตกลง",
            });
          }
          this.dataCanvasnBarSpec = res;
          for (let i = 0; i < res.length; i++) {
            if (res[i].status == "A") {
              this.dataCanvasandBarSpecCount =
                this.dataCanvasandBarSpecCount + 1;
            } else if (res[i].status == "I") {
              this.dataICanvasandBarSpecCount =
                this.dataICanvasandBarSpecCount + 1;
            } else if (res[i].status == "C") {
              this.dataCCanvasandBarSpecCount =
                this.dataCCanvasandBarSpecCount + 1;
            }
          }
          this.getDateCanvasnBarSpec();
          this.setPagesActive();
          this.setPagesInactive();
          this.setPagesCancel();
        },
      });

      this.$root.api.Master_s_CanvasnBarSpecSearchCancel({
        data: {
          cus_cod: this.search.cuscod_id,
          canvasnbar_cd: this.search.canvasnbar_cd,
          txtsearch: this.search.txtsearch,
        },
        callback: (res) => {
          this.dataCancelCanvasnBarSpec = res;
          this.getCancelDateCanvasnBarSpec();
        },
      });
    },
    getDateCanvasnBarSpec() {
      this.dataActive = [];
      this.dataInactive = [];
      for (let i = 0; i <= this.dataCanvasnBarSpec.length; i++) {
        if (this.dataCanvasnBarSpec[i].status == "A") {
          this.dataActive.push({
            canvasnbar_cd: this.dataCanvasnBarSpec[i].canvasnbar_cd,
            canvasnbar_id: this.dataCanvasnBarSpec[i].canvasnbar_id,
            canvasnbar_name: this.dataCanvasnBarSpec[i].canvasnbar_name,
            canvasnbarspec_id: this.dataCanvasnBarSpec[i].canvasnbarspec_id,
            cus_cod: this.dataCanvasnBarSpec[i].cus_cod,
            cus_id: this.dataCanvasnBarSpec[i].cus_id,
            cus_name: this.dataCanvasnBarSpec[i].cus_name,
            effdate_en: this.dataCanvasnBarSpec[i].effdate_en,
            effdate_st: this.dataCanvasnBarSpec[i].effdate_st,
            status: this.dataCanvasnBarSpec[i].status,
            user_date: this.dataCanvasnBarSpec[i].user_date,
            user_id: this.dataCanvasnBarSpec[i].user_id,
            user_name: this.dataCanvasnBarSpec[i].user_name,
            tsale: this.dataCanvasnBarSpec[i].tsale,
            producttype:
              this.dataCanvasnBarSpec[i].producttype_code +
              ":" +
              this.dataCanvasnBarSpec[i].producttype_desc,
            min_twinesize: this.dataCanvasnBarSpec[i].twinesize_min,
            max_twinesize: this.dataCanvasnBarSpec[i].twinesize_max,
            label:
              this.dataCanvasnBarSpec[i].label_cd +
              ":" +
              this.dataCanvasnBarSpec[i].label_name,
          });
        } else if (this.dataCanvasnBarSpec[i].status == "I") {
          this.dataInactive.push({
            canvasnbar_cd: this.dataCanvasnBarSpec[i].canvasnbar_cd,
            canvasnbar_id: this.dataCanvasnBarSpec[i].canvasnbar_id,
            canvasnbar_name: this.dataCanvasnBarSpec[i].canvasnbar_name,
            canvasnbarspec_id: this.dataCanvasnBarSpec[i].canvasnbarspec_id,
            cus_cod: this.dataCanvasnBarSpec[i].cus_cod,
            cus_id: this.dataCanvasnBarSpec[i].cus_id,
            cus_name: this.dataCanvasnBarSpec[i].cus_name,
            effdate_en: this.dataCanvasnBarSpec[i].effdate_en,
            effdate_st: this.dataCanvasnBarSpec[i].effdate_st,
            status: this.dataCanvasnBarSpec[i].status,
            user_date: this.dataCanvasnBarSpec[i].user_date,
            user_id: this.dataCanvasnBarSpec[i].user_id,
            user_name: this.dataCanvasnBarSpec[i].user_name,
            tsale: this.dataCanvasnBarSpec[i].tsale,
            producttype:
              this.dataCanvasnBarSpec[i].producttype_code +
              ":" +
              this.dataCanvasnBarSpec[i].producttype_desc,
            min_twinesize: this.dataCanvasnBarSpec[i].twinesize_min,
            max_twinesize: this.dataCanvasnBarSpec[i].twinesize_max,
            label:
              this.dataCanvasnBarSpec[i].label_cd +
              ":" +
              this.dataCanvasnBarSpec[i].label_name,
          });
        }
      }
    },
    getCancelDateCanvasnBarSpec() {
      this.dataCancel = [];
      for (let i = 0; i <= this.dataCancelCanvasnBarSpec.length; i++) {
        this.dataCancel.push({
          canvasnbar_cd: this.dataCancelCanvasnBarSpec[i].canvasnbar_cd,
          canvasnbar_id: this.dataCancelCanvasnBarSpec[i].canvasnbar_id,
          canvasnbar_name: this.dataCancelCanvasnBarSpec[i].canvasnbar_name,
          canvasnbarspec_id: this.dataCancelCanvasnBarSpec[i].canvasnbarspec_id,
          cus_cod: this.dataCancelCanvasnBarSpec[i].cus_cod,
          cus_id: this.dataCancelCanvasnBarSpec[i].cus_id,
          cus_name: this.dataCancelCanvasnBarSpec[i].cus_name,
          effdate_en: this.dataCancelCanvasnBarSpec[i].effdate_en,
          effdate_st: this.dataCancelCanvasnBarSpec[i].effdate_st,
          status: this.dataCancelCanvasnBarSpec[i].status,
          user_date: this.dataCancelCanvasnBarSpec[i].user_date,
          user_id: this.dataCancelCanvasnBarSpec[i].user_id,
          user_name: this.dataCancelCanvasnBarSpec[i].user_name,
          tsale: this.dataCancelCanvasnBarSpec[i].tsale,
          cancel_date: this.dataCancelCanvasnBarSpec[i].cancel_date,
          cancel_name: this.dataCancelCanvasnBarSpec[i].cancel_name,
          producttype:
            this.dataCanvasnBarSpec[i].producttype_code +
            ":" +
            this.dataCanvasnBarSpec[i].producttype_desc,
          min_twinesize: this.dataCanvasnBarSpec[i].twinesize_min,
          max_twinesize: this.dataCanvasnBarSpec[i].twinesize_max,
          label:
            this.dataCanvasnBarSpec[i].label_cd +
            ":" +
            this.dataCanvasnBarSpec[i].label_name,
        });
      }
    },
    insertCanvasandBarSpecButton() {
      var twinesize_min = "";
      var min_product_type = "";
      var min_productcate_grp = "";
      var min_twine_no = "";
      var min_line_no = "";
      var min_word = "";
      var min_product_grp = "";
      if (this.ProductSizeMinOptions.selected == null) {
        twinesize_min = "";
        min_product_type = "";
        min_productcate_grp = "";
        min_twine_no = 0;
        min_line_no = 0;
        min_word = "";
        min_product_grp = "";
      } else {
        twinesize_min = this.ProductSizeMinOptions.selected.product_size;
        min_product_type = this.ProductSizeMinOptions.selected.product_type;
        min_productcate_grp = this.ProductSizeMinOptions.selected.productcate;
        min_twine_no = this.ProductSizeMinOptions.selected.twine_no;
        min_line_no = this.ProductSizeMinOptions.selected.line_no;
        min_word = this.ProductSizeMinOptions.selected.word;
        min_product_grp = this.ProductSizeMinOptions.selected.product_grp;
      }

      var twinesize_max = "";
      var max_product_type = "";
      var max_productcate_grp = "";
      var max_twine_no = "";
      var max_line_no = "";
      var max_word = "";
      var max_product_grp = "";
      if (this.ProductSizeMaxOptions.selected == null) {
        twinesize_max = "";
        max_product_type = "";
        max_productcate_grp = "";
        max_twine_no = 0;
        max_line_no = 0;
        max_word = "";
        max_product_grp = "";
      } else {
        twinesize_max = this.ProductSizeMaxOptions.selected.product_size;
        max_product_type = this.ProductSizeMaxOptions.selected.product_type;
        max_productcate_grp = this.ProductSizeMaxOptions.selected.productcate;
        max_twine_no = this.ProductSizeMaxOptions.selected.twine_no;
        max_line_no = this.ProductSizeMaxOptions.selected.line_no;
        max_word = this.ProductSizeMaxOptions.selected.word;
        max_product_grp = this.ProductSizeMaxOptions.selected.product_grp;
      }

      if (
        this.SaleValue.value != undefined &&
        this.CasvasnBarOptions.selected != null &&
        this.CustomerOptions.selected != null &&
        this.LabelOptions.selected != null &&
        this.ProductTypeOptions.selected != null
      ) {
        if (
          this.insertSpec.canvasnbarspec_en == "2000-01-01" ||
          (this.insertSpec.canvasnbarspec_en >
            this.insertSpec.canvasnbarspec_st &&
            this.insertSpec.canvasnbarspec_en > this.thisNowDate)
        ) {
          this.$root.api.Master_s_CanvasnBarSpecSave({
            data: {
              canvasnbar_id: this.CasvasnBarOptions.selected.canvasnbar_id,
              canvasnbar_cd: this.CasvasnBarOptions.selected.canvasnbar_cd,
              canvasnbar_name: this.CasvasnBarOptions.selected.canvasnbar_name.trim(),
              cus_cod: this.CustomerOptions.selected.cuscod.trim(),
              cus_name: this.CustomerOptions.selected.fname.trim(),
              status: "A",
              effdate_st: this.insertSpec.canvasnbarspec_st,
              effdate_en: this.insertSpec.canvasnbarspec_en,
              user_id: localStorage.getItem("UserID"),
              user_name: localStorage.getItem("User"),
              tsale: this.SaleValue.value,
              producttype_code: this.ProductTypeOptions.selected
                .producttypecode,
              producttype_desc: this.ProductTypeOptions.selected.desc1,
              productcate_grp: this.ProductTypeOptions.selected.productcate_grp,
              twinesize_min: twinesize_min,
              twinesize_max: twinesize_max,
              min_product_type: min_product_type,
              min_productcate_grp: min_productcate_grp,
              min_twine_no: min_twine_no,
              min_line_no: min_line_no,
              min_word: min_word,
              min_product_grp: min_product_grp,

              max_product_type: max_product_type,
              max_productcate_grp: max_productcate_grp,
              max_twine_no: max_twine_no,
              max_line_no: max_line_no,
              max_word: max_word,
              max_product_grp: max_product_grp,
              label_id: this.LabelOptions.selected.label_id,
              label_cd: this.LabelOptions.selected.label_cd,
              label_name: this.LabelOptions.selected.label_name_th,
            },
            callback: (res) => {
              if (res._status == "S") {
                swal({
                  title: "สำเร็จ",
                  text:
                    "บันทึกข้อมูลมาตรฐานเงื่อนไข \n การใช้ผ้าใบและแถบสีเสร็จสิ้น",
                  icon: "success",
                  button: "ตกลง",
                });
              } else if (res._status == "F") {
                swal({
                  title: "ผิดพลาด!",
                  text: "ไม่สามารถบันทึกข้อมูลได้",
                  icon: "error",
                  button: "ตกลง",
                });
              }
              this.resetData();
              this.SearchCanvasandBarSpec();
              this.openAddModal = false;
            },
          });
        } else {
          swal({
            title: "ผิดพลาด!",
            text: "วันหมดอายุจะต้องมากกว่าวันที่ประกาศใช้และวันปัจจุบัน",
            icon: "error",
            button: "ตกลง",
          });
        }
      } else {
        swal({
          title: "ผิดพลาด!",
          text: "กรุณากรอกข้อมูลให้ครบถ้วน",
          icon: "error",
          button: "ตกลง",
        });
      }
    },
    checkItems(val) {
      this.checkedSpec = $("input[type=checkbox][name=Choosed]:checked")
        .map(function(_, el) {
          return $(el).val();
        })
        .get();
    },
    EditCanvasandBarSpecButton() {
      this.openEditModal = true;
      var x = this.checkedSpec[0];
      this.$root.api.Master_s_CanvasnBarSpecSearch({
        data: {
          canvasnbarspec_id: x,
        },
        callback: (res) => {
            this.editSpec =  res[0]
          this.editSpec.canvasnbarspec_st = res[0].effdate_st.split("T")[0];
          this.editSpec.canvasnbarspec_en = res[0].effdate_en.split("T")[0];
          var tsaleValue = res[0].tsale;
          if (tsaleValue == "0") {
            this.SaleValue = { value: "0", name: "ขายในประเทศ" };
            this.editSpec.tsale = "ขายในประเทศ";
          } else {
            this.SaleValue = { value: "1", name: "ขายต่างประเทศ" };
            this.editSpec.tsale = "ขายต่างประเทศ";
          }

          this.editSpec.producttype = res[0].producttype_code + " : " + res[0].producttype_desc;

          this.$root.api.View_master_s_cutomerSearch({
            data: { cuscod: res[0].cus_cod },
            callback: (res) => {
              res.forEach(
                (x) => (x._label = GetObjValJoin(x, ["cuscod", "fname"]))
              );
              this.CustomerOptions.selected = res[0];
              this.editSpec.customer =
                res[0].cuscod.trim() + " : " + res[0].fname;
            },
          });

          this.$root.api.View_master_s_cutomerSearch({
            data: { tsale: this.SaleValue.value },
            callback: (res) => {
              res.forEach(
                (x) => (x._label = GetObjValJoin(x, ["cuscod", "fname"]))
              );
              this.CustomerOptions.lists = res;
            },
          });

          this.$root.api.Master_s_CanvasnBarSearchActive({
            data: { canvasnbar_id: res[0].canvasnbar_id },
            callback: (res) => {
              res.forEach(
                (x) =>
                  (x._label = GetObjValJoin(x, [
                    "canvasnbar_cd",
                    "canvasnbar_name",
                  ]))
              );
              this.CasvasnBarOptions.selected = res[0];
              this.editSpec.canvasnbar = res[0]._label;
            },
          });

          this.$root.api.View_master_s_labelSearchActive({
            data: { label_id : res[0].label_id },
            callback: (res) => {
              res.forEach((x) => (x._label = GetObjValJoin(x, ["label_cd"])));
              this.LabelOptions.selected = res[0]
            },
          });

          this.$root.api.View_master_s_producttypeSearch({
            data: {  producttype_code: res[0].producttype_code  },
            callback: (res) => {
              res.forEach(
                (x) =>
                  (x._label = GetObjValJoin(x, ["producttypecode", "desc1"]))
              );
              this.ProductTypeOptions.selected = res[0]
            },
          });

          if (res[0].roductcate_grp == "H") {
            this.$root.api.View_master_s_productsizeSearch({
              data: {},
              callback: (res) => {
                res.forEach((x) => (x._label = GetObjValJoin(x, ["product_size"])));
                this.ProductSizeMinOptions.lists = res;
                this.ProductSizeMaxOptions.lists = res;
              },
            });
          } else {
            this.$root.api.View_master_s_productsizeSearch({
              data: {
                productcate_grp: res[0].roductcate_grp,
              },
              callback: (res) => {
                res.forEach((x) => (x._label = GetObjValJoin(x, ["product_size"])));
                this.ProductSizeMinOptions.lists = res;
                this.ProductSizeMaxOptions.lists = res;
              },
            });
          }

          this.$root.api.View_master_s_productsizeSearch({
            data: { product_size: res[0].twinesize_min },
            callback: (res) => {
              res.forEach(
                (x) => (x._label = GetObjValJoin(x, ["product_size"]))
              );
              this.ProductSizeMinOptions.selected = res[0];
            },
          });

          this.$root.api.View_master_s_productsizeSearch({
            data: { product_size: res[0].twinesize_max },
            callback: (res) => {
              res.forEach(
                (x) => (x._label = GetObjValJoin(x, ["product_size"]))
              );
              this.ProductSizeMaxOptions.selected = res[0];
            },
          });       
        },
      });
    },
    SaveEditCanvasandBarSpecButton() {
      var x = this.checkedSpec[0];

      var twinesize_min = "";
      var min_product_type = "";
      var min_productcate_grp = "";
      var min_twine_no = "";
      var min_line_no = "";
      var min_word = "";
      var min_product_grp = "";
      if (this.ProductSizeMinOptions.selected == null) {
        twinesize_min = this.editSpec.product_size;
        min_product_type = this.editSpec.product_type;
        min_productcate_grp = this.editSpec.productcate;
        min_twine_no = this.editSpec.twine_no;
        min_line_no = this.editSpec.line_no;
        min_word = this.editSpec.word;
        min_product_grp = this.editSpec.product_grp;
      } else {
        twinesize_min = this.ProductSizeMinOptions.selected.product_size;
        min_product_type = this.ProductSizeMinOptions.selected.product_type;
        min_productcate_grp = this.ProductSizeMinOptions.selected.productcate;
        min_twine_no = this.ProductSizeMinOptions.selected.twine_no;
        min_line_no = this.ProductSizeMinOptions.selected.line_no;
        min_word = this.ProductSizeMinOptions.selected.word;
        min_product_grp = this.ProductSizeMinOptions.selected.product_grp;
      }

      var twinesize_max = "";
      var max_product_type = "";
      var max_productcate_grp = "";
      var max_twine_no = "";
      var max_line_no = "";
      var max_word = "";
      var max_product_grp = "";
      if (this.ProductSizeMaxOptions.selected == null) {
        twinesize_max = this.editSpec.product_size;
        max_product_type = this.editSpec.product_type;
        max_productcate_grp = this.editSpec.productcate;
        max_twine_no = this.editSpec.twine_no;
        max_line_no = this.editSpec.line_no;
        max_word = this.editSpec.word;
        max_product_grp = this.editSpec.product_grp;
      } else {
        twinesize_max = this.ProductSizeMaxOptions.selected.product_size;
        max_product_type = this.ProductSizeMaxOptions.selected.product_type;
        max_productcate_grp = this.ProductSizeMaxOptions.selected.productcate;
        max_twine_no = this.ProductSizeMaxOptions.selected.twine_no;
        max_line_no = this.ProductSizeMaxOptions.selected.line_no;
        max_word = this.ProductSizeMaxOptions.selected.word;
        max_product_grp = this.ProductSizeMaxOptions.selected.product_grp;
      }

      if (
        this.SaleValue.value != undefined &&
        this.CasvasnBarOptions.selected != null &&
        this.CustomerOptions.selected != null &&
        this.LabelOptions.selected != null &&
        this.ProductTypeOptions.selected != null
      ) {
        if (
          this.editSpec.canvasnbarspec_en == "2000-01-01" ||
          (this.editSpec.canvasnbarspec_en > this.editSpec.canvasnbarspec_st &&
            this.editSpec.canvasnbarspec_en > this.thisNowDate)
        ) {
          this.$root.api.Master_s_CanvasnBarSpecSave({
            data: {
              canvasnbarspec_id: x,
              canvasnbar_id: this.CasvasnBarOptions.selected.canvasnbar_id,
              canvasnbar_cd: this.CasvasnBarOptions.selected.canvasnbar_cd,
              canvasnbar_name: this.CasvasnBarOptions.selected.canvasnbar_name.trim(),
              cus_cod: this.CustomerOptions.selected.cuscod.trim(),
              cus_name: this.CustomerOptions.selected.fname.trim(),
              status: "A",
              effdate_st: this.editSpec.canvasnbarspec_st,
              effdate_en: this.editSpec.canvasnbarspec_en,
              user_id: localStorage.getItem("UserID"),
              user_name: localStorage.getItem("User"),
              tsale: this.SaleValue.value,

              producttype_code: this.ProductTypeOptions.selected
                .producttypecode,
              producttype_desc: this.ProductTypeOptions.selected.desc1,
              productcate_grp: this.ProductTypeOptions.selected.productcate_grp,
              twinesize_min: twinesize_min,
              twinesize_max: twinesize_max,
              min_product_type: min_product_type,
              min_productcate_grp: min_productcate_grp,
              min_twine_no: min_twine_no,
              min_line_no: min_line_no,
              min_word: min_word,
              min_product_grp: min_product_grp,

              max_product_type: max_product_type,
              max_productcate_grp: max_productcate_grp,
              max_twine_no: max_twine_no,
              max_line_no: max_line_no,
              max_word: max_word,
              max_product_grp: max_product_grp,
              label_id: this.LabelOptions.selected.label_id,
              label_cd: this.LabelOptions.selected.label_cd,
              label_name: this.LabelOptions.selected.label_name_th,
            },
            callback: (res) => {
              if (res._status == "S") {
                swal({
                  title: "สำเร็จ",
                  text:
                    "บันทึกข้อมูลมาตรฐานเงื่อนไข \n การใช้ผ้าใบและแถบสีเสร็จสิ้น",
                  icon: "success",
                  button: "ตกลง",
                });
              } else if (res._status == "F") {
                swal({
                  title: "ผิดพลาด!",
                  text: "ไม่สามารถบันทึกข้อมูลได้",
                  icon: "error",
                  button: "ตกลง",
                });
              }
              this.resetData();
              this.SearchCanvasandBarSpec();
              this.openEditModal = false;
            },
          });
        } else {
          swal({
            title: "ผิดพลาด!",
            text: "วันหมดอายุจะต้องมากกว่าวันที่ประกาศใช้และวันปัจจุบัน",
            icon: "error",
            button: "ตกลง",
          });
        }
      } else {
        swal({
          title: "ผิดพลาด!",
          text: "กรุณากรอกข้อมูลให้ครบถ้วน",
          icon: "error",
          button: "ตกลง",
        });
      }
    },
    ViewCanvasandBarSpecButton() {
      this.openViewModal = true;
      var x = this.checkedSpec[0];
      this.$root.api.Master_s_CanvasnBarSpecSearch({
        data: {
          canvasnbarspec_id: x,
        },
        callback: (res) => {
          this.viewSpec = res[0];
          if (res[0].tsale == "0") {
            this.viewSpec.tsale = "ขายในประเทศ";
          } else {
            this.viewSpec.tsale = "ขายต่างประเทศ";
          }
          this.viewSpec.customer = res[0].cus_cod + " : " + res[0].cus_name;
          //   this.viewSpec.canvasnbar =
          //     res[0].canvasnbar_cd + " : " + res[0].canvasnbar_name;
          this.viewSpec.canvasnbarspec_st = res[0].effdate_st.split("T")[0];
          this.viewSpec.canvasnbarspec_en = res[0].effdate_en.split("T")[0];

          this.viewSpec.producttype =
            res[0].producttype_code + " : " + res[0].producttype_desc;
        },
      });
    },
    ViewDeleteCanvasandBarSpec() {
      this.openDeleteModal = true;
      var x = this.checkedSpec[0];
      this.$root.api.Master_s_CanvasnBarSpecSearch({
        data: {
          canvasnbarspec_id: x,
        },
        callback: (res) => {
            this.deleteSpec = res[0]
          this.deleteSpec.customer = res[0].cus_cod + " : " + res[0].cus_name;
          this.deleteSpec.canvasnbar =
            res[0].canvasnbar_cd + " : " + res[0].canvasnbar_name;
        },
      });
    },
    DeleteCanvasandBarSpecButton() {
      var x = this.checkedSpec[0];
      this.$root.api.Master_s_CanvasnBarSpecMove({
        data: {
          canvasnbarspec_move: [
            {
              canvasnbarspec_id: x,
              user_id: localStorage.getItem("UserID"),
              user_name: localStorage.getItem("User"),
            },
          ],
        },
        callback: (res) => {
          if (res[0]._status == "S") {
            swal({
              title: "สำเร็จ",
              text: "ลบข้อมูลผ้าใบเสร็จสิ้น",
              icon: "success",
              button: "ตกลง",
            });
          } else {
            swal({
              title: "ผิดพลาด!",
              text: "ไม่สามารถลบข้อมูลผ้าใบได้",
              icon: "error",
              button: "ตกลง",
            });
          }
          this.openDeleteModal = false;
          this.SearchCanvasandBarSpec();
        },
      });
    },
    FindLineTypeOptions() {
      this.ProductSizeMinOptions.selected = null;
      this.ProductSizeMaxOptions.selected = null;

      if (this.ProductTypeOptions.selected.productcate_grp == "H") {
        this.$root.api.View_master_s_productsizeSearch({
          data: {},
          callback: (res) => {
            res.forEach((x) => (x._label = GetObjValJoin(x, ["product_size"])));
            this.ProductSizeMinOptions.lists = res;
            this.ProductSizeMaxOptions.lists = res;
          },
        });
      } else {
        this.$root.api.View_master_s_productsizeSearch({
          data: {
            productcate_grp: this.ProductTypeOptions.selected.productcate_grp,
          },
          callback: (res) => {
            res.forEach((x) => (x._label = GetObjValJoin(x, ["product_size"])));
            this.ProductSizeMinOptions.lists = res;
            this.ProductSizeMaxOptions.lists = res;
          },
        });
      }
    },
    resetData() {
      this.SaleValue = null;
      this.CustomerOptions.selected = null;
      this.CasvasnBarOptions.selected = null;
      this.insertSpec.canvasnbarspec_st = this.currentDate;
      this.insertSpec.canvasnbarspec_en = this.defaultExDate;
      this.ProductTypeOptions.selected = null;
      this.ProductSizeMinOptions.selected = null;
      this.ProductSizeMaxOptions.selected = null;
      this.LabelOptions.selected = null;
    },
    setPagesActive() {
      this.pagesA = [];
      let numberOfPages = Math.ceil(
        this.dataCanvasandBarSpecCount / this.perPageA
      );
      for (let index = 0; index < numberOfPages; index++) {
        this.pagesA.push(index);
      }
      this.pageA = 1;
    },
    setPagesInactive() {
      this.pagesI = [];
      let numberOfPages = Math.ceil(
        this.dataICanvasandBarSpecCount / this.perPageI
      );
      for (let index = 0; index < numberOfPages; index++) {
        this.pagesI.push(index);
      }
      this.pageI = 1;
    },
    setPagesCancel() {
      this.pagesC = [];
      let numberOfPages = Math.ceil(
        this.dataCCanvasandBarSpecCount / this.perPageI
      );
      for (let index = 0; index < numberOfPages; index++) {
        this.pagesC.push(index);
      }
      this.pageC = 1;
    },
    paginate(Active) {
      let pageA = this.pageA;
      let perPageA = this.perPageA;
      let from = pageA * perPageA - perPageA;
      let to = pageA * perPageA;
      return Active.slice(from, to);
    },
    paginateInactive(Inactive) {
      let pageI = this.pageI;
      let perPageI = this.perPageI;
      let from = pageI * perPageI - perPageI;
      let to = pageI * perPageI;
      return Inactive.slice(from, to);
    },
    paginateCancel(Cancel) {
      let pageC = this.pageC;
      let perPageC = this.perPageC;
      let from = pageC * perPageC - perPageC;
      let to = pageC * perPageC;
      return Cancel.slice(from, to);
    },
  },
  updated() {
    $(document).ready(function() {
      $("#MyTable").DataTable({
        initComplete: function() {
          this.api()
            .columns()
            .every(function() {
              var column = this;
              var select = $('<select><option value=""></option></select>')
                .appendTo($(column.footer()).empty())
                .on("change", function() {
                  var val = $.fn.dataTable.util.escapeRegex($(this).val());
                  //to select and search from grid
                  column.search(val ? "^" + val + "$" : "", true, false).draw();
                });

              column
                .data()
                .unique()
                .sort()
                .each(function(d, j) {
                  select.append('<option value="' + d + '">' + d + "</option>");
                });
            });
        },
      });
    });
  },
  computed: {
    displayedPostsActive() {
      return this.paginate(this.dataActive);
    },
    displayedPostsInactive() {
      return this.paginateInactive(this.dataInactive);
    },
    displayedPostsCancel() {
      return this.paginateCancel(this.dataCancel);
    },
  },
  watch: {
    Active() {
      this.setPagesActive();
    },
    Inactive() {
      this.setPagesIntive();
    },
    Cancel() {
      this.setPagesCancel();
    },
  },
};
