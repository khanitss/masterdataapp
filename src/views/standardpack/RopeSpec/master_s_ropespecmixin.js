import Multiselect from "vue-multiselect";
import {
  GetObjVal,
  SetObjVal,
  ObjCopy,
  ObjResetValue,
  ObjCopyValue,
  GetObjValJoin,
  GetObjArr,
  GetLastUpdate,
  GetDate,
  GetDateView,
  FileToBase64,
} from "@/shared/utils";
import swal from "sweetalert";
import DatePicker from "vue2-datepicker";
import "vue2-datepicker/index.css";

export default {
  components: {
    Multiselect,
    DatePicker,
  },
  data() {
    return {
      HReport: true,
      search: {
        txtSearch: "",
        codeSearch: "",
      },
      SaleOptions: [
        { value: "0", name: "ขายในประเทศ" },
        { value: "1", name: "ขายต่างประเทศ" },
      ],
      SaleValue: {},

      CustomerOptions: {
        lists: [],
        selected: null,
      },
      ProductTypeOptions: {
        lists: [],
        selected: null,
      },
      RopeOptions: {
        lists: [],
        selected: null,
      },

      DataTable: [],
      DataSubTable: [],

      // Active
      dataActive: [],
      dataActiveCount: 0,
      pageA: 1,
      perPageA: 10,
      pagesA: [],

      // InActive
      dataInactive: [],
      dataInactiveCount: 0,
      pageI: 1,
      perPageI: 5,
      pagesI: [],

      // Cancel
      dataCancel: [],
      dataCancelCount: 0,
      pageC: 1,
      perPageC: 5,
      pagesC: [],

      checked: [],
      checkedItem: "",
      checkedSpec: null,

      itemsTabs1: true,
      itemsTabs2: false,
      itemsTabs3: false,

      openAddModal: false,
      openEditModal: false,
      openViewModal: false,
      openDeleteModal: false,
      insertData: {
        effdate_st: "",
        effdate_en: "",
      },
      EditData: {
        tsale: "",
        tsaleType: "",
        customer: "",
        productype: "",
        rope_cd: "",
        rope_name: "",
        effdate_st: "",
        effdate_en: "",
        path_pic: "",
        producttype_code: "",
      },
      ViewData: {
        tsale: "",
        customer: "",
        productype: "",
        rope_cd: "",
        rope_name: "",
        effdate_st: "",
        effdate_en: "",
        path_pic: "",
        producttype_code: "",
      },
      viewImgData: "",
      DeleteData: {},
      nowDate: "",
      old_effdate_st: "",
    };
  },
  updated() {},
  created() {
    let nowDate = new Date();
    this.nowDate = GetDate(nowDate.setDate(nowDate.getDate()));
    this.insertData.effdate_st = this.nowDate;
    this.insertData.effdate_en = "2000-01-01";

    this.$root.api.View_master_s_producttypeSearch({
      data: {},
      callback: (res) => {
        res.forEach(
          (x) => (x._label = GetObjValJoin(x, ["producttypecode", "desc1"]))
        );
        this.ProductTypeOptions.lists = res;
      },
    });
    this.$root.api.Master_s_RopeSearchActive({
      data: {},
      callback: (res) => {
        res.forEach((x) => (x._label = GetObjValJoin(x, ["rope_cd"])));
        this.RopeOptions.lists = res;
      },
    });
  },
  methods: {
    SearchData() {
      this.DataTable = [];
      this.DataCancelTable = [];
      this.dataActiveCount = 0;
      this.dataInactiveCount = 0;
      this.dataCancelCount = 0;
      this.checked = [];
      this.checkedItem = "";
      this.checkedSpec = [];
      this.DataSubTable = [];

      this.$root.api.Master_s_RopeSpecSearchCus({
        data: {
          codeSearch: this.search.codeSearch,
          txtSearch: this.search.txtSearch,
        },
        callback: (res) => {
          this.DataTable = res;
          for (let i = 0; i < res.length; i++) {
            if (res[i].status == "A") {
              this.dataActiveCount = this.dataActiveCount + 1;
            } else if (res[i].status == "I") {
              this.dataInactiveCount = this.dataInactiveCount + 1;
            } else if (res[i].status == "C") {
              this.dataCancelCount = this.dataCancelCount + 1;
            }
          }
          if (res.length == 0) {
            swal(
              "ไม่พบเงื่อนไขการเชือกและฟางที่ค้นหา",
              "กรุณาใส่รหัสลูกค้าหรือรหัสการเชือกและฟางใหม่",
              "warning"
            );
            this.search = [];
          }
          this.getDataTable();
          this.setPagesActive();
          this.setPagesInactive();
          this.setPagesCancel();
        },
      });
    },
    getDataTable() {
      this.dataActive = [];
      this.dataInactive = [];
      this.dataCancel = [];
      for (let i = 0; i < this.DataTable.length; i++) {
        if (this.DataTable[i].status == "A") {
          this.dataActive.push({
            cus_cod: this.DataTable[i].cus_cod,
            cus_id: this.DataTable[i].cus_id,
            tsale: this.DataTable[i].tsale,
            cus_name: this.DataTable[i].cus_name,
            status: this.DataTable[i].status,
          });
        } else if (this.DataTable[i].status == "I") {
          this.dataInactive.push({
            cus_cod: this.DataTable[i].cus_cod,
            cus_id: this.DataTable[i].cus_id,
            tsale: this.DataTable[i].tsale,
            cus_name: this.DataTable[i].cus_name,
            status: this.DataTable[i].status,
          });
        } else if (this.DataTable[i].status == "C") {
          this.dataCancel.push({
            cus_cod: this.DataTable[i].cus_cod,
            cus_id: this.DataTable[i].cus_id,
            tsale: this.DataTable[i].tsale,
            cus_name: this.DataTable[i].cus_name,
            status: this.DataTable[i].status,
          });
        }
      }
    },
    FindByCuscod(cuscod_id) {
      this.checkedSpec = [];
      this.$root.api.Master_s_RopeSpecSearch({
        data: {
          cus_cod: cuscod_id,
        },
        callback: (res) => {
          console.log(res);
          this.DataSubTable = res;
        },
      });
    },
    FindbyRopeSpecID(val) {
      this.checkedItem = "";
      if ($("input.checkbox_check").is(":checked")) {
        this.checkedItem = $("input[type=checkbox][name=Choosed]:checked")
          .map(function(_, el) {
            return $(el).val();
          })
          .get();
      }
      // console.log(this.checkedItem);
    },
    InsertSaveButton() {
      console.log(this.ProductTypeOptions.selected);
      console.log(this.RopeOptions.selected);
      if (
        this.SaleValue != null &&
        this.CustomerOptions.selected != null &&
        this.ProductTypeOptions.selected != null &&
        this.RopeOptions.selected != null
      ) {
        if (
          this.insertData.effdate_en == "2000-01-01" ||
          (this.insertData.effdate_en > this.nowDate &&
            this.insertData.effdate_en > this.insertData.effdate_st)
        ) {
          this.$root.api.Master_s_RopeSpecSave({
            data: {
              tsale: this.SaleValue.value,
              cus_cod: this.CustomerOptions.selected.cuscod,
              cus_name: this.CustomerOptions.selected.fname,
              produccttype_code: this.ProductTypeOptions.selected
                .producttypecode,
              producttype_desc: this.ProductTypeOptions.selected.desc1,
              rope_id: this.RopeOptions.selected.rope_id,
              rope_cd: this.RopeOptions.selected.rope_cd,
              rope_name: this.RopeOptions.selected.rope_name,
              status: "A",
              effdate_st: this.insertData.effdate_st,
              effdate_en: this.insertData.effdate_en,
              user_id: localStorage.getItem("UserID"),
              user_name: localStorage.getItem("User"),
            },
            callback: (res) => {
              console.log(res);
              if (res.status == "S") {
                swal({
                  title: "สำเร็จ",
                  text:
                    "บันทึกข้อมูลข้อมูลมาตราฐานเงื่อนไขเชือกและฟางเสร็จสิ้น",
                  icon: "success",
                  button: "ตกลง",
                });
                this.SearchData();
                this.resetData();
                this.openAddModal = false;
              } else if (res.status == "F") {
                swal({
                  title: "ผิดพลาด!",
                  text:
                    "ไม่สามารถบันทึกข้อมูลข้อมูลมาตราฐานเงื่อนไขเชือกและฟางได้",
                  icon: "error",
                  button: "ตกลง",
                });
                this.resetData();
                this.openAddModal = false;
              }
            },
          });
        } else {
          swal({
            title: "ข้อมูลไม่ถูกต้อง!",
            text: "วันหมดอายุจะต้องมากกว่าวันที่ประกาศใช้และวันปัจจุบัน",
            icon: "error",
            button: "ตกลง",
          });
        }
      } else {
        swal({
          title: "ผิดพลาด",
          text:
            "กรุณากรอกข้อมูลมาตราฐานเงื่อนไข \n การใช้เชือกและฟางให้ครบถ้วน",
          icon: "error",
          button: "ตกลง",
        });
      }
    },
    EditButton() {
      this.openEditModal = true;
      var x = this.checkedItem[0];
      console.log(x);
      this.$root.api.Master_s_RopeSpecSearch({
        data: {
          ropespec_id: x,
        },
        callback: (res) => {
          this.EditData = res[0];
          this.EditData.productype =
            res[0].produccttype_code.trim() +
            " : " +
            res[0].producttype_desc.trim();
          this.EditData.customer = res[0].cus_cod.trim();
          // this.EditData.producttype_code = res[0].produccttype_code;
          this.EditData.rope_cd = res[0].rope_cd;
          this.EditData.rope_name = res[0].rope_name;
          this.EditData.effdate_st = res[0].effdate_st.split("T")[0];
          this.EditData.effdate_en = res[0].effdate_en.split("T")[0];

          if (res[0].tsale == 1) {
            this.SaleValue = { value: "1", name: "ขายต่างประเทศ" };
            this.EditData.tsaleType = "ขายต่างประเทศ";
          } else if (res[0].tsale == 0) {
            this.SaleValue = { value: "0", name: "ขายในประเทศ" };
            this.EditData.tsaleType = "ขายในประเทศ";
          }

          this.$root.api.View_master_s_cutomerSearch({
            data: { tsale: res[0].tsale },
            callback: (res) => {
              res.forEach(
                (x) => (x._label = GetObjValJoin(x, ["cuscod", "fname"]))
              );
              this.CustomerOptions.lists = res;
            },
          });

          this.$root.api.View_master_s_cutomerSearch({
            data: { cuscod: res[0].cus_cod },
            callback: (res) => {
              res.forEach(
                (x) => (x._label = GetObjValJoin(x, ["cuscod", "fname"]))
              );
              this.CustomerOptions.selected = res[0];
            },
          });

          this.$root.api.View_master_s_producttypeSearch({
            data: { producttype_code: res[0].produccttype_code },
            callback: (res) => {
              res.forEach(
                (x) =>
                  (x._label = GetObjValJoin(x, ["producttypecode", "desc1"]))
              );
              this.ProductTypeOptions.selected = res[0];
            },
          });

          this.$root.api.Master_s_RopeSearch({
            data: { rope_id: res[0].rope_id },
            callback: (res) => {
              res.forEach((x) => (x._label = GetObjValJoin(x, ["rope_cd"])));
              this.RopeOptions.selected = res[0];
              console.log(res);
              this.EditData.path_pic = res[0].path_pic;
            },
          });
        },
      });
    },
    EditSaveButton() {
      console.log(this.EditData);
      var tsale = "";
      if (this.SaleValue == null) {
        tsale = this.EditData.tsale;
      } else {
        tsale = this.SaleValue.value;
      }
      console.log(this.EditData.cus_cod);
      var cuscod = "";
      var cusname = "";
      if (this.CustomerOptions.selected == null) {
        cuscod = this.EditData.cus_cod;
        cusname = this.EditData.cus_name;
      } else {
        cuscod = this.CustomerOptions.selected.cuscod;
        cusname = this.CustomerOptions.selected.fname;
      }

      var produccttype_code = "";
      var producttype_desc = "";
      if (this.ProductTypeOptions.selected == null) {
        produccttype_code = this.EditData.produccttype_code.trim();
        producttype_desc = this.EditData.producttype_desc.trim();
      } else {
        produccttype_code = this.ProductTypeOptions.selected.producttypecode;
        producttype_desc = this.ProductTypeOptions.selected.desc1;
      }

      var rope_id = "";
      var rope_cd = "";
      var rope_name = "";
      if (this.RopeOptions.selected == null) {
        rope_id = this.EditData.rope_id;
        rope_cd = this.EditData.rope_cd;
        rope_name = this.EditData.rope_name;
      } else {
        rope_id = this.RopeOptions.selected.rope_id;
        rope_cd = this.RopeOptions.selected.rope_cd;
        rope_name = this.RopeOptions.selected.rope_name;
      }

      if (
        this.EditData.effdate_en == "2000-01-01" ||
        (this.EditData.effdate_en > this.nowDate &&
          this.EditData.effdate_en > this.EditData.effdate_st)
      ) {
        this.$root.api.Master_s_RopeSpecSave({
          data: {
            ropespec_id: this.EditData.ropespec_id,
            tsale: tsale,
            cus_cod: cuscod,
            cus_name: cusname,
            produccttype_code: produccttype_code,
            producttype_desc: producttype_desc,
            rope_id: rope_id,
            rope_cd: rope_cd,
            rope_name: rope_name,
            status: "A",
            effdate_st: this.EditData.effdate_st,
            effdate_en: this.EditData.effdate_en,
            user_id: localStorage.getItem("UserID"),
            user_name: localStorage.getItem("User"),
          },
          callback: (res) => {
            console.log(res);
            if (res.status == "S") {
              swal({
                title: "สำเร็จ",
                text: "บันทึกข้อมูลข้อมูลมาตราฐานเงื่อนไขเชือกและฟางเสร็จสิ้น",
                icon: "success",
                button: "ตกลง",
              });
              this.SearchData();
              this.resetData();
              this.openEditModal = false;
            } else if (res.status == "F") {
              swal({
                title: "ผิดพลาด!",
                text:
                  "ไม่สามารถบันทึกข้อมูลข้อมูลมาตราฐานเงื่อนไขเชือกและฟางได้",
                icon: "error",
                button: "ตกลง",
              });
              this.resetData();
              this.openEditModal = false;
            }
          },
        });
      } else {
        swal({
          title: "ข้อมูลไม่ถูกต้อง!",
          text: "วันหมดอายุจะต้องมากกว่าวันที่ประกาศใช้และวันปัจจุบัน",
          icon: "error",
          button: "ตกลง",
        });
      }
    },

    ViewButton() {
      this.ViewData.path_pic = "";

      var x = this.checkedItem[0];
      this.$root.api.Master_s_RopeSpecSearch({
        data: {
          ropespec_id: x,
        },
        callback: (res) => {
          this.ViewData = res[0];

          if (res[0].tsale == 1) {
            this.ViewData.tsale = "ขายต่างประเทศ";
          } else if (res[0].tsale == 0) {
            this.ViewData.tsale = "ขายในประเทศ";
          }

          //   this.ViewData.productype = res[0].produccttype_code.trim();
          this.ViewData.productype =
            res[0].produccttype_code.trim() + " : " + res[0].producttype_desc;
          this.ViewData.rope_cd = res[0].rope_cd;
          this.ViewData.rope_name = res[0].rope_name;
          this.ViewData.effdate_st = res[0].effdate_st.split("T")[0];
          this.ViewData.effdate_en = res[0].effdate_en.split("T")[0];
          this.ViewData.customer =
            res[0].cus_cod.trim() + " : " + res[0].cus_name.trim();

          this.$root.api.Master_s_RopeSearch({
            data: { rope_id: this.ViewData.rope_id },
            callback: (res) => {
              this.ViewData.path_pic = res[0].path_pic;
              console.log(res);
      this.openViewModal = true;

            },
          });
        },
      });
    },
    DeleteButton() {
      this.openDeleteModal = true;
      var x = this.checkedItem[0];
      this.$root.api.Master_s_RopeSpecSearch({
        data: {
          ropespec_id: x,
        },
        callback: (res) => {
          this.DeleteData = res[0];
        },
      });
    },
    DeleteSaveButton() {
      this.$root.api.Master_s_RopeSpecMove({
        data: {
          ropespec_move: [
            {
              ropespec_id: this.DeleteData.ropespec_id,
              user_id: localStorage.getItem("UserID"),
              user_name: localStorage.getItem("User"),
            },
          ],
        },
        callback: (res) => {
          if (res.status == "S") {
            swal({
              title: "สำเร็จ",
              text: "ลบข้อมูลมาตราฐานเงื่อนไข \n การใช้เชือกและฟางเสร็จสิ้น",
              icon: "success",
              button: "ตกลง",
            });
            this.SearchData();
          } else {
            swal({
              title: "ผิดพลาด!",
              text: "ไม่สามารถลบข้อมูลมาตราฐานเงื่อนไข \n การใช้เชือกและฟางได้",
              icon: "error",
              button: "ตกลง",
            });
            this.SearchData();
          }
          this.openDeleteModal = false;
          this.SearchData();
        },
      });
    },
    resetCheck() {
      this.checked = [];
      this.checkedItem = "";
      this.checkedSpec = [];
      this.DataSubTable = [];
    },
    resetData() {
      this.SaleValue = null;
      this.CustomerOptions.selected = null;
      this.ProductTypeOptions.selected = null;
      this.RopeOptions.selected = null;
      this.insertData.effdate_st = this.nowDate;
      this.insertData.effdate_en = "2000-01-01";
    },
    ChangeProductype() {
      this.StretchingTypeOptions.selected = null;
      this.EditData.produccttype_code = this.ProductTypeOptions.selected.producttype_code;
    },
    FindCustomer() {
      this.$root.api.View_master_s_cutomerSearch({
        data: { tsale: this.SaleValue.value },
        callback: (res) => {
          res.forEach(
            (x) => (x._label = GetObjValJoin(x, ["cuscod", "fname"]))
          );
          this.CustomerOptions.lists = res;
        },
      });
    },
    setPagesActive() {
      this.pagesA = [];
      let numberOfPages = Math.ceil(this.dataActiveCount / this.perPageA);
      for (let index = 0; index < numberOfPages; index++) {
        this.pagesA.push(index);
      }
      this.pageA = 1;
    },
    setPagesInactive() {
      this.pagesI = [];
      let numberOfPages = Math.ceil(this.dataInactiveCount / this.perPageI);
      for (let index = 0; index < numberOfPages; index++) {
        this.pagesI.push(index);
      }
      this.pageI = 1;
    },
    setPagesCancel() {
      this.pagesC = [];
      let numberOfPages = Math.ceil(this.dataCancelCount / this.perPageI);
      for (let index = 0; index < numberOfPages; index++) {
        this.pagesC.push(index);
      }
      this.pageC = 1;
    },
    paginate(Active) {
      let pageA = this.pageA;
      let perPageA = this.perPageA;
      let from = pageA * perPageA - perPageA;
      let to = pageA * perPageA;
      return Active.slice(from, to);
    },
    paginateInactive(Inactive) {
      let pageI = this.pageI;
      let perPageI = this.perPageI;
      let from = pageI * perPageI - perPageI;
      let to = pageI * perPageI;
      return Inactive.slice(from, to);
    },
    paginateCancel(Cancel) {
      let pageC = this.pageC;
      let perPageC = this.perPageC;
      let from = pageC * perPageC - perPageC;
      let to = pageC * perPageC;
      return Cancel.slice(from, to);
    },
  },
  computed: {
    displayedPostsActive() {
      return this.paginate(this.dataActive);
    },
    displayedPostsInactive() {
      return this.paginateInactive(this.dataInactive);
    },
    displayedPostsCancel() {
      return this.paginateCancel(this.dataCancel);
    },
  },
  watch: {
    Active() {
      this.setPagesActive();
    },
    Inactive() {
      this.setPagesInactive();
    },
    Cancel() {
      this.setPagesCancel();
    },
  },
};
