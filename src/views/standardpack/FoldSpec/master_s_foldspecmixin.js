import Multiselect from "vue-multiselect";
import {
  GetObjVal,
  SetObjVal,
  ObjCopy,
  ObjResetValue,
  ObjCopyValue,
  GetObjValJoin,
  GetObjArr,
  GetLastUpdate,
  GetDate,
  GetDateView,
  FileToBase64,
} from "@/shared/utils";
import setupmixin from "@/shared/setupmixin";
import swal from "sweetalert";
import DatePicker from "vue2-datepicker";
import "vue2-datepicker/index.css";
import { loadPartialConfig } from "@babel/core";

export default {
  name: "FoldSpec",
  data() {
    return {
      HReport: true,
      canDoSomething: false,
      SaleOptions: [
        { value: "0", name: "ขายในประเทศ" },
        { value: "1", name: "ขายต่างประเทศ" },
      ],
      SaleValue: {},
      CustomerOptions: {
        lists: [],
        selected: null,
      },
      ProductTypeOptions: {
        lists: [],
        selected: null,
      },
      PqgradeOptions: {
        lists: [],
        selected: null,
      },
      ProductSizeMinOptions: {
        lists: [],
        selected: null,
      },
      ProductSizeMaxOptions: {
        lists: [],
        selected: null,
      },
      KnotTypeOptions: {
        lists: [],
        selected: null,
      },
      LineTypeOptions: {
        lists: [],
        selected: null,
      },
      StretchingTypeOptions: {
        lists: [],
        selected: null,
      },
      ColorCodeOptions: {
        lists: [],
        selected: null,
      },
      FoldOptions: {
        lists: [],
        selected: null,
      },
      depsize_min: null,
      depsize_max: null,
      depamt_min: null,
      depamt_max: null,
      len_min: null,
      len_max: null,
      path_pic: "",

      checkItemID: [],

      showTable: false,
      itemsTabs1: true,
      itemsTabs2: false,
      itemsTabs3: false,
      search: {
        cuscod_id: "",
        fold_cd: "",
        txtsearch: "",
      },
      dataFoldCus: [],
      dataFoldSpec: [],
      insertFoldSpec: {
        stdwei_min: null,
        stdwei_max: null,
        foldspec_information: "",
      },
      oldEditData: {},
      editFoldSpec: {
        foldspec_information: "",
        tsale: null,
        cus_id: null,
        cus_cod: null,
        producttype_code: null,
        pqgrade_cd: null,
        stdwei_min: 0,
        stdwei_max: 0,
        twinesize_min: null,
        twinesize_max: null,
        depsize_min: null,
        depsize_max: null,
        depamt_min: null,
        depamt_max: null,
        len_min: null,
        len_max: null,
        knottype_code: null,
        linetype_code: null,
        stretchingtype_code: null,
        old_color_code: null,
        new_color_code: null,
        color_code: null,
        fold_id: null,
        fold_cd: null,
        fold_name: null,
        effdate_st: null,
        effdate_en: null,
        status: null,

        //////////// Min /////////////////////
        min_product_type: null,
        min_productcate_grp: null,
        min_twine_no: null,
        min_line_no: null,
        min_word: null,
        min_product_grp: null,

        //////////// Max /////////////////////
        max_product_type: null,
        max_productcate_grp: null,
        max_twine_no: null,
        max_line_no: null,
        max_word: null,
        max_product_grp: null,
        depsize_min: 0,
        depsize_max: 0,
        depamt_min: 0,
        depamt_max: 0,
        len_min: 0,
        len_max: 0,
        startdate: null,
        exdate: null,
        saleValuetype: null,
      },
      viewFoldSpec: [],
      editViewFoldSpec: [],
      openAddModal: false,
      openEditModal: false,
      openViewModal: false,
      openDeleteModal: false,
      foldType: [],

      checked: [],
      checkedSpec: null,

      defaultExDate: null,
      currentDate: null,
      optionValue: null,
      path_pic: "",
      fold_name: "",
      thisNowDate: "",
      dataFoldCus: [],
      cancelFoldspec: [],
      dataActive: [],
      dataFoldCusCount: 0,
      pageA: 1,
      perPageA: 5,
      pagesA: [],

      // InActive
      dataInactive: [],
      dataIFoldCusCount: 0,
      pageI: 1,
      perPageI: 5,
      pagesI: [],

      // Cancel
      dataCancel: [],
      dataCFoldCusCount: 0,
      pageC: 1,
      perPageC: 5,
      pagesC: [],

      showhidden: true,
      producttype_net: [
        "+",
        "0",
        "1",
        "2",
        "3",
        "4",
        "5",
        "6",
        "A",
        "B",
        "C",
        "D",
        "E",
        "H",
        "I",
        "K",
        "L",
        "M",
        "N",
        "P",
        "Q",
        "S",
        "T",
        "U",
      ],
      producttype_cord: [
        "/",
        "7",
        "8",
        "9",
        "F",
        "G",
        "J",
        "O",
        "R",
        "V",
        "W",
        "X",
        "Y",
        "Z",
      ],
    };
  },
  components: {
    Multiselect,
    DatePicker,
  },
  created() {},
  mounted() {
    let nowDate = new Date();
    this.currentDate = GetDate(nowDate.setDate(nowDate.getDate()));
    this.thisNowDate = GetDate(nowDate.setDate(nowDate.getDate()));

    this.defaultExDate = "2000-01-01";

    this.$root.api.View_master_s_cutomerSearch({
      data: {},
      callback: (res) => {
        res.forEach((x) => (x._label = GetObjValJoin(x, ["cuscod", "fname"])));
        this.CustomerOptions.lists = res;
      },
    });

    this.$root.api.View_master_s_producttypeSearch({
      data: {},
      callback: (res) => {
        res.forEach(
          (x) => (x._label = GetObjValJoin(x, ["producttypecode", "desc1"]))
        );
        this.ProductTypeOptions.lists = res;
      },
    });

    this.$root.api.View_master_s_pqgradeSearch({
      data: {},
      callback: (res) => {
        res.forEach(
          (x) => (x._label = GetObjValJoin(x, ["pqgradecd", "pqgradedesc"]))
        );
        this.PqgradeOptions.lists = res;
      },
    });

    this.$root.api.View_master_s_knottypeSearch({
      data: {},
      callback: (res) => {
        res.forEach((x) => (x._label = GetObjValJoin(x, ["knot_type"])));
        this.KnotTypeOptions.lists = res;
      },
    });

    this.$root.api.View_master_s_stretchingtypeSearch({
      data: {},
      callback: (res) => {
        res.forEach((x) => (x._label = GetObjValJoin(x, ["stretching_desc"])));
        this.StretchingTypeOptions.lists = res;
      },
    });

    // this.$root.api.View_master_s_productsizeSearch({
    //   data: {},
    //   callback: (res) => {
    //     res.forEach((x) => (x._label = GetObjValJoin(x, ["product_size"])));
    //     this.ProductSizeMinOptions.lists = res;
    //     this.ProductSizeMaxOptions.lists = res;
    //   },
    // });
    this.$root.api.Master_s_FoldSearch({
      data: {},
      callback: (res) => {
        res.forEach((x) => (x._label = GetObjValJoin(x, ["fold_code"])));
        this.FoldOptions.lists = res;
      },
    });

    this.$root.api.View_master_s_colorSearch({
      data: {},
      callback: (res) => {
        res.forEach(
          (x) => (x._label = GetObjValJoin(x, ["color_code", "color_name"]))
        );
        this.ColorCodeOptions.lists = res;
      },
    });

    this.$root.api.View_master_s_linetypeSearch({
      data: {
        // producttypecode: producttypecode
      },
      callback: (res) => {
        res.forEach(
          (x) => (x._label = GetObjValJoin(x, ["linetypecode", "desc1"]))
        );
        this.LineTypeOptions.lists = res;
      },
    });
  },

  updated() {
    this.FindImg();
    if (this.SaleValue.value != null && this.CustomerOptions.selected == null) {
      this.FindCustomer();
    }
  },
  beforeUpdate() {},
  methods: {
    checkedMinMax() {
      var prodSizeMin = this.ProductSizeMinOptions.selected;
      var prodSizeMax = this.ProductSizeMaxOptions.selected;
      // // console.log(prodSizeMax.twine_no , 'prodSizeMax.twine_no');
      // // console.log(prodSizeMin.twine_no , 'prodSizeMin.twine_no');
      // // console.log(prodSizeMax.line_no ,' prodSizeMax.line_no');
      // // console.log(prodSizeMin.line_no ,' prodSizeMin.line_no');
      if (prodSizeMin != null && prodSizeMax != null) {
        console.log(
          Number(prodSizeMax.twine_no) < Number(prodSizeMin.twine_no)
        );
        if (Number(prodSizeMax.twine_no) < Number(prodSizeMin.twine_no)) {
          swal("ผิดพลาด!", "เบอร์ใยสูงสุดต้องมากกว่าเบอร์ใยต่ำสุด", "error");
          this.ProductSizeMinOptions.selected = null;
          this.ProductSizeMaxOptions.selected = null;
        } else if (
          Number(prodSizeMax.twine_no) == Number(prodSizeMin.twine_no)
        ) {
          // // console.log(Number(prodSizeMax.line_no) < Number(prodSizeMin.line_no));
          if (Number(prodSizeMax.line_no) < Number(prodSizeMin.line_no)) {
            swal("ผิดพลาด!", "เบอร์ใยสูงสุดต้องมากกว่าเบอร์ใยต่ำสุด", "error");
            this.ProductSizeMinOptions.selected = null;
            this.ProductSizeMaxOptions.selected = null;
          }
        }
      }

      if (this.depsize_min != null && this.depsize_max != null) {
        if (Number(this.depsize_max) < Number(this.depsize_min)) {
          swal("ผิดพลาด!", "ขนาดตาสูงสุดต้องมากกว่าขนาดตาต่ำสุด", "error");
          this.depsize_min = null;
          this.depsize_max = null;
        }
      }

      if (this.depamt_min != null && this.depamt_max != null) {
        if (Number(this.depamt_max) < Number(this.depamt_min)) {
          swal("ผิดพลาด!", "จำนวนตาสูงสุดต้องมากกว่าจำนวนตาต่ำสุด", "error");
          this.depamt_min = null;
          this.depamt_max = null;
        }
      }

      if (this.len_min != null && this.len_max != null) {
        if (Number(this.len_max) < Number(this.len_min)) {
          swal("ผิดพลาด!", "ความยาวสูงสุดต้องมากกว่าความยาวต่ำสุด", "error");
          this.len_min = null;
          this.len_max = null;
        }
      }
      if (
        this.insertFoldSpec.stdwei_min != null &&
        this.insertFoldSpec.stdwei_max != null
      ) {
        if (
          Number(this.insertFoldSpec.stdwei_max) <
          Number(this.insertFoldSpec.stdwei_min)
        ) {
          swal(
            "ผิดพลาด!",
            "นน.ต่อผืนสูงสุดต้องมากกว่านน.ต่อผืนต่ำสุด",
            "error"
          );
          this.insertFoldSpec.stdwei_min = null;
          this.insertFoldSpec.stdwei_max = null;
        }
      }
    },
    checkedMinMaxinEdit() {
      var old_prodSizemin = [];
      var old_prodSizemax = [];

      var prodSizeMin = this.ProductSizeMinOptions.selected;
      var prodSizeMax = this.ProductSizeMaxOptions.selected;
      console.log("Max.twine_no", Number(prodSizeMax.twine_no));
      console.log("Min.twine_no", Number(prodSizeMin.twine_no));
      if (prodSizeMin != null && prodSizeMax != null) {
        if (Number(prodSizeMax.twine_no) < Number(prodSizeMin.twine_no)) {
          swal("ผิดพลาด!", "เบอร์ใยสูงสุดต้องมากกว่าเบอร์ใยต่ำสุด", "error");
          this.$root.api.View_master_s_productsizeSearch({
            data: { product_size: this.editViewFoldSpec[0].twinesize_min },
            callback: (res) => {
              res.forEach(
                (x) => (x._label = GetObjValJoin(x, ["product_size"]))
              );
              this.ProductSizeMinOptions.selected = res[0];
            },
          });

          this.$root.api.View_master_s_productsizeSearch({
            data: { product_size: this.editViewFoldSpec[0].twinesize_max },
            callback: (res) => {
              res.forEach(
                (x) => (x._label = GetObjValJoin(x, ["product_size"]))
              );
              this.ProductSizeMaxOptions.selected = res[0];
            },
          });
        } else if (
          Number(prodSizeMax.twine_no) == Number(prodSizeMin.twine_no)
        ) {
          // // console.log(Number(prodSizeMax.line_no) < Number(prodSizeMin.line_no));
          if (Number(prodSizeMax.line_no) < Number(prodSizeMin.line_no)) {
            swal("ผิดพลาด!", "เบอร์ใยสูงสุดต้องมากกว่าเบอร์ใยต่ำสุด", "error");
            this.$root.api.View_master_s_productsizeSearch({
              data: { product_size: this.editViewFoldSpec[0].twinesize_min },
              callback: (res) => {
                res.forEach(
                  (x) => (x._label = GetObjValJoin(x, ["product_size"]))
                );
                this.ProductSizeMinOptions.selected = res[0];
              },
            });

            this.$root.api.View_master_s_productsizeSearch({
              data: { product_size: this.editViewFoldSpec[0].twinesize_max },
              callback: (res) => {
                res.forEach(
                  (x) => (x._label = GetObjValJoin(x, ["product_size"]))
                );
                this.ProductSizeMaxOptions.selected = res[0];
              },
            });
          }
        }
      }

      if (
        this.editFoldSpec.depsize_min != 0 &&
        this.editFoldSpec.depsize_max != 0
      ) {
        if (
          Number(this.editFoldSpec.depsize_max) <
          Number(this.editFoldSpec.depsize_min)
        ) {
          swal("ผิดพลาด!", "ขนาดตาสูงสุดต้องมากกว่าขนาดตาต่ำสุด", "error");
          this.editFoldSpec.depsize_min = this.editViewFoldSpec[0].depsize_min.toFixed(
            2
          );
          this.editFoldSpec.depsize_max = this.editViewFoldSpec[0].depsize_max.toFixed(
            2
          );
        }
      }

      if (
        this.editFoldSpec.depamt_min != 0 &&
        this.editFoldSpec.depamt_max != 0
      ) {
        if (
          Number(this.editFoldSpec.depamt_max) <
          Number(this.editFoldSpec.depamt_min)
        ) {
          swal("ผิดพลาด!", "จำนวนตาสูงสุดต้องมากกว่าจำนวนตาต่ำสุด", "error");
          this.editFoldSpec.depamt_min = this.editViewFoldSpec[0].depamt_min.toFixed(
            2
          );
          this.editFoldSpec.depamt_max = this.editViewFoldSpec[0].depamt_max.toFixed(
            2
          );
        }
      }

      if (this.editFoldSpec.len_min != 0 && this.editFoldSpec.len_max != 0) {
        if (
          Number(this.editFoldSpec.len_max) < Number(this.editFoldSpec.len_min)
        ) {
          swal("ผิดพลาด!", "ความยาวสูงสุดต้องมากกว่าความยาวต่ำสุด", "error");
          this.editFoldSpec.len_min = this.editViewFoldSpec[0].len_min.toFixed(
            2
          );
          this.editFoldSpec.len_max = this.editViewFoldSpec[0].len_max.toFixed(
            2
          );
        }
      }
      if (
        this.editFoldSpec.stdwei_min != 0 &&
        this.editFoldSpec.stdwei_max != 0
      ) {
        if (
          Number(this.editFoldSpec.stdwei_max) <
          Number(this.editFoldSpec.stdwei_min)
        ) {
          swal(
            "ผิดพลาด!",
            "นน.ต่อผืนสูงสุดต้องมากกว่านน.ต่อผืนต่ำสุด",
            "error"
          );
          this.editFoldSpec.stdwei_min = this.editViewFoldSpec[0].stdwei_min.toFixed(
            2
          );
          this.editFoldSpec.stdwei_max = this.editViewFoldSpec[0].stdwei_max.toFixed(
            2
          );
        }
      }
    },
    FindCustomer() {
      this.$root.api.View_master_s_cutomerSearch({
        data: { tsale: this.SaleValue.value },
        callback: (res) => {
          res.forEach(
            (x) => (x._label = GetObjValJoin(x, ["cuscod", "fname"]))
          );
          this.CustomerOptions.lists = res;
        },
      });
    },
    ResetInput() {
      this.SaleValue = {};
      this.checkedSpec = [];
      this.CustomerOptions.selected = null;
      this.ProductTypeOptions.selected = null;
      this.PqgradeOptions.selected = null;
      this.ProductSizeMinOptions.selected = null;
      this.ProductSizeMaxOptions.selected = null;
      this.KnotTypeOptions.selected = null;
      this.LineTypeOptions.selected = null;
      this.ColorCodeOptions.selected = null;
      this.StretchingTypeOptions.selected = null;
      this.FoldOptions.selected = null;
      this.depsize_min = null;
      this.depsize_max = null;
      this.depamt_min = null;
      this.depamt_max = null;
      this.len_min = null;
      this.len_max = null;
      this.insertFoldSpec.stdwei_min = null;
      this.insertFoldSpec.stdwei_max = null;

      let nowDate = new Date();
      this.currentDate = GetDate(nowDate.setDate(nowDate.getDate()));
      this.thisNowDate = GetDate(nowDate.setDate(nowDate.getDate()));
      this.defaultExDate = "2000-01-01";
    },
    FindImg() {
      if (this.FoldOptions.selected != null) {
        this.path_pic =
          "http://masterdatadev.kkfnets.com" +
          this.FoldOptions.selected.path_pic;
      }
    },
    FindLineTypeOptions() {
      this.ProductSizeMinOptions.selected = null;
      this.ProductSizeMaxOptions.selected = null;
      if (this.ProductTypeOptions.selected.productcate_grp == "H") {
        this.$root.api.View_master_s_productsizeSearch({
          data: {},
          callback: (res) => {
            res.forEach((x) => (x._label = GetObjValJoin(x, ["product_size"])));
            this.ProductSizeMinOptions.lists = res;
            this.ProductSizeMaxOptions.lists = res;
          },
        });
      } else {
        this.$root.api.View_master_s_productsizeSearch({
          data: {
            productcate_grp: this.ProductTypeOptions.selected.productcate_grp,
          },
          callback: (res) => {
            res.forEach((x) => (x._label = GetObjValJoin(x, ["product_size"])));
            this.ProductSizeMinOptions.lists = res;
            this.ProductSizeMaxOptions.lists = res;
          },
        });
      }
      this.$root.api.View_master_s_linetypeSearch({
        data: {
          // producttypecode: producttypecode
        },
        callback: (res) => {
          res.forEach(
            (x) => (x._label = GetObjValJoin(x, ["linetypecode", "desc1"]))
          );
          this.LineTypeOptions.lists = res;
        },
      });
    },
    SearchSpec() {
      this.dataFoldCus = [];
      this.dataFoldSpec = [];
      this.dataFoldCusCount = 0;
      this.dataIFoldCusCount = 0;
      this.dataCFoldCusCount = 0;
      this.checked = [];
      this.checkedSpec = [];

      this.$root.api.Master_s_FoldSpecSearchbyCus({
        data: {
          fold_cd: this.search.fold_cd,
          cuscod: this.search.cuscod_id,
          txtSearch: this.search.txtsearch,
        },
        callback: (res) => {
          this.dataFoldCus = res;
          for (let i = 0; i < res.length; i++) {
            if (res[i].status == "A") {
              this.dataFoldCusCount = this.dataFoldCusCount + 1;
            } else if (res[i].status == "I") {
              this.dataIFoldCusCount = this.dataIFoldCusCount + 1;
            } else if (res[i].status == "C") {
              this.dataCFoldCusCount = this.dataCFoldCusCount + 1;
            }
          }
          if (res.length == 0) {
            swal(
              "ไม่พบเงื่อนไขการพับมัดที่ค้นหา",
              "กรุณาใส่รหัสลูกค้าหรือรหัสการพับมัดใหม่",
              "warning"
            );
            this.search = [];
          }
          this.getDataStrap();
          this.setPagesActive();
          this.setPagesInactive();
          this.setPagesCancel();
        },
      });

      this.showTable = true;
      this.$root.api.Master_s_FoldSpecSearchCancel({
        data: { foldspec_id: foldspec_id },
        callback: (res) => {
          console.log("cancelFoldspec", res);
          this.cancelFoldspec = res[0];
        },
      });
    },
    checkItems(cuscod_id) {
      this.checkedSpec = [];
      this.$root.api.Master_s_FoldSpecSearch({
        data: {
          cuscod: cuscod_id,
        },
        callback: (res) => {
          this.dataFoldSpec = res;
        },
      });
    },
    checkFoldSpecItems(foldspec_id) {
      this.editViewFoldSpec = [];
      this.viewFoldSpec = [];
      this.checkedSpec = [];

      if (this.itemsTabs3 == true) {
        this.$root.api.Master_s_FoldSpecSearchCancel({
          data: { foldspec_id: foldspec_id },
          callback: (res) => {
            if (res[0].stretchingtype_code != " ") {
              this.$root.api.View_master_s_stretchingtypeSearch({
                data: { stretchingtype_code: res[0].stretchingtype_code },
                callback: (res) => {
                  res.forEach(
                    (x) => (x._label = GetObjValJoin(x, ["stretching_desc"]))
                  );
                  this.StretchingTypeOptions.selected = res[0];
                },
              });
            } else {
            }
            //? เงื่อน
            this.$root.api.View_master_s_knottypeSearch({
              data: { knottype_code: res[0].knottype_code },
              callback: (res) => {
                res.forEach((x) => (x._label = GetObjValJoin(x, ["knot_type"])));
                this.KnotTypeOptions.selected = res[0];
              },
            });
  
            //? ประเภทสินค้า
            this.$root.api.View_master_s_producttypeSearch({
              data: { producttype_code: res[0].producttype_code },
              callback: (res) => {
                res.forEach(
                  (x) =>
                    (x._label = GetObjValJoin(x, ["producttypecode", "desc1"]))
                );
                this.ProductTypeOptions.selected = res[0];
                this.editViewFoldSpec.push({
                  productcate_grp: res[0].productcate_grp,
                  productcate_grp_name: res[0].productcate_grp_name,
                });
                this.viewFoldSpec.push({
                  productcate_grp: res[0].productcate_grp,
                  productcate_grp_name: res[0].productcate_grp_name,
                });
              },
            });
  
            //? ระดับคุณภาพ
            this.$root.api.View_master_s_pqgradeSearch({
              data: { pqgradecd: res[0].pqgradepd },
              callback: (res) => {
                res.forEach(
                  (x) =>
                    (x._label = GetObjValJoin(x, ["pqgradecd", "pqgradedesc"]))
                );
                this.PqgradeOptions.selected = res[0];
              },
            });
  
            //? เบอร์ใย
            this.$root.api.View_master_s_productsizeSearch({
              data: { product_size: res[0].twinesize_max },
              callback: (res) => {
                res.forEach(
                  (x) => (x._label = GetObjValJoin(x, ["product_size"]))
                );
                this.ProductSizeMaxOptions.selected = res[0];
              },
            });
  
            this.$root.api.View_master_s_productsizeSearch({
              data: { product_size: res[0].twinesize_min },
              callback: (res) => {
                res.forEach(
                  (x) => (x._label = GetObjValJoin(x, ["product_size"]))
                );
                this.ProductSizeMinOptions.selected = res[0];
              },
            });
  
            // ?สี
            this.$root.api.View_master_s_colorSearch({
              data: { color_code: res[0].color_code },
              callback: (res) => {
                res.forEach(
                  (x) =>
                    (x._label = GetObjValJoin(x, ["color_code", "color_name"]))
                );
                this.ColorCodeOptions.selected = res[0];
              },
            });
  
            this.$root.api.Master_s_FoldSearch({
              data: { fold_id: res[0].fold_id },
              callback: (res) => {
                res.forEach((x) => (x._label = GetObjValJoin(x, ["fold_code"])));
                this.FoldOptions.selected = res[0];
              },
            });
  
            //? รุ่นด้าย
            this.$root.api.View_master_s_linetypeSearch({
              data: {
                linetypecode: res[0].linetype_code,
              },
              callback: (res) => {
                res.forEach(
                  (x) => (x._label = GetObjValJoin(x, ["linetypecode", "desc1"]))
                );
                this.LineTypeOptions.selected = res[0];
              },
            });
            this.editViewFoldSpec = res;
            this.editFoldSpec.foldspec_information = this.editViewFoldSpec[0].foldspec_information;
            this.editFoldSpec.startdate = this.editViewFoldSpec[0].effdate_st.split(
              "T"
            )[0];
            if (this.editViewFoldSpec[0].tsale == 0) {
              this.editFoldSpec.saleValuetype = "ขายในประเทศ";
            } else {
              this.editFoldSpec.saleValuetype = "ขายต่างประเทศ";
            }
  
            this.editFoldSpec.exdate = this.editViewFoldSpec[0].effdate_en.split(
              "T"
            )[0];
            this.viewFoldSpec = res;
  
            this.path_pic = "";
            this.fold_name = "";
            this.$root.api.Master_s_FoldSearch({
              data: { fold_id: this.viewFoldSpec[0].fold_id },
              callback: (res) => {
                this.viewFoldSpec.path_pic = res[0].path_pic;
                this.fold_name = res[0].fold_name;
              },
            });
  
            this.canDoSomething = true;
          },
        });
      }
      this.$root.api.Master_s_FoldSpecSearchbyId({
        data: { foldspec_id: foldspec_id },
        callback: (res) => {
          console.log("stretchingtype_code", res[0].stretchingtype_code);
          console.log(
            "includes",
            this.producttype_cord.includes(res[0].producttype_code)
          );
          console.log(this.producttype_cord);

          if (res[0].stretchingtype_code != " ") {
            this.$root.api.View_master_s_stretchingtypeSearch({
              data: { stretchingtype_code: res[0].stretchingtype_code },
              callback: (res) => {
                res.forEach(
                  (x) => (x._label = GetObjValJoin(x, ["stretching_desc"]))
                );
                this.StretchingTypeOptions.selected = res[0];
              },
            });
          } else {
          }
          //? เงื่อน
          this.$root.api.View_master_s_knottypeSearch({
            data: { knottype_code: res[0].knottype_code },
            callback: (res) => {
              res.forEach((x) => (x._label = GetObjValJoin(x, ["knot_type"])));
              this.KnotTypeOptions.selected = res[0];
            },
          });

          //? ประเภทสินค้า
          this.$root.api.View_master_s_producttypeSearch({
            data: { producttype_code: res[0].producttype_code },
            callback: (res) => {
              res.forEach(
                (x) =>
                  (x._label = GetObjValJoin(x, ["producttypecode", "desc1"]))
              );
              this.ProductTypeOptions.selected = res[0];
              this.editViewFoldSpec.push({
                productcate_grp: res[0].productcate_grp,
                productcate_grp_name: res[0].productcate_grp_name,
              });
              this.viewFoldSpec.push({
                productcate_grp: res[0].productcate_grp,
                productcate_grp_name: res[0].productcate_grp_name,
              });
            },
          });

          //? ระดับคุณภาพ
          this.$root.api.View_master_s_pqgradeSearch({
            data: { pqgradecd: res[0].pqgradepd },
            callback: (res) => {
              res.forEach(
                (x) =>
                  (x._label = GetObjValJoin(x, ["pqgradecd", "pqgradedesc"]))
              );
              this.PqgradeOptions.selected = res[0];
            },
          });

          //? เบอร์ใย
          this.$root.api.View_master_s_productsizeSearch({
            data: { product_size: res[0].twinesize_max },
            callback: (res) => {
              res.forEach(
                (x) => (x._label = GetObjValJoin(x, ["product_size"]))
              );
              this.ProductSizeMaxOptions.selected = res[0];
            },
          });

          this.$root.api.View_master_s_productsizeSearch({
            data: { product_size: res[0].twinesize_min },
            callback: (res) => {
              res.forEach(
                (x) => (x._label = GetObjValJoin(x, ["product_size"]))
              );
              this.ProductSizeMinOptions.selected = res[0];
            },
          });

          // ?สี
          this.$root.api.View_master_s_colorSearch({
            data: { color_code: res[0].color_code },
            callback: (res) => {
              res.forEach(
                (x) =>
                  (x._label = GetObjValJoin(x, ["color_code", "color_name"]))
              );
              this.ColorCodeOptions.selected = res[0];
            },
          });

          this.$root.api.Master_s_FoldSearch({
            data: { fold_id: res[0].fold_id },
            callback: (res) => {
              res.forEach((x) => (x._label = GetObjValJoin(x, ["fold_code"])));
              this.FoldOptions.selected = res[0];
            },
          });

          //? รุ่นด้าย
          this.$root.api.View_master_s_linetypeSearch({
            data: {
              linetypecode: res[0].linetype_code,
            },
            callback: (res) => {
              res.forEach(
                (x) => (x._label = GetObjValJoin(x, ["linetypecode", "desc1"]))
              );
              this.LineTypeOptions.selected = res[0];
            },
          });
          this.editViewFoldSpec = res;
          this.editFoldSpec.foldspec_information = this.editViewFoldSpec[0].foldspec_information;
          this.editFoldSpec.startdate = this.editViewFoldSpec[0].effdate_st.split(
            "T"
          )[0];
          if (this.editViewFoldSpec[0].tsale == 0) {
            this.editFoldSpec.saleValuetype = "ขายในประเทศ";
          } else {
            this.editFoldSpec.saleValuetype = "ขายต่างประเทศ";
          }

          this.editFoldSpec.exdate = this.editViewFoldSpec[0].effdate_en.split(
            "T"
          )[0];
          this.viewFoldSpec = res;

          this.path_pic = "";
          this.fold_name = "";
          this.$root.api.Master_s_FoldSearch({
            data: { fold_id: this.viewFoldSpec[0].fold_id },
            callback: (res) => {
              this.viewFoldSpec.path_pic = res[0].path_pic;
              this.fold_name = res[0].fold_name;
            },
          });

          this.canDoSomething = true;
        },
      });
    },

    EditButton() {
      (this.editFoldSpec.depsize_min = this.editViewFoldSpec[0].depsize_min.toFixed(
        2
      )),
        (this.editFoldSpec.depsize_max = this.editViewFoldSpec[0].depsize_max.toFixed(
          2
        )),
        (this.editFoldSpec.depamt_min = this.editViewFoldSpec[0].depamt_min.toFixed(
          2
        )),
        (this.editFoldSpec.depamt_max = this.editViewFoldSpec[0].depamt_max.toFixed(
          2
        )),
        (this.editFoldSpec.len_min = this.editViewFoldSpec[0].len_min.toFixed(
          2
        )),
        (this.editFoldSpec.len_max = this.editViewFoldSpec[0].len_max.toFixed(
          2
        )),
        (this.editFoldSpec.stdwei_min = this.editViewFoldSpec[0].stdwei_min.toFixed(
          2
        )),
        (this.editFoldSpec.stdwei_max = this.editViewFoldSpec[0].stdwei_max.toFixed(
          2
        )),
        (this.openEditModal = true);
    },
    ViewButton() {
      this.openViewModal = true;
    },
    SaveEditLableButton(linetype_code, knottype, stretchingtype) {
      if (this.editFoldSpec.exdate > this.editFoldSpec.startdate) {
        var statusLable = "A";
      } else {
        var statusLable = "A";
      }

      var cuscod = "";
      var cusname = "";
      if (this.CustomerOptions.selected == null) {
        (cuscod = this.editViewFoldSpec[0].cus_cod),
          (cusname = this.editViewFoldSpec[0].cus_name);
      } else {
        (cuscod = this.CustomerOptions.selected.cuscod),
          (cusname = this.CustomerOptions.selected.fname);
      }

      var saletype = "";
      if (this.SaleValue.value == null) {
        saletype = this.editViewFoldSpec[0].tsale;
      } else {
        saletype = this.SaleValue.value;
      }

      var producttype_code = "";
      var producttype_desc = "";
      if (this.ProductTypeOptions.selected == null) {
        producttype_code = this.editViewFoldSpec[0].producttype_code;
        producttype_desc = this.editViewFoldSpec[0].producttype_desc;
      } else {
        producttype_code = this.ProductTypeOptions.selected.producttypecode;
        producttype_desc = this.ProductTypeOptions.selected.desc1;
      }

      var pqgrade_cd = "";
      var pqgrade_desc = "";
      if (this.PqgradeOptions.selected == null) {
        pqgrade_cd = this.editViewFoldSpec[0].pqgradepd;
        pqgrade_desc = this.editViewFoldSpec[0].pqgrade_desc;
      } else {
        pqgrade_cd = this.PqgradeOptions.selected.pqgradecd;
        pqgrade_desc = this.PqgradeOptions.selected.pqgradedesc;
      }

      var twinesize_min = "";
      var min_product_type = "";
      var min_productcate_grp = "";
      var min_twine_no = "";
      var min_line_no = "";
      var min_word = "";
      var min_product_grp = "";
      if (this.ProductSizeMinOptions.selected == null) {
        twinesize_min = this.editViewFoldSpec[0].twinesize_min;
        min_product_type = this.editViewFoldSpec[0].min_product_type;
        min_productcate_grp = this.editViewFoldSpec[0].min_productcate_grp;
        min_twine_no = this.editViewFoldSpec[0].min_twine_no;
        min_line_no = this.editViewFoldSpec[0].min_line_no;
        min_word = this.editViewFoldSpec[0].min_word;
        min_product_grp = this.editViewFoldSpec[0].min_product_grp;
      } else {
        twinesize_min = this.ProductSizeMinOptions.selected.product_size;
        min_product_type = this.ProductSizeMinOptions.selected.product_type;
        min_productcate_grp = this.ProductSizeMinOptions.selected.productcate;
        min_twine_no = this.ProductSizeMinOptions.selected.twine_no;
        min_line_no = this.ProductSizeMinOptions.selected.line_no;
        min_word = this.ProductSizeMinOptions.selected.word;
        min_product_grp = this.ProductSizeMinOptions.selected.product_grp;
      }

      var twinesize_max = "";
      var max_product_type = "";
      var max_productcate_grp = "";
      var max_twine_no = "";
      var max_line_no = "";
      var max_word = "";
      var max_product_grp = "";
      if (this.ProductSizeMaxOptions.selected == null) {
        twinesize_max = this.editViewFoldSpec[0].twinesize_max;
        max_product_type = this.editViewFoldSpec[0].max_product_type;
        max_productcate_grp = this.editViewFoldSpec[0].max_productcate_grp;
        max_twine_no = this.editViewFoldSpec[0].max_twine_no;
        max_line_no = this.editViewFoldSpec[0].max_line_no;
        max_word = this.editViewFoldSpec[0].max_word;
        max_product_grp = this.editViewFoldSpec[0].max_product_grp;
      } else {
        twinesize_max = this.ProductSizeMaxOptions.selected.product_size;
        max_product_type = this.ProductSizeMaxOptions.selected.product_type;
        max_productcate_grp = this.ProductSizeMaxOptions.selected.productcate;
        max_twine_no = this.ProductSizeMaxOptions.selected.twine_no;
        max_line_no = this.ProductSizeMaxOptions.selected.line_no;
        max_word = this.ProductSizeMaxOptions.selected.word;
        max_product_grp = this.ProductSizeMaxOptions.selected.product_grp;
      }

      var color_code = "";
      var old_color_code = "";
      var new_color_code = "";
      if (this.ColorCodeOptions.selected == null) {
        color_code = "";
        old_color_code = "";
        new_color_code = "";
      } else {
        color_code = this.ColorCodeOptions.selected.color_code;
        old_color_code = this.ColorCodeOptions.selected.old_color_code;
        new_color_code = this.ColorCodeOptions.selected.new_color_code;
      }

      var knottype_code = knottype;
      var linetype_code = linetype_code;

      var stretchingtype_code = stretchingtype;
      var fold_id = "";
      var fold_cd = "";
      var fold_name = "";
      if (this.FoldOptions.selected == null) {
        fold_id = this.editViewFoldSpec[0].fold_id;
        fold_cd = this.editViewFoldSpec[0].fold_cd;
        fold_name = this.editViewFoldSpec[0].fold_name;
      } else {
        fold_id = this.FoldOptions.selected.fold_id;
        fold_cd = this.FoldOptions.selected.fold_code;
        fold_name = this.FoldOptions.selected.fold_name;
      }
      this.$root.api.Master_s_FoldSpecSave({
        data: {
          foldspec_id: this.editViewFoldSpec[0].foldspec_id,
          foldspec_information: this.editFoldSpec.foldspec_information,
          tsale: saletype,
          cus_cod: cuscod,
          cus_name: cusname,
          producttype_code: producttype_code,
          producttype_desc: producttype_desc,
          pqgradepd: pqgrade_cd,
          pqgrade_desc: pqgrade_desc,
          stdwei_min: this.editFoldSpec.stdwei_min,
          stdwei_max: this.editFoldSpec.stdwei_max,
          twinesize_min: twinesize_min,
          twinesize_max: twinesize_max,
          depsize_min: this.editFoldSpec.depsize_min,
          depsize_max: this.editFoldSpec.depsize_max,
          depamt_min: this.editFoldSpec.depamt_min,
          depamt_max: this.editFoldSpec.depamt_max,
          len_min: this.editFoldSpec.len_min,
          len_max: this.editFoldSpec.len_max,
          knottype_code: knottype_code,
          linetype_code: linetype_code,
          stretchingtype_code: stretchingtype_code,
          old_color_code: old_color_code,
          new_color_code: new_color_code,
          color_code: color_code,
          fold_id: fold_id,
          fold_cd: fold_cd,
          fold_name: fold_name,

          effdate_st: this.editFoldSpec.startdate,
          effdate_en: this.editFoldSpec.exdate,
          status: statusLable,

          //////////// Min /////////////////////
          min_product_type: min_product_type,
          min_productcate_grp: min_productcate_grp,
          min_twine_no: min_twine_no,
          min_line_no: min_line_no,
          min_word: min_word,
          min_product_grp: min_product_grp,

          //////////// Max /////////////////////
          max_product_type: max_product_type,
          max_productcate_grp: max_productcate_grp,
          max_twine_no: max_twine_no,
          max_line_no: max_line_no,
          max_word: max_word,
          max_product_grp: max_product_grp,
          user_id: localStorage.getItem("UserID"),
          user_name: localStorage.getItem("User"),
        },
        callback: (res) => {
          swal("สำเร็จ!", "บันทึกข้อมูลเสร็จสิ้น", "success");
          this.openEditModal = false;
        },
      });
      this.SearchSpec();
      this.ResetInput();
    },
    // showHideRow(row) {
    //   $("#" + row).toggle();
    // },
    DeleteLableButton() {
      this.$root.api.Master_s_FoldSpecMove({
        data: {
          foldspec_move: [
            {
              foldspec_id: this.viewFoldSpec[0].foldspec_id,
              cancel_id: localStorage.getItem("UserID"),
              cancel_name: localStorage.getItem("User"),
            },
          ],
        },
        callback: (res) => {
          swal("สำเร็จ!", "ลบรายการเสร็จสิ้น", "success");
          this.openDeleteModal = false;
          this.checked = [];
          this.SearchSpec();
        },
      });
    },
    InsertFoldButton() {
      var aleartArr = [];
      var aleartTitle = "";
      // ข้อความแจ้งเตือน
      if (this.SaleValue.value == null) {
        aleartArr.push("ประเภทการขาย");
      }
      if (this.CustomerOptions.selected == null) {
        aleartArr.push("รหัสลูกค้า");
      }

      if (this.ProductTypeOptions.selected == null) {
        aleartArr.push("ประเภทสินค้า");
      }
      if (this.PqgradeOptions.selected == null) {
        aleartArr.push("ระดับคุณภาพ");
      }

      if (this.FoldOptions.selected == null) {
        aleartArr.push("รหัสการพับมัด");
      }

      aleartTitle = aleartArr.join(", ");

      if (aleartArr.length > 0) {
        swal({
          title: "กรุณากรอกข้อมูลให้ครบถ้วน",
          text: aleartTitle,
          icon: "error",
        });
        aleartArr = [];
      } else {
        this.$root.api.Master_s_FoldSpecSearch({
          data: {
            cuscod: this.CustomerOptions.selected.cuscod,
          },
          callback: (res) => {
            console.log(res);
            if (res.length != 0) {
              const twine = [];
              const depsize = [];
              const depamt = [];
              const len = [];
              const stdwei = [];

              for (let i = 0; i < res.length; i++) {
                twine.push({
                  max: res[i].max_twine_no,
                  min: res[i].min_twine_no,
                });
                depsize.push({
                  max: res[i].depsize_max,
                  min: res[i].depsize_min,
                });
                depamt.push({
                  max: res[i].depamt_max,
                  min: res[i].depamt_min,
                });
                len.push({
                  max: res[i].len_max,
                  min: res[i].len_min,
                });
                stdwei.push({
                  max: res[i].stdwei_max,
                  min: res[i].stdwei_min,
                });
              }

              var twineResult = [];
              var depsizeResult = [];
              var depamtResult = [];
              var lenResult = [];
              var stdweiResult = [];

              for (let i = 0; i < twine.length; i++) {
                var _twine_no_min = 0;
                var _twine_no_max = 0;
                if (
                  this.ProductSizeMinOptions.selected == null &&
                  this.ProductSizeMaxOptions.selected == null
                ) {
                  _twine_no_min = 0;
                  _twine_no_max = 0;
                } else {
                  _twine_no_min = this.ProductSizeMinOptions.selected.twine_no;
                  _twine_no_max = this.ProductSizeMaxOptions.selected.twine_no;
                }
                var x =
                  _twine_no_min > twine[i].max || _twine_no_max < twine[i].min;
                twineResult.push(x.toString());
              }

              for (let i = 0; i < depsize.length; i++) {
                if (this.depsize_min == null) {
                  this.depsize_min = 0;
                }

                if (this.depsize_max == null) {
                  this.depsize_max = 0;
                }

                var x =
                  this.depsize_min > depsize[i].max ||
                  this.depsize_max < depsize[i].min;
                depsizeResult.push(x.toString());
              }
              for (let i = 0; i < depamt.length; i++) {
                if (this.depamt_min == null) {
                  this.depamt_min = 0;
                }
                if (this.depamt_max == null) {
                  this.depamt_max = 0;
                }
                var x =
                  this.depamt_min > depamt[i].max ||
                  this.depamt_max < depamt[i].min;
                depamtResult.push(x.toString());
              }
              for (let i = 0; i < len.length; i++) {
                if (this.len_min == null) {
                  this.len_min = 0;
                }
                if (this.len_max == null) {
                  this.len_max = 0;
                }

                var x = this.len_min > len[i].max || this.len_max < len[i].min;
                lenResult.push(x.toString());
              }
              for (let i = 0; i < stdwei.length; i++) {
                if (this.insertFoldSpec.stdwei_min == null) {
                  this.insertFoldSpec.stdwei_min = 0;
                }
                if (this.insertFoldSpec.stdwei_max == null) {
                  this.insertFoldSpec.stdwei_max = 0;
                }
                var x =
                  this.insertFoldSpec.stdwei_min > stdwei[i].max ||
                  this.insertFoldSpec.stdwei_max < stdwei[i].min;
                stdweiResult.push(x.toString());
              }
              var twineCheck = twineResult.includes("false");
              var depsizeCheck = depsizeResult.includes("false");
              var depamtCheck = depamtResult.includes("false");
              var lenCheck = lenResult.includes("false");
              var stdweiCheck = stdweiResult.includes("false");
              if (twineCheck == false) {
                //   TWINE Check PASS Insert this
                this.insertNewFoldSpec();
              } else {
                //   Next Check NOT PASS : DEPSIZE
                if (depsizeCheck == false) {
                  // DEPSIZE Check PASS Insert this
                  this.insertNewFoldSpec();
                } else {
                  // Next Checed NOT PASS : DEPAMP
                  if (depamtCheck == false) {
                    //   DEPAMP Check PASS Insert this
                    this.insertNewFoldSpec();
                  } else {
                    //   Next Checed NOT PASS  : LEN
                    if (lenCheck == false) {
                      // LEN Check PASS Insert this
                      this.insertNewFoldSpec();
                    } else {
                      //   Next Checed NOT PASS  : STDWEI
                      if (stdweiCheck == false) {
                        //   STDWEI Check PASS Insert this
                        this.insertNewFoldSpec();
                      } else {
                        // PLEASE INSERT NEW ARRAY
                        swal(
                          "ข้อมูลซ้ำซ้อน",
                          "กรุณายืนยันการบันทึกข้อมูลมาตราฐานเงื่อนไขการพับมัดนี้",
                          "warning",
                          {
                            buttons: {
                              cancel: "ยกเลิก",
                              catch: {
                                text: "บันทึก",
                                value: "catch",
                              },
                            },
                          }
                        ).then((value) => {
                          switch (value) {
                            case "catch":
                              this.insertNewFoldSpec();
                          }
                        });
                      }
                    }
                  }
                }
              }
            } else if ((res = [])) {
              this.insertNewFoldSpec();
            }
          },
        });
      }
      // }
    },
    // function Insert
    insertNewFoldSpec() {
      let nowDate = new Date();
      if (this.depsize_min == null) {
        this.depsize_min = 0;
      }

      if (this.depsize_max == null) {
        this.depsize_max = 0;
      }

      if (this.depamt_min == null) {
        this.depamt_min = 0;
      }
      if (this.depamt_max == null) {
        this.depamt_max = 0;
      }
      if (this.len_min == null) {
        this.len_min = 0;
      }
      if (this.len_max == null) {
        this.len_max = 0;
      }
      if (this.insertFoldSpec.stdwei_min == null) {
        this.insertFoldSpec.stdwei_min = 0;
      }
      if (this.insertFoldSpec.stdwei_max == null) {
        this.insertFoldSpec.stdwei_max = 0;
      }

      let x = GetDate(nowDate.setDate(nowDate.getDate()));
      var statusLable = "A";
      if (
        (this.defaultExDate > this.currentDate && this.defaultExDate > x) ||
        this.defaultExDate == "2000-01-01"
      ) {
        var linetype = "";
        var knottype = "";
        var stretchingtype = "";
        var twinesize_min = "";
        var min_product_type = "";
        var min_productcate_grp = "";
        var min_twine_no = "";
        var min_line_no = "";
        var min_word = "";
        var min_product_grp = "";

        var twinesize_max = "";
        var max_product_type = "";
        var max_productcate_grp = "";
        var max_twine_no = "";
        var max_line_no = "";
        var max_word = "";
        var max_product_grp = "";

        if (this.LineTypeOptions.selected == null) {
          linetype = "";
        } else {
          linetype = this.LineTypeOptions.selected.linetypecode;
        }
        if (this.KnotTypeOptions.selected == null) {
          knottype = "";
        } else {
          knottype = this.KnotTypeOptions.selected.knot_type;
        }

        if (this.StretchingTypeOptions.selected == null) {
          stretchingtype = "";
        } else {
          stretchingtype = this.StretchingTypeOptions.selected
            .stretchingtype_code;
        }

        if (this.ProductSizeMinOptions.selected == null) {
          twinesize_min = "";
          min_product_type = "";
          min_productcate_grp = "";
          min_twine_no = 0;
          min_line_no = 0;
          min_word = "";
          min_product_grp = "";
        } else {
          twinesize_min = this.ProductSizeMinOptions.selected.product_size;
          min_product_type = this.ProductSizeMinOptions.selected.product_type;
          min_productcate_grp = this.ProductSizeMinOptions.selected.productcate;
          min_twine_no = this.ProductSizeMinOptions.selected.twine_no;
          min_line_no = this.ProductSizeMinOptions.selected.line_no;
          min_word = this.ProductSizeMinOptions.selected.word;
          min_product_grp = this.ProductSizeMinOptions.selected.product_grp;
        }

        if (this.ProductSizeMaxOptions.selected == null) {
          twinesize_max = "";
          max_product_type = "";
          max_productcate_grp = "";
          max_twine_no = 0;
          max_line_no = 0;
          max_word = "";
          max_product_grp = "";
        } else {
          twinesize_max = this.ProductSizeMaxOptions.selected.product_size;
          max_product_type = this.ProductSizeMaxOptions.selected.product_type;
          max_productcate_grp = this.ProductSizeMaxOptions.selected.productcate;
          max_twine_no = this.ProductSizeMaxOptions.selected.twine_no;
          max_line_no = this.ProductSizeMaxOptions.selected.line_no;
          max_word = this.ProductSizeMaxOptions.selected.word;
          max_product_grp = this.ProductSizeMaxOptions.selected.product_grp;
        }

        var color_code = "";
        var old_color_code = "";
        var new_color_code = "";
        if (this.ColorCodeOptions.selected == null) {
          color_code = "";
          old_color_code = "";
          new_color_code = "";
        } else {
          color_code = this.ColorCodeOptions.selected.color_code;
          old_color_code = this.ColorCodeOptions.selected.old_color_code;
          new_color_code = this.ColorCodeOptions.selected.new_color_code;
        }
        this.$root.api.Master_s_FoldSpecSave({
          data: {
            foldspec_information: this.insertFoldSpec.foldspec_information,
            tsale: this.SaleValue.value,
            cus_cod: this.CustomerOptions.selected.cuscod,
            cus_name: this.CustomerOptions.selected.fname,
            producttype_code: this.ProductTypeOptions.selected.producttypecode,
            producttype_desc: this.ProductTypeOptions.selected.desc1,
            pqgradepd: this.PqgradeOptions.selected.pqgradecd,
            pqgrade_desc: this.PqgradeOptions.selected.pqgradedesc,
            stdwei_min: this.insertFoldSpec.stdwei_min, //0.00 ,
            stdwei_max: this.insertFoldSpec.stdwei_max, //0.00  ,
            twinesize_min: twinesize_min, //twinesize_min,
            twinesize_max: twinesize_max, //twinesize_max,
            depsize_min: this.depsize_min, //0.00,
            depsize_max: this.depsize_max, //0.00,
            depamt_min: this.depamt_min, //0.00,
            depamt_max: this.depamt_max, //0.00,
            len_min: this.len_min, // 0.00,
            len_max: this.len_max, // 0.00,
            knottype_code: knottype,
            linetype_code: linetype,
            stretchingtype_code: stretchingtype,
            old_color_code: old_color_code,
            new_color_code: new_color_code,
            color_code: color_code,
            fold_id: this.FoldOptions.selected.fold_id,
            fold_cd: this.FoldOptions.selected.fold_code,
            fold_name: this.FoldOptions.selected.fold_name,
            effdate_st: this.currentDate, //"2020-11-01",
            effdate_en: this.defaultExDate, //"2000-01-01",
            status: "A",
            min_product_type: min_product_type,
            min_productcate_grp: min_productcate_grp,
            min_twine_no: min_twine_no, // 0.00,
            min_line_no: min_line_no, // 0.00,
            min_word: min_word,
            min_product_grp: min_product_grp,
            max_product_type: max_product_type,
            max_productcate_grp: max_productcate_grp,
            max_twine_no: max_twine_no, //0.00,
            max_line_no: max_line_no, // 0.00,
            max_word: max_word,
            max_product_grp: max_product_grp,
            user_id: localStorage.getItem("UserID"),
            user_name: localStorage.getItem("User"),
          },
          callback: (res) => {
            if (res.status == "S") {
              swal("สำเร็จ!", "บันทึกข้อมูลเสร็จสิ้น", "success");
            } else {
              swal("ผิดพลาด!", "ไม่สามารถบันทึกข้อมูลได้", "error");
            }
            this.ResetInput();
            this.SearchSpec();
          },
        });
        this.openAddModal = false;
      } else {
        swal(
          "ข้อมูลไม่ถูกต้อง",
          "วันหมดอายุจะต้องมากกว่าวันที่ประกาศใช้และวันปัจจุบัน",
          "warning"
        );
      }
    },
    checkLinetypeVal() {
      var linetype = "";
      var knottype = "";
      var stretchingtype = "";
      if (this.LineTypeOptions.selected == null) {
        linetype = "";
      } else {
        linetype = this.LineTypeOptions.selected.linetypecode;
      }
      if (this.KnotTypeOptions.selected == null) {
        knottype = "";
      } else {
        knottype = this.KnotTypeOptions.selected.knot_type;
      }

      if (this.StretchingTypeOptions.selected == null) {
        stretchingtype = "";
      } else {
        stretchingtype = this.StretchingTypeOptions.selected.stretching_type;
      }
      this.checkEditFoldSpec(linetype, knottype, stretchingtype);
    },
    checkEditFoldSpec(linetype_code, knottype, stretchingtype) {
      let nowDate = new Date();
      let x = GetDate(nowDate.setDate(nowDate.getDate()));

      var statusLable = "A";
      if (
        (this.editFoldSpec.exdate > this.editFoldSpec.startdate &&
          this.editFoldSpec.exdate > x) ||
        this.editFoldSpec.exdate == "2000-01-01"
      ) {
        var cuscod = "";
        if (this.CustomerOptions.selected == null) {
          cuscod = this.editViewFoldSpec[0].cus_cod;
        } else {
          cuscod = this.CustomerOptions.selected.cuscod;
        }

        var max_twine_no = "";
        var min_twine_no = "";
        if (this.ProductSizeMaxOptions.selected == null) {
          max_twine_no = this.editViewFoldSpec[0].max_twine_no;
        } else {
          max_twine_no = this.ProductSizeMaxOptions.selected.twine_no;
        }
        if (this.ProductSizeMinOptions.selected == null) {
          min_twine_no = this.editViewFoldSpec[0].min_twine_no;
        } else {
          min_twine_no = this.ProductSizeMinOptions.selected.twine_no;
        }
        this.$root.api.Master_s_FoldSpecSave({
          data: {
            cuscod: cuscod,
          },
          callback: (res) => {
            var respondeExcept = [];
            for (let i = 0; i < res.length; i++) {
              if (res[i].foldspec_id != this.editViewFoldSpec[0].foldspec_id) {
                respondeExcept.push(res[i]);
              }
            }
            if (res.length != 0) {
              const twine = [];
              const depsize = [];
              const depamt = [];
              const len = [];
              const stdwei = [];

              for (let i = 0; i < respondeExcept.length; i++) {
                twine.push({
                  max: respondeExcept[i].max_twine_no,
                  min: respondeExcept[i].min_twine_no,
                });
                depsize.push({
                  max: respondeExcept[i].depsize_max,
                  min: respondeExcept[i].depsize_min,
                });
                depamt.push({
                  max: respondeExcept[i].depamt_max,
                  min: respondeExcept[i].depamt_min,
                });
                len.push({
                  max: respondeExcept[i].len_max,
                  min: respondeExcept[i].len_min,
                });
                stdwei.push({
                  max: respondeExcept[i].stdwei_max,
                  min: respondeExcept[i].stdwei_min,
                });
              }

              var twineResult = [];
              var depsizeResult = [];
              var depamtResult = [];
              var lenResult = [];
              var stdweiResult = [];

              for (let i = 0; i < twine.length; i++) {
                var x =
                  min_twine_no > twine[i].max || max_twine_no < twine[i].min;
                twineResult.push(x.toString());
              }
              for (let i = 0; i < depsize.length; i++) {
                var x =
                  this.editFoldSpec.depsize_min > depsize[i].max ||
                  this.editFoldSpec.depsize_max < depsize[i].min;
                depsizeResult.push(x.toString());
              }
              for (let i = 0; i < depamt.length; i++) {
                var x =
                  this.editFoldSpec.depamt_min > depamt[i].max ||
                  this.editFoldSpec.depamt_max < depamt[i].min;
                depamtResult.push(x.toString());
              }
              for (let i = 0; i < len.length; i++) {
                var x =
                  this.editFoldSpec.len_min > len[i].max ||
                  this.editFoldSpec.len_max < len[i].min;
                lenResult.push(x.toString());
              }
              for (let i = 0; i < stdwei.length; i++) {
                var x =
                  this.insertFoldSpec.stdwei_min > stdwei[i].max ||
                  this.insertFoldSpec.stdwei_max < stdwei[i].min;
                stdweiResult.push(x.toString());
              }
              var twineCheck = twineResult.includes("false");
              var depsizeCheck = depsizeResult.includes("false");
              var depamtCheck = depamtResult.includes("false");
              var lenCheck = lenResult.includes("false");
              var stdweiCheck = stdweiResult.includes("false");

              if (twineCheck == false) {
                this.SaveEditLableButton(
                  linetype_code,
                  knottype,
                  stretchingtype
                );
              } else {
                //   Next Check NOT PASS : DEPSIZE
                if (depsizeCheck == false) {
                  // DEPSIZE Check PASS Insert this
                  this.SaveEditLableButton(
                    linetype_code,
                    knottype,
                    stretchingtype
                  );
                } else {
                  // Next Checed NOT PASS : DEPAMP
                  if (depamtCheck == false) {
                    //   DEPAMP Check PASS Insert this
                    this.SaveEditLableButton(
                      linetype_code,
                      knottype,
                      stretchingtype
                    );
                  } else {
                    //   Next Checed NOT PASS  : LEN
                    if (lenCheck == false) {
                      // LEN Check PASS Insert this
                      this.SaveEditLableButton(
                        linetype_code,
                        knottype,
                        stretchingtype
                      );
                    }
                    if (stdweiCheck == false) {
                      //   STDWEI Check PASS Insert this
                      this.SaveEditLableButton(
                        linetype_code,
                        knottype,
                        stretchingtype
                      );
                    } else {
                      // PLEASE INSERT NEW ARRAY
                      swal(
                        "ข้อมูลซ้ำซ้อน",
                        "กรุณายืนยันการบันทึกข้อมูลมาตราฐานเงื่อนไขการพับมัดนี้",
                        "warning",
                        {
                          buttons: {
                            cancel: "ยกเลิก",
                            catch: {
                              text: "บันทึก",
                              value: "catch",
                            },
                          },
                        }
                      ).then((value) => {
                        switch (value) {
                          case "catch":
                            this.SaveEditLableButton(
                              linetype_code,
                              knottype,
                              stretchingtype
                            );
                        }
                      });
                    }
                  }
                }
              }
            }
          },
        });
      } else {
        swal(
          "ข้อมูลไม่ถูกต้อง",
          "วันหมดอายุจะต้องมากกว่าวันที่ประกาศใช้และวันปัจจุบัน",
          "warning"
        );
      }
    },
    getDataStrap() {
      this.dataActive = [];
      this.dataInactive = [];
      this.dataCancel = [];
      for (let i = 0; i < this.dataFoldCus.length; i++) {
        if (this.dataFoldCus[i].status == "A") {
          this.dataActive.push({
            cus_cod: this.dataFoldCus[i].cus_cod,
            cus_id: this.dataFoldCus[i].cus_id,
            tsale: this.dataFoldCus[i].tsale,
            cus_name: this.dataFoldCus[i].cus_name,
          });
        } else if (this.dataFoldCus[i].status == "I") {
          this.dataInactive.push({
            cus_cod: this.dataFoldCus[i].cus_cod,
            cus_id: this.dataFoldCus[i].cus_id,
            tsale: this.dataFoldCus[i].tsale,
            cus_name: this.dataFoldCus[i].cus_name,
          });
        } else if (this.dataFoldCus[i].status == "C") {
          this.dataCancel.push({
            cus_cod: this.dataFoldCus[i].cus_cod,
            cus_id: this.dataFoldCus[i].cus_id,
            tsale: this.dataFoldCus[i].tsale,
            cus_name: this.dataFoldCus[i].cus_name,
          });
        }
      }
    },
    setPagesActive() {
      this.pagesA = [];
      let numberOfPages = Math.ceil(this.dataFoldCusCount / this.perPageA);
      for (let index = 0; index < numberOfPages; index++) {
        this.pagesA.push(index);
      }
      this.pageA = 1;
    },
    setPagesInactive() {
      this.pagesI = [];
      let numberOfPages = Math.ceil(this.dataIFoldCusCount / this.perPageI);
      for (let index = 0; index < numberOfPages; index++) {
        this.pagesI.push(index);
      }
      this.pageI = 1;
    },
    setPagesCancel() {
      this.pagesC = [];
      let numberOfPages = Math.ceil(this.dataCFoldCusCount / this.perPageI);
      for (let index = 0; index < numberOfPages; index++) {
        this.pagesC.push(index);
      }
      this.pageC = 1;
    },
    paginate(Active) {
      let pageA = this.pageA;
      let perPageA = this.perPageA;
      let from = pageA * perPageA - perPageA;
      let to = pageA * perPageA;
      return Active.slice(from, to);
    },
    paginateInactive(Inactive) {
      let pageI = this.pageI;
      let perPageI = this.perPageI;
      let from = pageI * perPageI - perPageI;
      let to = pageI * perPageI;
      return Inactive.slice(from, to);
    },
    paginateCancel(Cancel) {
      let pageC = this.pageC;
      let perPageC = this.perPageC;
      let from = pageC * perPageC - perPageC;
      let to = pageC * perPageC;
      return Cancel.slice(from, to);
    },
  },
  computed: {
    displayedPostsActive() {
      return this.paginate(this.dataActive);
    },
    displayedPostsInactive() {
      return this.paginateInactive(this.dataInactive);
    },
    displayedPostsCancel() {
      return this.paginateCancel(this.dataCancel);
    },
  },
  watch: {
    Active() {
      this.setPagesActive();
    },
    Inactive() {
      this.setPagesInactive();
    },
    Cancel() {
      this.setPagesCancel();
    },
  },
};
