import imageMixins from "./imageMixins";
import swal from "sweetalert";
import DatePicker from "vue2-datepicker";
import "vue2-datepicker/index.css";
import { GetObjValJoin, GetDate, FileToBase64 } from "@/shared/utils";
import setupmixin from "@/shared/setupmixin";
import { loadPartialConfig } from "@babel/core";

export default {
  mixins: [imageMixins],
  components: {
    DatePicker,
  },
  data() {
    return {
      HReport: true,
      search: {
        bag_id: "",
        bag_cd: "",
        txtsearch: "",
      },

      DataTable: [],
      DataCancelTable: [],

      // Active
      dataActive: [],
      dataActiveCount: 0,
      pageA: 1,
      perPageA: 10,
      pagesA: [],

      // InActive
      dataInactive: [],
      dataInactiveCount: 0,
      pageI: 1,
      perPageI: 5,
      pagesI: [],

      // Cancel
      dataCancel: [],
      dataCancelCount: 0,
      pageC: 1,
      perPageC: 5,
      pagesC: [],

      checked: [],
      checkedItem: "",

      itemsTabs1: true,
      itemsTabs2: false,
      itemsTabs3: false,

      openAddModal: false,
      openEditModal: false,
      openViewModal: false,
      openDeleteModal: false,
      openLastOrderBagModal: false,

      insertData: {
        bag_cd: "",
        bag_name_th: "",
        bag_name_en: "",
        effdate_st: "",
        effdate_en: "",
        width: "",
        length: "",
        bag_cd_old: "",
        path_pic: "",
      },
      EditData: {
        bag_id: "",
        bag_cd: "",
        bag_name_th: "",
        bag_name_en: "",
        width: "",
        length: "",
        bag_cd_old: "",
        effdate_en: "",
        effdate_st: "",
        path_pic: "",
        old_code: "",
      },
      ViewData: {
        bag_id: "",
        bag_cd: "",
        bag_name_th: "",
        bag_name_en: "",
        effdate_st: "",
        effdate_en: "",
        width: "",
        length: "",
        bag_cd_old: "",
        path_pic: "",
      },

      LastOrderData: {
        bag_id: "",
        bag_cd: "",
        bag_name_th: "",
        bag_name_en: "",
        width: "",
        length: "",
        bag_cd_old: "",
        last_date_use: "",
        last_order_use: "",
        last_user_id: "",
        last_user_name: "",
        last_user_datetime: "",
      },
      LastOrderBagData: {
        bag_id: "",
        bag_cd: "",
        bag_name_th: "",
        bag_cd_old: "",
      },
      DeleteData: {},
      nowDate: "",
      old_effdate_st: "",
    };
  },
  updated() {
    if (this.insertData.bag_cd == "") {
      $("#bag_cd").focus();
    }
  },
  created() {
    let nowDate = new Date();
    this.nowDate = GetDate(nowDate.setDate(nowDate.getDate()));
    this.insertData.effdate_st = this.nowDate;
    this.insertData.effdate_en = "2000-01-01";
  },
  methods: {
    SearchData() {
      this.DataTable = [];
      this.DataCancelTable = [];
      this.dataActiveCount = 0;
      this.dataInactiveCount = 0;
      this.dataCancelCount = 0;
      this.checked = [];
      this.checkedItem = "";

      this.$root.api.Master_s_BagSearch({
        data: {
          bag_cd: this.search.bag_cd,
          txtSearch: this.search.txtsearch,
        },
        callback: (res) => { 
          if (res.length == 0) {
            swal({
              title: "ผิดพลาด",
              text: "ไม่พบข้อมูลถุงบรรจุสินค้า",
              icon: "warning",
              button: "ตกลง",
            });
          } else {
            this.DataTable = res;
            for (let i = 0; i < res.length; i++) {
              if (res[i].status == "A") {
                this.dataActiveCount = this.dataActiveCount + 1;
              } else if (res[i].status == "I") {
                this.dataInactiveCount = this.dataInactiveCount + 1;
              }
            }
            this.GetDataTable();
          }
          this.$root.api.Master_s_BagSearchCancel({
            data: {
              sticker_cd: this.search_cd,
              txtsearch: this.search_txtsearch,
            },
            callback: (res) => {
              this.DataCancelTable = res;
              for (let i = 0; i < res.length; i++) {
              this.dataCancelCount = this.dataCancelCount + 1;
              }
              this.GetCancelDataTable();
            },
          });
        },
      });
    },
    GetDataTable() {
      this.dataActive = [];
      this.dataInactive = [];
      for (let i = 0; i < this.DataTable.length; i++) {
        if (this.DataTable[i].status == "A") {
          this.dataActive.push({
            bag_id: this.DataTable[i].bag_id,
            bag_cd: this.DataTable[i].bag_cd,
            bag_name_th: this.DataTable[i].bag_name_th,
            bag_name_en: this.DataTable[i].bag_name_en,
            bag_cd_old: this.DataTable[i].bag_cd_old,
            width: this.DataTable[i].width,
            length: this.DataTable[i].length,
            path_pic: this.DataTable[i].path_pic,
            status: this.DataTable[i].status,
            effdate_st: this.DataTable[i].effdate_st,
            effdate_en: this.DataTable[i].effdate_en,
            user_id: this.DataTable[i].user_id,
            user_name: this.DataTable[i].user_name,
            user_date: this.DataTable[i].user_date,
          });
        } else if (this.DataTable[i].status == "I") {
          this.dataInactive.push({
            bag_id: this.DataTable[i].bag_id,
            bag_cd: this.DataTable[i].bag_cd,
            bag_name_th: this.DataTable[i].bag_name_th,
            bag_name_en: this.DataTable[i].bag_name_en,
            bag_cd_old: this.DataTable[i].bag_cd_old,
            width: this.DataTable[i].width,
            length: this.DataTable[i].length,
            path_pic: this.DataTable[i].path_pic,
            status: this.DataTable[i].status,
            effdate_st: this.DataTable[i].effdate_st,
            effdate_en: this.DataTable[i].effdate_en,
            user_id: this.DataTable[i].user_id,
            user_name: this.DataTable[i].user_name,
            user_date: this.DataTable[i].user_date,
          });
        }
      }
    },
    GetCancelDataTable() {
      this.dataCancel = [];
      for (let i = 0; i < this.DataCancelTable.length; i++) {
        this.dataCancel.push({
          bag_id: this.DataCancelTable[i].bag_id,
          bag_cd: this.DataCancelTable[i].bag_cd,
          bag_name_th: this.DataCancelTable[i].bag_name_th,
          bag_name_en: this.DataCancelTable[i].bag_name_en,
          bag_cd_old: this.DataCancelTable[i].bag_cd_old,
          width: this.DataCancelTable[i].width,
          length: this.DataCancelTable[i].length,
          path_pic: this.DataCancelTable[i].path_pic,
          status: this.DataCancelTable[i].status,
          effdate_st: this.DataCancelTable[i].effdate_st,
          effdate_en: this.DataCancelTable[i].effdate_en,
          user_id: this.DataCancelTable[i].user_id,
          user_name: this.DataCancelTable[i].user_name,
          user_date: this.DataCancelTable[i].user_date,
          cancel_id: this.DataCancelTable[i].cancel_id,
          cancel_name: this.DataCancelTable[i].cancel_name,
          cancel_date: this.DataCancelTable[i].cancel_date,
        });
      }
    },
    checkItems(val) {
      this.checkedItem = $("input[type=checkbox][name=Choosed]:checked")
        .map(function(_, el) {
          return $(el).val();
        })
        .get();
    },
    CheckDupCode() {
      if (this.openAddModal == true) {
        if (this.insertData.bag_cd.length < 2) {
          swal({
            title: "ผิดพลาด!",
            text: "รหัสถุงบรรจุสินค้า ต้องมี 2 หลัก",
            icon: "error",
            button: "ตกลง",
          });
          $("#bag_cd").focus();
        } else {
          this.$root.api.Master_s_BagSearch({
            data: {
              bag_cd: this.insertData.bag_cd,
            },
            callback: (res) => {
              for (let i = 0; i < res.length; i++) {
                if (res.length != 0 && res[0].status == "A") {
                  swal({
                    title: "รหัสถุงซ้ำ",
                    text: "กรุณากรอกรหัสถุงบรรจุสินค้าใหม่",
                    icon: "warning",
                    button: "ตกลง",
                  });
                  this.insertData.bag_cd = "";
                }
              }
            },
          });
        }
      } else if (this.openEditModal == true) {
        if (this.EditData.bag_cd.length < 2) {
          swal({
            title: "ผิดพลาด!",
            text: "รหัสถุงบรรจุสินค้าต้องมี 2 หลัก",
            icon: "error",
            button: "ตกลง",
          });
          $("#bag_cd_edit").focus();
        } else {
          this.$root.api.Master_s_BagSearch({
            data: {
              bag_cd: this.EditData.bag_cd,
            },
            callback: (res) => {
              if (res.length != 0) {
                if (this.EditData.bag_cd.toUpperCase() != this.EditData.old_code) {
                  swal({
                    title: "รหัสถุงซ้ำ",
                    text: "กรุณากรอกรหัสถุงบรรจุสินค้าใหม่",
                    icon: "warning",
                    button: "ตกลง",
                  });
                  this.EditData.bag_cd = "";
                }
              }
            },
          });
        }
      }
    },
    InsertSaveButton() {
      if (
        this.insertData.bag_cd != "" &&
        this.insertData.bag_name_th != "" &&
        this.insertData.bag_name_en != "" &&
        this.insertData.width != "" &&
        this.insertData.length != ""
      ) {
        if (
          this.insertData.effdate_en == "2000-01-01" ||
          (this.insertData.effdate_en > this.nowDate &&
            this.insertData.effdate_en > this.insertData.effdate_st)
        ) {
          this.$root.api.Master_s_BagSave({
            data: {
              bag_cd: this.insertData.bag_cd.toUpperCase(),
              bag_name_th: this.insertData.bag_name_th,
              bag_name_en: this.insertData.bag_name_en,
              bag_cd_old: this.insertData.bag_cd_old.toUpperCase(),
              width: this.insertData.width,
              length: this.insertData.length,
              path_pic: this.insertData.path_pic,
              status: "A",
              effdate_st: this.insertData.effdate_st,
              effdate_en: this.insertData.effdate_en,
              user_id: localStorage.getItem("UserID"),
              user_name: localStorage.getItem("User"),
            },
            callback: (res) => {
              if (res.status == "S") {
                swal({
                  title: "สำเร็จ",
                  text: "บันทึกข้อมูลถุงบรรจุสินค้า เสร็จสิ้น",
                  icon: "success",
                  button: "ตกลง",
                });
                this.SearchData();
                this.resetData();
                this.openAddModal = false;
              } else if (res.status == "F") {
                swal({
                  title: "ผิดพลาด!",
                  text: "ไม่สามารถบันทึกข้อมูลถุงบรรจุสินค้าได้",
                  icon: "error",
                  button: "ตกลง",
                });
                this.resetData();
                this.openAddModal = false;
              }
            },
          });
          this.openAddModal = false;
        } else {
          swal({
            title: "ข้อมูลไม่ถูกต้อง!",
            text: "วันหมดอายุจะต้องมากกว่าวันที่ประกาศใช้และวันปัจจุบัน",
            icon: "error",
            button: "ตกลง",
          });
        }
      } else {
        swal({
          title: "ผิดพลาด",
          text: "กรุณากรอกข้อมูลถุงบรรจุสินค้าให้ครบถ้วน",
          icon: "error",
          button: "ตกลง",
        });
      }
    },
    resetCheck() {
      this.checked = [];
      this.checkedItem = "";
    },
    resetData() {
      this.insertData.bag_cd = "";
      this.insertData.bag_name_th = "";
      this.insertData.bag_name_en = "";
      this.insertData.effdate_st = this.nowDate;
      this.insertData.effdate_en = "2000-01-01";
      this.insertData.width = "";
      this.insertData.length = "";
      this.insertData.bag_cd_old = "";
      this.insertData.path_pic = "";

      document.getElementById("filename").value = "";
      document.getElementById("showImage").removeAttribute("src");
      document.getElementById("filename-edit").value = "";
    },
    LastOrderBaView(bag_id, bag_cd, bag_cd_old, bag_name_th) {
      this.LastOrderBagData.bag_cd = bag_cd;
      this.LastOrderBagData.bag_name_th = bag_name_th;
      this.LastOrderBagData.bag_cd_old = bag_cd_old;
      this.openLastOrderBagModal = true;
      this.$root.api.View_master_v_bagorderSearch({
        data: {
          bag_id: bag_id,
          bag_cd: bag_cd,
          bag_cd_old: bag_cd_old,
        },
        callback: (res) => {
          this.LastOrderData.bag_id = res;
        },
      });
    },
    EditView() {
      this.openEditModal = true;
      var x = this.checkedItem[0];
      this.$root.api.Master_s_BagSearch({
        data: {
          bag_id: x,
        },
        callback: (res) => {
          this.EditData.bag_id = res[0].bag_id;
          this.EditData.bag_cd = res[0].bag_cd;
          this.EditData.bag_name_th = res[0].bag_name_th;
          this.EditData.bag_name_en = res[0].bag_name_en;
          this.EditData.effdate_st = res[0].effdate_st.split("T")[0];
          this.EditData.effdate_en = res[0].effdate_en.split("T")[0];
          this.EditData.width = res[0].width.toFixed(2);
          this.EditData.length = res[0].length.toFixed(2);
          this.EditData.bag_cd_old = res[0].bag_cd_old.trim();
          this.EditData.path_pic = res[0].path_pic;
          this.old_effdate_st = res[0].effdate_st.split("T")[0];
        },
      });
    },
    EditSaveButton() {
      if (
        this.EditData.effdate_en == "2000-01-01" ||
        (this.EditData.effdate_en > this.nowDate &&
          this.EditData.effdate_en > this.EditData.effdate_st)
      ) {
        this.$root.api.Master_s_BagSave({
          data: {
            bag_id: this.EditData.bag_id,
            bag_cd: this.EditData.bag_cd.toUpperCase(),
            bag_name_th: this.EditData.bag_name_th,
            bag_name_en: this.EditData.bag_name_en,
            bag_cd_old: this.EditData.bag_cd_old.toUpperCase(),
            width: this.EditData.width,
            length: this.EditData.length,
            path_pic: this.EditData.path_pic,
            status: "A",
            effdate_st: this.EditData.effdate_st,
            effdate_en: this.EditData.effdate_en,
            user_id: localStorage.getItem("UserID"),
            user_name: localStorage.getItem("User"),
          },
          callback: (res) => {
            if (res.status == "S") {
              swal({
                title: "สำเร็จ",
                text: "บันทึกข้อมูลถุงบรรจุสินค้า เสร็จสิ้น",
                icon: "success",
                button: "ตกลง",
              });
              this.SearchData();
              this.resetData();
              this.openEditModal = false;
            } else if (res.status == "F") {
              swal({
                title: "ผิดพลาด!",
                text: "ไม่สามารถบันทึกข้อมูลถุงบรรจุสินค้าได้",
                icon: "error",
                button: "ตกลง",
              });
              this.resetData();
              this.openEditModal = false;
            }
          },
        });
        this.openEditModal = false;
      } else {
        swal({
          title: "ข้อมูลไม่ถูกต้อง!",
          text: "วันหมดอายุจะต้องมากกว่าวันที่ประกาศใช้และวันปัจจุบัน",
          icon: "error",
          button: "ตกลง",
        });
      }
    },
    ViewButton() {
      this.openViewModal = true;
      var x = this.checkedItem[0];
      if (this.itemsTabs3 == false) {
        this.$root.api.Master_s_BagSearch({
          data: {
            bag_id: x,
          },
          callback: (res) => {
            this.ViewData.bag_id = res[0].bag_id;
            this.ViewData.bag_cd = res[0].bag_cd;
            this.ViewData.bag_name_th = res[0].bag_name_th;
            this.ViewData.bag_name_en = res[0].bag_name_en;
            this.ViewData.effdate_st = res[0].effdate_st.split("T")[0];
            this.ViewData.effdate_en = res[0].effdate_en.split("T")[0];
            this.ViewData.width = res[0].width.toFixed(2);
            this.ViewData.length = res[0].length.toFixed(2);
            this.ViewData.bag_cd_old = res[0].bag_cd_old;
            this.ViewData.path_pic = res[0].path_pic;
          },
        });
      } else if (this.itemsTabs3 == true) {
        this.$root.api.Master_s_BagSearchCancel({
          data: {
            bag_id: x,
          },
          callback: (res) => {
            this.ViewData.bag_id = res[0].bag_id;
            this.ViewData.bag_cd = res[0].bag_cd;
            this.ViewData.bag_name_th = res[0].bag_name_th;
            this.ViewData.bag_name_en = res[0].bag_name_en;
            this.ViewData.effdate_st = res[0].effdate_st.split("T")[0];
            this.ViewData.effdate_en = res[0].effdate_en.split("T")[0];
            this.ViewData.width = res[0].width.toFixed(2);
            this.ViewData.length = res[0].length.toFixed(2);
            this.ViewData.bag_cd_old = res[0].bag_cd_old;
            this.ViewData.path_pic = res[0].path_pic;
          },
        });
      }
    },
    DeleteButton() {
      this.openDeleteModal = true;
      var x = this.checkedItem[0];
      this.$root.api.Master_s_BagSearch({
        data: {
          bag_id: x,
        },
        callback: (res) => {
          this.DeleteData = res[0];
        },
      });
    },
    DeleteSaveButton() {
      this.$root.api.Master_s_BagMove({
        data: {
          bag_move: [
            {
              bag_id: this.DeleteData.bag_id,
              user_id: localStorage.getItem("UserID"),
              user_name: localStorage.getItem("User"),
            },
          ],
        },
        callback: (res) => {
          if (res.status == "S") {
            swal({
              title: "สำเร็จ",
              text: "ลบข้อมูลถุงบรรจุสินค้าเสร็จสิ้น",
              icon: "success",
              button: "ตกลง",
            });
            this.SearchData();
          } else {
            swal({
              title: "ผิดพลาด!",
              text: "ไม่สามารถลบข้อมูลถุงบรรจุสินค้าได้",
              icon: "error",
              button: "ตกลง",
            });
            this.SearchData();
          }
          this.openDeleteModal = false;
          this.SearchData();
        },
      });
    },
    setPagesActive() {
      this.pagesA = [];
      let numberOfPages = Math.ceil(this.dataActiveCount / this.perPageA);
      for (let index = 0; index < numberOfPages; index++) {
        this.pagesA.push(index);
      }
      this.pageA = 1;
    },
    setPagesInactive() {
      this.pagesI = [];
      let numberOfPages = Math.ceil(this.dataInactiveCount / this.perPageI);
      for (let index = 0; index < numberOfPages; index++) {
        this.pagesI.push(index);
      }
      this.pageI = 1;
    },
    setPagesCancel() {
      this.pagesC = [];
      let numberOfPages = Math.ceil(this.dataCStickerSpecCount / this.perPageI);
      for (let index = 0; index < numberOfPages; index++) {
        this.pagesC.push(index);
      }
      this.pageC = 1;
    },
    paginate(Active) {
      let pageA = this.pageA;
      let perPageA = this.perPageA;
      let from = pageA * perPageA - perPageA;
      let to = pageA * perPageA;
      return Active.slice(from, to);
    },
    paginateInactive(Inactive) {
      let pageI = this.pageI;
      let perPageI = this.perPageI;
      let from = pageI * perPageI - perPageI;
      let to = pageI * perPageI;
      return Inactive.slice(from, to);
    },
    paginateCancel(Cancel) {
      let pageC = this.pageC;
      let perPageC = this.perPageC;
      let from = pageC * perPageC - perPageC;
      let to = pageC * perPageC;
      return Cancel.slice(from, to);
    },
  },
  computed: {
    displayedPostsActive() {
      return this.paginate(this.dataActive);
    },
    displayedPostsInactive() {
      return this.paginateInactive(this.dataInactive);
    },
    displayedPostsCancel() {
      return this.paginateCancel(this.dataCancel);
    },
  },
  watch: {
    Active() {
      this.setPagesActive();
    },
    Inactive() {
      this.setPagesInactive();
    },
    Cancel() {
      this.setPagesCancel();
    },
  },
};
