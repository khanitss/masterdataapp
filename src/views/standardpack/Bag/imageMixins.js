import { GetObjValJoin, GetDate, FileToBase64 } from "@/shared/utils";

export default {
  methods: {
    showPreview() {
      document.getElementById("showImage").removeAttribute("src");
      var filename = document.getElementById("filename");
      filename.onchange = function() {
        var files = filename.files[0];
        var reader = new FileReader();
        reader.readAsDataURL(files);
        reader.onload = function() {
          var result = reader.result;
          document.getElementById("showImage").src = result;
        };
      };
    },
    SaveImage(event) {
      if (event.target.files.length) {
        let file = event.target.files[0];
        if (
          file &&
          (/^image[/]jpeg/.test(file.type) || /^image[/]png/.test(file.type))
        ) {
          FileToBase64(file).then((pic) => {
            this.insertData.path_pic = pic;
            return;
          });
        } else {
        }
      }
      this.file = null;
      this.insertData.path_pic = "";
    },
    showPreviewEdit() {
      var filename = document.getElementById("filename-edit");
      filename.onchange = function() {
        var files = filename.files[0];
        var reader = new FileReader();
        reader.readAsDataURL(files);
        reader.onload = function() {
          var result = reader.result;
          document.getElementById("showImage2").src = result;
        };
      };
    },
    SaveEditImage(event) {
      if (event.target.files.length) {
        let file = event.target.files[0];
        if (
          file &&
          (/^image[/]jpeg/.test(file.type) || /^image[/]png/.test(file.type))
        ) {
          FileToBase64(file).then((pic) => {
            this.EditData.path_pic = pic;
            return;
          });
        } else {
        }
        this.file = null;
        this.EditData.path_pic = null;
      }
    },
  },
};
