import Multiselect from "vue-multiselect";
import {
  GetObjVal,
  ObjCopy,
  GetObjValJoin,
  GetObjArr,
  GetDate,
  GetDateView,
  FileToBase64,
} from "@/shared/utils";
import swal from "sweetalert";
import DatePicker from "vue2-datepicker";
import "vue2-datepicker/index.css";
import imageMixins from "./imageMixins";

export default {
  components: {
    Multiselect,
    DatePicker,
  },
  mixins: [imageMixins],
  data() {
    return {
      HReport: true,
      search: {
        rope_id: "",
        rope_cd: "",
        txtsearch: "",
      },

      DataTable: [],
      DataCancelTable: [],

      // Active
      dataActive: [],
      dataActiveCount: 0,
      pageA: 1,
      perPageA: 10,
      pagesA: [],

      // InActive
      dataInactive: [],
      dataInactiveCount: 0,
      pageI: 1,
      perPageI: 5,
      pagesI: [],

      // Cancel
      dataCancel: [],
      dataCancelCount: 0,
      pageC: 1,
      perPageC: 5,
      pagesC: [],

      checked: [],
      checkedItem: "",
      itemsTabs1: true,
      itemsTabs2: false,
      itemsTabs3: false,

      openAddModal: false,
      openEditModal: false,
      openViewModal: false,
      openDeleteModal: false,

      nowDate: "",
      old_effdate_st: "",

      ItemsCodeOptions: {
        lists: [],
        selected: null,
      },
      insertData: {
        rope_cd: "",
        rope_name: "",
        effdate_st: "",
        effdate_en: "",
        rope_cd_old: "",
        itemcode: "",
        path_pic: "",
      },
      EditData: {
        rope_cd: "",
        rope_name: "",
        effdate_st: "",
        effdate_en: "",
        rope_cd_old: "",
        itemcode: "",
        path_pic: "",
        old_rope_cd: "",
        itemcode_name: "",
      },
      ViewData: {
      },
      DeleteData : {}
    };
  },
  created() {
    let nowDate = new Date();
    this.nowDate = GetDate(nowDate.setDate(nowDate.getDate()));
    this.insertData.effdate_st = this.nowDate;
    this.insertData.effdate_en = "2000-01-01";

    this.$root.api.View_itemSearchAllItemCode({
      data: {
        ItemCode: "0825",
        ItemGrp: "0825",
      },
      callback: (res) => {
        res.forEach(
          (x) => (x._label = GetObjValJoin(x, ["itemCode", "itemName"]))
        );
        this.ItemsCodeOptions.lists = res;
      },
    });
  },
  updated() {
    if (this.insertData.rope_cd == "") {
      $("#rope_cd").focus();
    }
    if (this.EditData.rope_cd != "") {
      $("#rope_cd_edit").focus();
    }
  },
  methods: {
    SearchData() {
      this.DataTable = [];
      this.DataCancelTable = [];
      this.dataActiveCount = 0;
      this.dataInactiveCount = 0;
      this.dataCancelCount = 0;
      this.checked = [];
      this.checkedItem = "";

      this.$root.api.Master_s_RopeSearch({
        data: {
          rope_cd: this.search.rope_cd,
          txtSearch: this.search.txtsearch,
        },
        callback: (res) => {
          if (res.length == 0) {
            swal({
              title: "ผิดพลาด",
              text: "ไม่พบข้อมูลเชือกและฟาง",
              icon: "warning",
              button: "ตกลง",
            });
          } else {
            this.DataTable = res;
            for (let i = 0; i < res.length; i++) {
              if (res[i].status == "A") {
                this.dataActiveCount = this.dataActiveCount + 1;
              } else if (res[i].status == "I") {
                this.dataInactiveCount = this.dataInactiveCount + 1;
              }
            }
            this.GetDataTable();
          }
        },
      });
    },
    GetDataTable() {
      this.dataActive = [];
      this.dataInactive = [];
      this.dataCancel = [];

      for (let i = 0; i < this.DataTable.length; i++) {
        if (this.DataTable[i].status == "A") {
          this.dataActive.push({
            rope_id: this.DataTable[i].rope_id,
            rope_cd: this.DataTable[i].rope_cd,
            rope_name: this.DataTable[i].rope_name,
            rope_cd_old: this.DataTable[i].rope_cd_old,
            path_pic: this.DataTable[i].path_pic,
            status: this.DataTable[i].status,
            effdate_st: this.DataTable[i].effdate_st,
            effdate_en: this.DataTable[i].effdate_en,
            user_id: this.DataTable[i].user_id,
            user_name: this.DataTable[i].user_name,
            user_date: this.DataTable[i].user_date,
          });
        } else if (this.DataTable[i].status == "I") {
          this.dataInactive.push({
            rope_id: this.DataTable[i].rope_id,
            rope_cd: this.DataTable[i].rope_cd,
            rope_name: this.DataTable[i].rope_name,
            rope_cd_old: this.DataTable[i].rope_cd_old,
            path_pic: this.DataTable[i].path_pic,
            status: this.DataTable[i].status,
            effdate_st: this.DataTable[i].effdate_st,
            effdate_en: this.DataTable[i].effdate_en,
            user_id: this.DataTable[i].user_id,
            user_name: this.DataTable[i].user_name,
            user_date: this.DataTable[i].user_date,
          });
        } else if (this.DataTable[i].status == "C") {
          this.dataCancel.push({
            rope_id: this.DataTable[i].rope_id,
            rope_cd: this.DataTable[i].rope_cd,
            rope_name: this.DataTable[i].rope_name,
            rope_cd_old: this.DataTable[i].rope_cd_old,
            path_pic: this.DataTable[i].path_pic,
            status: this.DataTable[i].status,
            effdate_st: this.DataTable[i].effdate_st,
            effdate_en: this.DataTable[i].effdate_en,
            user_id: this.DataTable[i].user_id,
            user_name: this.DataTable[i].user_name,
            user_date: this.DataTable[i].user_date,
            cancel_id: this.DataTable[i].cancel_id,
            cancel_name: this.DataTable[i].cancel_name,
            cancel_date: this.DataTable[i].cancel_date,
          });
        }
      }
    },
    checkItems(val) {
      this.checkedItem = $("input[type=checkbox][name=Choosed]:checked")
        .map(function(_, el) {
          return $(el).val();
        })
        .get();
    },
    InsertSaveButton() {
      var itemcode = "";
      if (this.ItemsCodeOptions.selected != null) {
        itemcode = this.ItemsCodeOptions.selected.itemCode;
      } else {
        itemcode = "";
      }

      if (this.insertData.rope_cd != "" && this.insertData.rope_name != "") {
        if (
          this.insertData.effdate_en == "2000-01-01" ||
          (this.insertData.effdate_en > this.nowDate &&
            this.insertData.effdate_en > this.insertData.effdate_st)
        ) {
          this.$root.api.Master_s_RopeSave({
            data: {
              rope_cd: this.insertData.rope_cd.toUpperCase(),
              rope_name: this.insertData.rope_name,
              rope_cd_old: this.insertData.rope_cd_old.toUpperCase(),
              path_pic: this.insertData.path_pic,
              status: "A",
              itemcode: itemcode,
              effdate_st: this.insertData.effdate_st,
              effdate_en: this.insertData.effdate_en,
              user_id: localStorage.getItem("UserID"),
              user_name: localStorage.getItem("User"),
            },
            callback: (res) => {
              if (res.status == "S") {
                swal({
                  title: "สำเร็จ",
                  text: "บันทึกข้อมูลเชือกและฟางเสร็จสิ้น",
                  icon: "success",
                  button: "ตกลง",
                });
                this.SearchData();
                this.resetData();
                this.openAddModal = false;
              } else if (res.status == "F") {
                swal({
                  title: "ผิดพลาด!",
                  text: "ไม่สามารถบันทึกข้อมูลเชือกและฟางได้",
                  icon: "error",
                  button: "ตกลง",
                });
                this.resetData();
                this.openAddModal = false;
              }
            },
          });
          this.openAddModal = false;
        } else {
          swal({
            title: "ข้อมูลไม่ถูกต้อง!",
            text: "วันหมดอายุจะต้องมากกว่าวันที่ประกาศใช้และวันปัจจุบัน",
            icon: "error",
            button: "ตกลง",
          });
        }
      } else {
        swal({
          title: "ผิดพลาด",
          text: "กรุณากรอกข้อมูลเชือกและฟางให้ครบถ้วน",
          icon: "error",
          button: "ตกลง",
        });
      }
    },
    CheckDupCode() {
      if (this.openAddModal == true) {
        if (this.insertData.rope_cd.length < 2) {
          swal({
            title: "ผิดพลาด!",
            text: "รหัสเชือกและฟางต้องมี 2 หลัก",
            icon: "error",
            button: "ตกลง",
          });
          $("#rope_cd").focus();
        } else {
          this.$root.api.Master_s_RopeSearchActive({
            data: {
              rope_cd: this.insertData.rope_cd,
            },
            callback: (res) => {
              if (res.length != 0 && res[0].status == "A") {
                swal({
                  title: "รหัสเชือกและฟางซ้ำ",
                  text: "กรุณากรอกรหัสเชือกและฟางใหม่",
                  icon: "warning",
                  button: "ตกลง",
                });
                this.insertData.rope_cd = "";
              }
            },
          });
        }
      } else if (this.openEditModal == true) {
        if (this.EditData.rope_cd.length < 2) {
          swal({
            title: "ผิดพลาด!",
            text: "รหัสเชือกและฟางต้องมี 2 หลัก",
            icon: "error",
            button: "ตกลง",
          });
          $("#rope_cd_edit").focus();
        } else {
          this.$root.api.Master_s_RopeSearch({
            data: {
              rope_cd: this.EditData.rope_cd,
            },
            callback: (res) => {
              if (res.length != 0) {
                if (
                  this.EditData.rope_cd.toUpperCase() !=
                  this.EditData.old_rope_cd
                ) {
                  swal({
                    title: "รหัสเชือกและฟางซ้ำ",
                    text: "กรุณากรอกรหัสเชือกและฟางใหม่",
                    icon: "warning",
                    button: "ตกลง",
                  });
                  this.EditData.rope_cd = "";
                }
              }
            },
          });
        }
      }
    },
    EditButton() {
      this.openEditModal = true;
      var x = this.checkedItem[0];
      this.$root.api.Master_s_RopeSearch({
        data: {
          rope_id: x,
        },
        callback: (res) => {
          this.EditData = res[0];
          this.EditData.effdate_st = res[0].effdate_st.split("T")[0];
          this.EditData.effdate_en = res[0].effdate_en.split("T")[0];
          this.EditData.old_rope_cd = res[0].rope_cd;
          this.old_effdate_st = res[0].effdate_st.split("T")[0];
          console.log(this.EditData);

          this.$root.api.View_itemSearchItemCode({
            data: {
              ItemCode: res[0].itemcode,
            },
            callback: (res) => {
              res.forEach(
                (x) => (x._label = GetObjValJoin(x, ["itemCode", "itemName"]))
              );
              this.ItemsCodeOptions.selected = res[0];
              this.EditData.itemcode_name = res[0]._label;
            },
          });
        },
      });
    },
    EditSaveButton() {
      var itemcode = "";
      if (this.ItemsCodeOptions.selected != null) {
        itemcode = this.ItemsCodeOptions.selected.itemCode;
      } else {
        itemcode = "";
      }

      if (
        this.EditData.effdate_en == "2000-01-01" ||
        (this.EditData.effdate_en > this.nowDate &&
          this.EditData.effdate_en > this.EditData.effdate_st)
      ) {
        this.$root.api.Master_s_RopeSave({
          data: {
            rope_id : this.EditData.rope_id,
            rope_cd: this.EditData.rope_cd.toUpperCase(),
            rope_name: this.EditData.rope_name,
            rope_cd_old: this.EditData.rope_cd_old.toUpperCase(),
            path_pic: this.EditData.path_pic,
            status: "A",
            itemcode: itemcode,
            effdate_st: this.EditData.effdate_st,
            effdate_en: this.EditData.effdate_en,
            user_id: localStorage.getItem("UserID"),
            user_name: localStorage.getItem("User"),
          },
          callback: (res) => {
            if (res.status == "S") {
              swal({
                title: "สำเร็จ",
                text: "บันทึกข้อมูลเชือกและฟางเสร็จสิ้น",
                icon: "success",
                button: "ตกลง",
              });
              this.SearchData();
              this.resetData();
              this.openEditModal = false;
            } else if (res.status == "F") {
              swal({
                title: "ผิดพลาด!",
                text: "ไม่สามารถบันทึกข้อมูลเชือกและฟางได้",
                icon: "error",
                button: "ตกลง",
              });
              this.resetData();
              this.openEditModal = false;
            }
          },
        });
      }
    },
    ViewButton() {
      this.ViewData.itemcode_name  = ""
      this.openViewModal = true;
      var x = this.checkedItem[0];
      this.$root.api.Master_s_RopeSearch({
        data: {
          rope_id: x,
        },
        callback: (res) => {
          this.$root.api.View_itemSearchItemCode({
            data: {
              ItemCode: res[0].itemcode,
            },
            callback: (res) => {
              this.ViewData.itemcode = res[0].itemCode + ' : ' + res[0].itemName;
            },
          });
          this.ViewData = res[0];
          this.ViewData.effdate_st = res[0].effdate_st.split("T")[0];
          this.ViewData.effdate_en = res[0].effdate_en.split("T")[0];


        },
      });
    },
    DeleteButton() {
      this.openDeleteModal = true;
      var x = this.checkedItem[0];
      this.$root.api.Master_s_RopeSearch({
        data: {
          rope_id: x,
        },
        callback: (res) => {
          this.DeleteData = res[0];
        },
      });
    },
    DeleteSaveButton() {
      this.$root.api.Master_s_RopeMove({
        data: {
          rope_move: [
            {
              rope_id: this.DeleteData.rope_id,
              user_id: localStorage.getItem("UserID"),
              user_name: localStorage.getItem("User"),
            },
          ],
        },
        callback: (res) => {
          if (res.status == "S") {
            swal({
              title: "สำเร็จ",
              text: "ลบข้อมูลเชือกและฟางเสร็จสิ้น",
              icon: "success",
              button: "ตกลง",
            });
            this.SearchData();
          } else {
            swal({
              title: "ผิดพลาด!",
              text: "ไม่สามารถลบข้อมูลเชือกและฟางได้",
              icon: "error",
              button: "ตกลง",
            });
            this.SearchData();
          }
          this.openDeleteModal = false;
          this.SearchData();
        },
      });
    },
    resetCheck() {
      this.checked = [];
      this.checkedItem = "";
    },
    resetData() {
      this.insertData.rope_cd = "";
      this.insertData.rope_name = "";
      this.insertData.rope_cd_old = "";
      this.insertData.effdate_st = this.nowDate;
      this.insertData.effdate_en = "2000-01-01";
      this.insertData.path_pic = "";
      this.ItemsCodeOptions.selected = null;

      document.getElementById("filename").value = "";
      document.getElementById("showImage").removeAttribute("src");
      document.getElementById("filename-edit").value = "";
    },
    setPagesActive() {
      this.pagesA = [];
      let numberOfPages = Math.ceil(this.dataActiveCount / this.perPageA);
      for (let index = 0; index < numberOfPages; index++) {
        this.pagesA.push(index);
      }
      this.pageA = 1;
    },
    setPagesInactive() {
      this.pagesI = [];
      let numberOfPages = Math.ceil(this.dataInactiveCount / this.perPageI);
      for (let index = 0; index < numberOfPages; index++) {
        this.pagesI.push(index);
      }
      this.pageI = 1;
    },
    setPagesCancel() {
      this.pagesC = [];
      let numberOfPages = Math.ceil(this.dataCStickerSpecCount / this.perPageI);
      for (let index = 0; index < numberOfPages; index++) {
        this.pagesC.push(index);
      }
      this.pageC = 1;
    },
    paginate(Active) {
      let pageA = this.pageA;
      let perPageA = this.perPageA;
      let from = pageA * perPageA - perPageA;
      let to = pageA * perPageA;
      return Active.slice(from, to);
    },
    paginateInactive(Inactive) {
      let pageI = this.pageI;
      let perPageI = this.perPageI;
      let from = pageI * perPageI - perPageI;
      let to = pageI * perPageI;
      return Inactive.slice(from, to);
    },
    paginateCancel(Cancel) {
      let pageC = this.pageC;
      let perPageC = this.perPageC;
      let from = pageC * perPageC - perPageC;
      let to = pageC * perPageC;
      return Cancel.slice(from, to);
    },
  },
  computed: {
    displayedPostsActive() {
      return this.paginate(this.dataActive);
    },
    displayedPostsInactive() {
      return this.paginateInactive(this.dataInactive);
    },
    displayedPostsCancel() {
      return this.paginateCancel(this.dataCancel);
    },
  },
  watch: {
    Active() {
      this.setPagesActive();
    },
    Inactive() {
      this.setPagesInactive();
    },
    Cancel() {
      this.setPagesCancel();
    },
  },
};
