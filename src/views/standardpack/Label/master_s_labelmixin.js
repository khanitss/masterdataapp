import Multiselect from "vue-multiselect";
// import Datepicker from 'vuejs-datepicker';
import {
  GetObjVal,
  SetObjVal,
  ObjCopy,
  ObjResetValue,
  ObjCopyValue,
  GetObjValJoin,
  GetObjArr,
  GetLastUpdate,
  GetDate,
  GetDateView,
  FileToBase64,
} from "@/shared/utils";
import setupmixin from "@/shared/setupmixin";
import DatePicker from "vue2-datepicker";
import "vue2-datepicker/index.css";
import swal from "sweetalert";

export default {
  name: "Label",
  data() {
    return {
      HReport: true,
      checkItemID: [],
      showTable: false,
      itemsTabs1: true,
      itemsTabs2: false,
      itemsTabs3: false,
      search: {
        labelID: "",
        labelDescrip: "",
      },
      dataLabel: {
        labelID: "",
        labelNameTH: "",
        labelNameEN: "",
        startUseDate: null,
        ExpireDate: null,
        sizeWidth: "",
        sizeLength: "",
        labelType: "",
        label10ID: "",
        labelImg: null,
      },
      insertLabel: {
        labelID: "",
        labelNameTH: "",
        labelNameEN: "",
        startUseDate: null,
        ExpireDate: null,
        sizeWidth: "",
        sizeLength: "",
        labelType: "",
        label10ID: "",
        labelImg: null,
        label_version: 0,
        label_remark: "",
      },
      editLabel: {
        ID: "",
        labelID: "",
        labelNameTH: "",
        labelNameEN: "",
        startUseDate: null,
        ExpireDate: null,
        sizeWidth: "",
        sizeLength: "",
        labelType: "",
        ItemCode: "",
        labelImg: null,
        label_version: "",
        label_remark: "",
        old_version: "",
      },
      viewLabel: {
        labelID: "",
        labelNameTH: "",
        labelNameEN: "",
        startUseDate: null,
        ExpireDate: null,
        sizeWidth: "",
        sizeLength: "",
        labelType: "",
        label10ID: "",
        labelImg: null,
        label_version: "",
        label_remark: "",
      },
      itemcode10: {
        lists: [],
        selected: null,
      },
      LabelGradeOptions: {
        lists: [],
        selected: null,
      },
      CustomerOptions: {
        lists: [],
        selected: null,
      },
      AccZone: {
        zone_code: "",
        zone_desc: "",
      },
      AccZoneLabelCode: {
        label_zone: "",
        acczonelabelcode_id: "",
      },
      LabelOwner: {},
      current_label_id: "",
      label: "",
      label_type_id: "",
      digit_1: "",
      digit_23: "00",
      digit_4: "",

      openAddModal: false,
      openEditModal: false,
      openViewModal: false,
      openDeleteModal: false,

      labelType: [],
      checked: [],
      defaultExDate: null,
      currentDate: null,

      Active: [],
      page: 1,
      perPage: 10,
      pages: [],
      ActiveLabel: [],
      Activedatalength: "",
      ActiveCount: "",

      Inactive: [],
      InactiveLabel: [],
      Inactivedatalength: "",
      InactiveCount: "",
      pageI: 1,
      perPageI: 5,
      pagesI: [],

      Cancel: [],
      CancelLabel: [],
      Canceldatalength: "",
      CancelCount: "",
      pageC: 1,
      perPageC: 5,
      pagesC: [],
      thisNowDate: "",
    };
  },
  components: {
    Multiselect,
    // Datepicker,
    DatePicker,
  },
  created() {
    this.GetDigit23();
  },
  mounted() {
    let nowDate = new Date();
    this.currentDate = GetDate(nowDate.setDate(nowDate.getDate()));
    this.thisNowDate = GetDate(nowDate.setDate(nowDate.getDate()));
    this.defaultExDate = "2000-01-01";

    this.$root.api.Master_s_LabelOwnerSearch({
      data: {},
      callback: (res) => {
        console.log("LabelOwner", res);
        this.LabelOwner = res;
      },
    });

    this.$root.api.View_itemSearch({
      data: {},
      callback: (res) => {
        res.forEach(
          (x) => (x._label = GetObjValJoin(x, ["itemCode", "itemName"]))
        );
        this.itemcode10.lists = res;
      },
    });

    this.$root.api.View_master_s_cutomerSearch({
      data: {},
      callback: (res) => {
        res.forEach((x) => (x._label = GetObjValJoin(x, ["cuscod", "fname"])));
        this.CustomerOptions.lists = res;
      },
    });

    this.$root.api.Master_s_LabelGradeCodeSearch({
      data: {},
      callback: (res) => {
        res.forEach(
          (x) =>
            (x._label = GetObjValJoin(x, ["labelgrade_cd", "labelgrade_name"]))
        );
        this.LabelGradeOptions.lists = res;
      },
    });
  },
  updated() {
    $(document).ready(function() {
      $("#MyTable").DataTable({
        initComplete: function() {
          this.api()
            .columns()
            .every(function() {
              var column = this;
              var select = $('<select><option value=""></option></select>')
                .appendTo($(column.footer()).empty())
                .on("change", function() {
                  var val = $.fn.dataTable.util.escapeRegex($(this).val());
                  //to select and search from grid
                  column.search(val ? "^" + val + "$" : "", true, false).draw();
                });

              column
                .data()
                .unique()
                .sort()
                .each(function(d, j) {
                  select.append('<option value="' + d + '">' + d + "</option>");
                });
            });
        },
      });
    });
    if (this.insertLabel.labelID == "") {
      $("#labelID").focus();
    }

    if (this.digit_1 != "" && this.digit_23 != "" && this.digit_4 != "") {
      this.insertLabel.labelID = this.digit_1 + this.digit_23 + this.digit_4;
    }
    if (this.insertLabel.labelID.length == 4) {
      this.searchDupLabelCD();
    }
  },
  methods: {
    openInsertModel() {
      this.openAddModal = true;
      this.GetDigit23();
      this.resetData();
    },
    GetDigit4() {
      if (this.label_type_id == 1) {
        this.digit_4 = 1;
      } else if (this.label_type_id == 2) {
        this.digit_4 = 2;
      }
    },
    GetDigit1() {
      this.insertLabel.labelID = "";
      if (this.label == "1") {
        this.digit_1 = "A";
        console.log(this.digit_1.length);
      } else {
        this.digit_1 = "";
      }
    },
    GetDigit23() {
      this.$root.api.View_master_s_labelSearchSearchLastID({
        data: {},
        callback: (res) => {
          var max_id = (res.length + 1).toString();
          var max_id_split = max_id.split("");
          // หารหัส 2 หลัก
          if (max_id_split.length <= 1) {
            this.digit_23 = "0" + max_id_split[0];
          } else if (max_id_split.length == 2) {
            this.digit_23 = res[0];
          } else if (max_id_split.length >= 3) {
            var last = max_id_split.length - 1;
            var last_n = max_id_split.length - 2;
            this.digit_23 = max_id_split[last_n] + "" + max_id_split[last];
          }
        },
      });
    },
    findAccZoneLabel() {
      if (this.CustomerOptions.selected != null) {
        console.log(this.CustomerOptions.selected.acczone);
        this.$root.api.Master_v_AcczoneSearch({
          data: { zone_code: this.CustomerOptions.selected.acczone },
          callback: (res) => {
            console.log("res zone", res);
            this.AccZone.zone_code = res[0].zonecode;
            this.AccZone.zone_desc = res[0].desC1.trim();
          },
        });
        this.$root.api.Master_s_AcczoneLabelCodeSearch({
          data: { zone_code: this.CustomerOptions.selected.acczone },
          callback: (res) => {
            console.log("res acc label", res);
            this.AccZoneLabelCode.label_zone = res[0].acczonelabelcode_cd.trim();
            this.AccZoneLabelCode.acczonelabelcode_id = res[0].acczonelabelcode_id.trim();
            this.digit_1 = this.AccZoneLabelCode.label_zone;
          },
        });
      }
    },
    resetData() {
      this.digit_1 = "";
      this.digit_23 = "";
      this.digit_4 = "";
      this.label = null;
      this.label_type_id = null;
      this.LabelGradeOptions.selected = null;
      this.CustomerOptions.selected = null;
      this.insertLabel.labelID = "";
      this.insertLabel.labelNameTH = "";
      this.insertLabel.labelNameEN = "";
      this.insertLabel.sizeWidth = "";
      this.insertLabel.sizeLength = "";
      this.insertLabel.labelType = "";
      this.insertLabel.labelImg = "";
      this.insertLabel.label_version = "";
      this.insertLabel.label_remark = "";
      this.itemcode10.selected = null;
      let nowDate = new Date();
      this.currentDate = GetDate(nowDate.setDate(nowDate.getDate()));
      this.thisNowDate = GetDate(nowDate.setDate(nowDate.getDate()));
      this.defaultExDate = "2000-01-01";
      document.getElementById("filename").value = "";
      document.getElementById("showImage").removeAttribute("src");
      document.getElementById("filename-edit").value = "";
    },
    SearchLabel() {
      this.ActiveCount = "";
      this.Activedatalength = "";

      this.InactiveCount = "";
      this.Inactivedatalength = "";

      this.Canceldatalength = "";
      this.CancelCount = "";

      this.checked = [];
      this.$root.api.View_master_s_labelSearch({
        data: {
          label_cd: this.search.labelID,
          txtSearch: this.search.labelDescrip,
        },
        callback: (res) => {
          this.ActiveLabel = res;
          for (let j = 0; j < this.ActiveLabel.length; j++) {
            if (this.ActiveLabel[j].status == "A") {
              this.ActiveCount = this.ActiveCount + 1;
            }
          }
          this.getActive();
          this.ActiveCount = this.ActiveCount.length;
          this.Activedatalength = this.ActiveCount;
          // // console.log(this.ActiveLabel);
          this.showTable = true;

          this.InactiveLabel = res;
          for (let i = 0; i < this.InactiveLabel.length; i++) {
            if (this.InactiveLabel[i].status == "I") {
              this.InactiveCount = this.InactiveCount + 1;
            }
          }
          this.getInactive();
          this.InactiveCount = this.InactiveCount.length;
          this.Inactivedatalength = this.InactiveCount;

          this.CancelLabel = res;
          for (let i = 0; i < this.CancelLabel.length; i++) {
            if (this.CancelLabel[i].status == "C") {
              this.CancelCount = this.CancelCount + 1;
            }
          }
          this.getCancel();
          this.CancelCount = this.CancelCount.length;
          this.Canceldatalength = this.CancelCount;
        },
      });
      this.Active = [];
      this.Inactive = [];
      this.Cancle = [];
    },
    showPreview() {
      document.getElementById("showImage").removeAttribute("src");
      var filename = document.getElementById("filename");
      filename.onchange = function() {
        var files = filename.files[0];
        var reader = new FileReader();
        reader.readAsDataURL(files);
        reader.onload = function() {
          var result = reader.result;
          document.getElementById("showImage").src = result;
        };
      };
    },
    showPreviewEdit() {
      var filename = document.getElementById("filename-edit");
      filename.onchange = function() {
        var files = filename.files[0];
        var reader = new FileReader();
        reader.readAsDataURL(files);
        reader.onload = function() {
          var result = reader.result;
          document.getElementById("showImage2").src = result;
        };
      };
    },
    SaveImage(event) {
      if (event.target.files.length) {
        let file = event.target.files[0];
        if (
          file &&
          (/^image[/]jpeg/.test(file.type) || /^image[/]png/.test(file.type))
        ) {
          FileToBase64(file).then((pic) => {
            this.insertLabel.labelImg = pic;
            return;
          });
        } else {
        }
      }
      this.file = null;
      this.insertLabel.labelImg = "";
    },
    checkItems() {
      this.checkItemID = $("input[type=checkbox]:checked")
        .map(function(_, el) {
          return $(el).val();
        })
        .get();
    },
    EditLableButton() {
      this.openEditModal = true;
      var x = this.checkItemID[0].split("-")[0];
      this.$root.api.View_master_s_labelSearchID({
        data: { txtSearch: x },
        callback: (res) => {
          this.$root.api.View_itemSearch({
            data: { txtSearch: res[0].itemcode },
            callback: (res) => {
              res.forEach(
                (x) => (x._label = GetObjValJoin(x, ["itemCode", "itemName"]))
              );
              this.itemcode10.selected = res[0];
              this.editLabel.label10ID =
                res[0].itemCode + " : " + res[0].itemName;
            },
          });
          this.editLabel.ID = res[0].label_id;
          this.editLabel.labelID = res[0].label_cd;
          this.editLabel.labelNameTH = res[0].label_name_th;
          this.editLabel.labelNameEN = res[0].label_name_en;
          this.editLabel.startUseDate = res[0].effdate_st.split("T")[0];
          this.editLabel.ExpireDate = res[0].effdate_en.split("T")[0];
          this.editLabel.sizeWidth = res[0].width;
          this.editLabel.sizeLength = res[0].length;
          this.editLabel.labelType = res[0].label_type;
          this.editLabel.labelImg = res[0].path_pic;
          this.editLabel.label_version = res[0].version;
          this.editLabel.old_version = res[0].version;
          this.editLabel.label_remark = res[0].remark;
        },
      });
    },
    ViewLableButton() {
      this.openViewModal = true;
      var x = this.checkItemID[0].split("-")[0];
      this.$root.api.View_master_s_labelSearchID({
        data: { txtSearch: x },
        callback: (res) => {
          this.viewLabel.labelID = res[0].label_cd;
          this.viewLabel.labelNameTH = res[0].label_name_th;
          this.viewLabel.labelNameEN = res[0].label_name_en;
          this.viewLabel.startUseDate = res[0].effdate_st.split("T")[0];
          this.viewLabel.ExpireDate = res[0].effdate_en.split("T")[0];
          this.viewLabel.sizeWidth = res[0].width;
          this.viewLabel.sizeLength = res[0].length;
          this.viewLabel.labelType = res[0].label_type;
          this.viewLabel.labelImg =
            "http://masterdatadev.kkfnets.com" + res[0].path_pic;
          (this.viewLabel.label_version = res[0].version),
            (this.viewLabel.label_remark = res[0].remark);
          console.log(res[0].remark);
        },
      });
    },
    SaveEditImage(event) {
      if (event.target.files.length) {
        let file = event.target.files[0];
        if (
          file &&
          (/^image[/]jpeg/.test(file.type) || /^image[/]png/.test(file.type))
        ) {
          FileToBase64(file).then((pic) => {
            this.editLabel.labelImg = pic;
            return;
          });
        } else {
        }
        this.file = null;
        this.editLabel.labelImg = null;
      }
    },
    SaveEditLableButton() {
      var selectitemcode = "";
      if (this.itemcode10.selected === null) {
        selectitemcode = this.editLabel.label10ID;
      } else {
        selectitemcode = this.itemcode10.selected.itemCode;
      }

      let nowDate = new Date();
      let x = GetDate(nowDate.setDate(nowDate.getDate()));
      if (
        (this.editLabel.ExpireDate > this.editLabel.startUseDate &&
          this.editLabel.ExpireDate > x) ||
        this.editLabel.ExpireDate == "2000-01-01"
      ) {
        this.$root.api.Master_s_lableEdit({
          data: {
            label_id: this.editLabel.ID,
            label_name_th: this.editLabel.labelNameTH,
            label_name_en: this.editLabel.labelNameEN,
            width: this.editLabel.sizeWidth,
            length: this.editLabel.sizeLength,
            label_type: this.editLabel.labelType,
            effdate_st: this.editLabel.startUseDate,
            effdate_en: this.editLabel.ExpireDate,
            status: "A",
            path_pic: this.editLabel.labelImg,
            user_id: localStorage.getItem("UserID"),
            user_name: localStorage.getItem("User"),
            itemcode: selectitemcode,
            version: this.editLabel.label_version,
            remark: this.editLabel.label_remark,
          },
          callback: (res) => {
            swal("สำเร็จ!", "บันทึกข้อมูลเสร็จสิ้น", "success");
            this.SearchLabel();
            this.openEditModal = false;
          },
        });
        document.getElementById("filename-edit").value = "";
        this.checked = [];
      } else {
        swal(
          "ข้อมูลไม่ถูกต้อง",
          "วันหมดอายุจะต้องมากกว่าวันที่ประกาศใช้และวันปัจจุบัน",
          "warning"
        );
      }
    },
    DeleteLableButton() {
      // this.openDeleteModal = true;
      var deleteID = "";
      var deleteCD = "";
      var DeleteArray = [];
      for (let i = 0; i < this.checkItemID.length; i++) {
        deleteID = this.checkItemID[i].split("-")[0];
        deleteCD = this.checkItemID[i].split("-")[1];
        this.$root.api.View_master_u_labelSearchLabelCd({
          data: {
            txtSearch: deleteCD,
          },
          callback: (res) => {
            for (let i = 0; i < res.length; i++) {
              DeleteArray.push(res[i].labelcd);
              // // console.log(DeleteArray);
            }
            if (DeleteArray.length > 0) {
              swal(
                "ไม่สามารถลบรายการได้",
                "มีการนำรหัสป้ายไปใช้แล้วจึงไม่สามารถลบรายการได้",
                "error"
              );
              this.checked = [];

              this.openDeleteModal = false;
            } else if (DeleteArray.length == 0) {
              this.$root.api.Master_s_lableMove({
                data: {
                  label_move: [
                    {
                      label_id: deleteID,
                      label_cd: deleteCD,
                      user_id: localStorage.getItem("UserID"),
                      user_name: localStorage.getItem("User"),
                    },
                  ],
                },
                callback: (res) => {
                  swal("สำเร็จ!", "ลบรายการเสร็จสิ้น", "success");
                  this.openDeleteModal = false;
                  this.checked = [];
                  this.SearchLabel();
                },
              });
            }
          },
        });
      }
    },
    sortTable(n) {
      var table,
        rows,
        switching,
        i,
        x,
        y,
        shouldSwitch,
        dir,
        switchcount = 0;
      table = document.getElementById("table");
      switching = true;
      dir = "asc";
      while (switching) {
        switching = false;
        rows = table.getElementsByTagName("TR");
        for (i = 1; i < rows.length - 1; i++) {
          shouldSwitch = false;
          x = rows[i].getElementsByTagName("TD")[n];
          y = rows[i + 1].getElementsByTagName("TD")[n];
          if (dir == "asc") {
            if (x.innerHTML.toLowerCase() > y.innerHTML.toLowerCase()) {
              shouldSwitch = true;
              break;
            }
          } else if (dir == "desc") {
            if (x.innerHTML.toLowerCase() < y.innerHTML.toLowerCase()) {
              shouldSwitch = true;
              break;
            }
          }
        }
        if (shouldSwitch) {
          rows[i].parentNode.insertBefore(rows[i + 1], rows[i]);
          switching = true;
          switchcount++;
        } else {
          if (switchcount == 0 && dir == "asc") {
            dir = "desc";
            switching = true;
          }
        }
      }
    },
    CheckDate() {
      let nowDate = new Date(this.insertLabel.labelID);
      // let x = GetDate(nowDate.setDate(nowDate.getDate()));
      // console.log(this.thisNowDate);
      // console.log((this.defaultExDate > this.currentDate && this.defaultExDate > this.thisNowDate) ||
      // this.defaultExDate == "2000-01-01");

      if (this.insertLabel.labelID.length == 4) {
        if (
          this.insertLabel.labelID != "" &&
          this.insertLabel.labelNameTH != "" &&
          this.insertLabel.labelNameEN != "" &&
          this.insertLabel.sizeWidth != "" &&
          this.insertLabel.sizeLength != "" &&
          this.currentDate != null &&
          this.insertLabel.label_version != ""
        ) {
          if (
            (this.defaultExDate > this.currentDate &&
              this.defaultExDate > this.thisNowDate) ||
            this.defaultExDate == "2000-01-01"
          ) {
            this.$root.api.View_master_s_labelSearch({
              data: { txtSearch: this.insertLabel.labelID },
              callback: (res) => {
                if (res.length != 0) {
                  swal(
                    "รหัสป้ายซ้ำ",
                    "กรุณากรอกรหัสป้ายอื่นหรือเปลี่ยน version ",
                    "warning"
                  );
                } else {
                  this.InsertLabelButton();
                }
              },
            });
          } else {
            swal(
              "ข้อมูลไม่ถูกต้อง",
              "วันหมดอายุจะต้องมากกว่าวันที่ประกาศใช้และวันปัจจุบัน",
              "warning"
            );
          }
        } else {
          swal(
            "ไม่สามารถบันทึกข้อมูลได้",
            "กรุณากรอกข้อมูลให้ครบถ้วน",
            "warning"
          );
        }
      } else if (this.insertLabel.labelID.length != 4) {
        swal(
          "รหัสป้ายต้องมี 4 หลัก",
          "กรุณากรอกรหัสป้ายใหม่ให้ถูกต้อง",
          "warning"
        );
      }
    },
    InsertLabelButton() {
      var labelgrade_id = "";
      var labelgrade_cd = "";
      var cus_cod = "";
      var zone_code = "";
      var acczonelabelcode_id = "";
      var acczonelabelcode_cd = "";
      if (this.label == 1) {
        labelgrade_id = this.LabelGradeOptions.selected.labelgrade_id;
        labelgrade_cd = this.LabelGradeOptions.selected.labelgrade_cd;

        cus_cod = "";
        zone_code = "";
        acczonelabelcode_id = 0;
        acczonelabelcode_cd = "";
      } else if (this.label == 2) {
        labelgrade_id = 0;
        labelgrade_cd = 0;

        cus_cod = this.CustomerOptions.selected.cuscod;
        zone_code = this.AccZone.zone_code;
        acczonelabelcode_id = this.AccZoneLabelCode.acczonelabelcode_id;
        acczonelabelcode_cd = this.AccZoneLabelCode.label_zone;
      }

      var labelpapertype_id = "";
      var labelpapertype_cd = "";
      if (this.label_type_id == 1) {
        labelpapertype_id = 1;
        labelpapertype_cd = 1;
      } else if (this.label_type_id == 2) {
        labelpapertype_id = 2;
        labelpapertype_cd = 2;
      }

      var selectitemcode = "";
      if (this.itemcode10.selected === null) {
        selectitemcode = "";
      } else {
        selectitemcode = this.itemcode10.selected.itemCode;
      }
      this.$root.api.Master_s_lableSave({
        data: {
          label_cd: this.insertLabel.labelID.toUpperCase(),
          label_name_th: this.insertLabel.labelNameTH,
          label_name_en: this.insertLabel.labelNameEN,
          width: this.insertLabel.sizeWidth,
          length: this.insertLabel.sizeLength,
          label_type: this.insertLabel.labelType,
          effdate_st: this.currentDate,
          effdate_en: this.defaultExDate,
          status: "A",
          path_pic: this.insertLabel.labelImg,
          user_id: localStorage.getItem("UserID"),
          user_name: localStorage.getItem("User"),
          itemcode: selectitemcode,
          version: this.insertLabel.label_version,
          remark: this.insertLabel.label_remark,
          labelowner_id: this.label,
          labelgrade_id: labelgrade_id,
          labelgrade_cd: labelgrade_cd,
          cus_cod: cus_cod,
          zone_code: zone_code,
          acczonelabelcode_id: acczonelabelcode_id,
          acczonelabelcode_cd: acczonelabelcode_cd,
          labelpapertype_id: labelpapertype_id,
          labelpapertype_cd: labelpapertype_cd,
          digit1: this.digit_1,
          digit23: this.digit_23,
          digit4: this.digit_4,
        },
        callback: (res) => {
          if (res._status == "S") {
            swal("สำเร็จ!", "บันทึกข้อมูลเสร็จสิ้น", "success");
            this.SearchLabel();
            this.openAddModal = false;
            this.resetData();
          } else if (res._status == "F") {
            swal("ไม่สำเร็จ", "ไม่สามารถบันทึกข้อมูลได้", "error");
            this.SearchLabel();
            this.openAddModal = false;
            this.resetData();
          }
        },
      });
    },
    // pagination //
    getActive() {
      this.Active = [];
      for (let i = 0; i < this.ActiveLabel.length; i++) {
        if (this.ActiveLabel[i].status == "A") {
            var labelowner_id_name = ""
            if (this.ActiveLabel[i].labelowner_id == 1) {
                labelowner_id_name = "ป้าย KKF"
            } else if (this.ActiveLabel[i].labelowner_id == 2)
            {
                labelowner_id_name = "ป้าย OEM"
            }
          this.Active.push({
            label_id: this.ActiveLabel[i].label_id,
            label_cd: this.ActiveLabel[i].label_cd,
            label_name_th: this.ActiveLabel[i].label_name_th,
            label_name_en: this.ActiveLabel[i].label_name_en,
            label_cd_old: this.ActiveLabel[i].label_cd_old,
            width: this.ActiveLabel[i].width,
            length: this.ActiveLabel[i].length,
            labelowner_id: labelowner_id_name,
            effdate_st: this.ActiveLabel[i].effdate_st,
            effdate_en: this.ActiveLabel[i].effdate_en,
            path_pic: this.ActiveLabel[i].path_pic,
            status: this.ActiveLabel[i].status,
            // ! 27/10/2020 add field version and remark
            label_version: this.ActiveLabel[i].version,
            label_remark: this.ActiveLabel[i].remark,
          });
        }
      }
    },
    getInactive() {
      this.Inactive = [];
      for (let i = 0; i < this.InactiveLabel.length; i++) {
        if (this.InactiveLabel[i].status == "I") {
            var labelowner_id_name = ""
            if (this.InactiveLabel[i].labelowner_id == 1) {
                labelowner_id_name = "ป้าย KKF"
            } else if (this.InactiveLabel[i].labelowner_id == 2)
            {
                labelowner_id_name = "ป้าย OEM"
            }
          this.Inactive.push({
            label_id: this.InactiveLabel[i].label_id,
            label_cd: this.InactiveLabel[i].label_cd,
            label_name_th: this.InactiveLabel[i].label_name_th,
            label_name_en: this.InactiveLabel[i].label_name_en,
            label_cd_old: this.InactiveLabel[i].label_cd_old,
            width: this.InactiveLabel[i].width,
            length: this.InactiveLabel[i].length,
            labelowner_id: labelowner_id_name,
            effdate_st: this.InactiveLabel[i].effdate_st,
            effdate_en: this.InactiveLabel[i].effdate_en,
            path_pic: this.InactiveLabel[i].path_pic,
            status: this.InactiveLabel[i].status,
            // ! 27/10/2020 add field version and remark
            label_version: this.InactiveLabel[i].version,
            label_remark: this.InactiveLabel[i].remark,
          });
        }
      }
    },
    getCancel() {
      this.Cancel = [];
      for (let i = 0; i < this.CancelLabel.length; i++) {
        if (this.CancelLabel[i].status == "C") {
            var labelowner_id_name = ""
            if (this.CancelLabel[i].labelowner_id == 1) {
                labelowner_id_name = "ป้าย KKF"
            } else if (this.CancelLabel[i].labelowner_id == 2)
            {
                labelowner_id_name = "ป้าย OEM"
            }
          this.Cancel.push({
            label_id: this.CancelLabel[i].label_id,
            label_cd: this.CancelLabel[i].label_cd,
            label_name_th: this.CancelLabel[i].label_name_th,
            label_name_en: this.CancelLabel[i].label_name_en,
            label_cd_old: this.CancelLabel[i].label_cd_old,
            width: this.CancelLabel[i].width,
            length: this.CancelLabel[i].length,
            labelowner_id: labelowner_id_name,
            effdate_st: this.CancelLabel[i].effdate_st,
            effdate_en: this.CancelLabel[i].effdate_en,
            path_pic: this.CancelLabel[i].path_pic,
            status: this.CancelLabel[i].status,
            // ! 27/10/2020 add field version and remark
            label_version: this.CancelLabel[i].version,
            label_remark: this.CancelLabel[i].remark,
          });
        }
      }
    },
    setPagesActive() {
      this.pages = [];
      let numberOfPages = Math.ceil(this.Activedatalength / this.perPage);
      for (let index = 0; index < numberOfPages; index++) {
        this.pages.push(index);
      }
      this.page = 1;
    },
    setPagesIntive() {
      this.pagesI = [];
      let numberOfPages = Math.ceil(this.Inactivedatalength / this.perPageI);
      for (let index = 0; index < numberOfPages; index++) {
        this.pagesI.push(index);
      }
      this.pageI = 1;
    },
    setPagesCancel() {
      this.pagesC = [];
      let numberOfPages = Math.ceil(this.Canceldatalength / this.perPageC);
      for (let index = 0; index < numberOfPages; index++) {
        this.pagesC.push(index);
      }
      this.pageC = 1;
    },
    paginate(Active) {
      let page = this.page;
      let perPage = this.perPage;
      let from = page * perPage - perPage;
      let to = page * perPage;
      return Active.slice(from, to);
    },
    paginateInactive(Inactive) {
      let page = this.pageI;
      let perPage = this.perPageI;
      let from = page * perPage - perPage;
      let to = page * perPage;
      return Inactive.slice(from, to);
    },
    paginateCancel(Cancel) {
      let page = this.pageC;
      let perPage = this.perPageC;
      let from = page * perPage - perPage;
      let to = page * perPage;
      return Cancel.slice(from, to);
    },
    searchDupLabelCD() {
      console.log("searchDupLabelCD()");
      if (this.insertLabel.labelID.length != 4) {
        swal(
          "รหัสป้ายต้องมี 4 หลัก",
          "กรุณากรอกรหัสป้ายใหม่ให้ถูกต้อง",
          "warning"
        );
      } else if (this.insertLabel.labelID.length == 4) {
        var Dupvalue = [];
        this.$root.api.View_master_s_labelSearch({
          data: { txtSearch: this.insertLabel.labelID },
          callback: (res) => {
            if (res.length != 0) {
              // ! check labelcd and version
              for (let i = 0; i < res.length; i++) {
                if (res[i].version == this.insertLabel.label_version) {
                  Dupvalue.push("true");
                }
              }
              var CheckDupvalue = Dupvalue.includes("true");
              if (CheckDupvalue == true) {
                swal(
                  "รหัสป้ายซ้ำ",
                  "กรุณากรอกรหัสป้ายอื่นหรือเปลี่ยน version ",
                  "warning"
                );
                this.insertLabel.label_version = "";
              }
            }
          },
        });
      }
    },
    checkDupEdit() {
      var Dupvalue = [];
      this.$root.api.View_master_s_labelSearch({
        data: { txtSearch: this.editLabel.labelID },
        callback: (res) => {
          if (res.length != 0) {
            // ! check labelcd and version
            for (let i = 0; i < res.length; i++) {
              if (res[i].version == this.editLabel.label_version) {
                if (
                  this.editLabel.label_version != this.editLabel.old_version
                ) {
                  Dupvalue.push("true");
                }
              }
            }
            var CheckDupvalue = Dupvalue.includes("true");
            if (CheckDupvalue == true) {
              swal(
                "รหัสป้ายซ้ำ",
                "กรุณากรอกรหัสป้ายอื่นหรือเปลี่ยน version ",
                "warning"
              );
              this.editLabel.label_version = this.editLabel.old_version;
            }
          }
        },
      });
    },
  },
  computed: {
    displayedPostsActive() {
      return this.paginate(this.Active);
    },
    displayedPostsInactive() {
      return this.paginateInactive(this.Inactive);
    },
    displayedPostsCancel() {
      return this.paginateCancel(this.Cancel);
    },
  },
  watch: {
    Active() {
      this.setPagesActive();
    },
    Inactive() {
      this.setPagesIntive();
    },
    Cancel() {
      this.setPagesCancel();
    },
  },
};
