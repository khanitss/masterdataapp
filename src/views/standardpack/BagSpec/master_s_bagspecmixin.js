import Multiselect from "vue-multiselect";
import {
  GetObjVal,
  SetObjVal,
  ObjCopy,
  ObjResetValue,
  ObjCopyValue,
  GetObjValJoin,
  GetObjArr,
  GetLastUpdate,
  GetDate,
  GetDateView,
  FileToBase64,
} from "@/shared/utils";
import swal from "sweetalert";
import DatePicker from "vue2-datepicker";
import "vue2-datepicker/index.css";

export default {
  components: {
    Multiselect,
    DatePicker,
  },
  data() {
    return {
      HReport: true,
      search: {
        txtSearch: "",
        codeSearch: "",
      },
      SaleOptions: [
        { value: "0", name: "ขายในประเทศ" },
        { value: "1", name: "ขายต่างประเทศ" },
      ],
      SaleValue: {},

      CustomerOptions: {
        lists: [],
        selected: null,
      },
      ProductTypeOptions: {
        lists: [],
        selected: null,
      },
      StretchingTypeOptions: {
        lists: [],
        selected: null,
      },
      LabelOptions: {
        lists: [],
        selected: null,
      },
      BagOptions: {
        lists: [],
        selected: null,
      },

      DataTable: [],
      DataSubTable: [],

      // Active
      dataActive: [],
      dataActiveCount: 0,
      pageA: 1,
      perPageA: 10,
      pagesA: [],

      // InActive
      dataInactive: [],
      dataInactiveCount: 0,
      pageI: 1,
      perPageI: 5,
      pagesI: [],

      // Cancel
      dataCancel: [],
      dataCancelCount: 0,
      pageC: 1,
      perPageC: 5,
      pagesC: [],

      checked: [],
      checkedItem: "",
      checkedSpec: null,

      itemsTabs1: true,
      itemsTabs2: false,
      itemsTabs3: false,

      openAddModal: false,
      openEditModal: false,
      openViewModal: false,
      openDeleteModal: false,
      insertData: {
        effdate_st: "",
        effdate_en: "",
      },
      EditData: {
        tsale: "",
        tsaleType: "",
        // cus_cod : "",
        // cus_name : "",
        // produccttype_code : "",
        // producttype_desc : "",
        // label_id : "",
        // bag_id : "",
        // bag_cd : "",
        // bag_name : "",
        // stretching_type :"",
        // stretching_desc : "",
        
        customer: "",
        productype: "",
        stretchingType: "",
        label_cd: "",
        label_name: "",
        bag_cd: "",
        bag_name: "",
        effdate_st: "",
        effdate_en: "",
        path_pic: "",
        producttype_code: "",
      },
      ViewData: {
        tsale: "",
        customer: "",
        productype: "",
        stretchingType: "",
        label_cd: "",
        label_name: "",
        bag_cd: "",
        bag_name: "",
        effdate_st: "",
        effdate_en: "",
        path_pic: "",
        producttype_code: "",
      },
      DeleteData: {},
      nowDate: "",
      old_effdate_st: "",

      producttype_net: [
        "+",
        "0",
        "1",
        "2",
        "3",
        "4",
        "5",
        "6",
        "A",
        "B",
        "C",
        "D",
        "E",
        "H",
        "I",
        "K",
        "L",
        "M",
        "N",
        "P",
        "Q",
        "S",
        "T",
        "U",
      ],
      producttype_cord: [
        "/",
        "7",
        "8",
        "9",
        "F",
        "G",
        "J",
        "O",
        "R",
        "V",
        "W",
        "X",
        "Y",
        "Z",
      ],
    };
  },
  updated() {},
  created() {
    let nowDate = new Date();
    this.nowDate = GetDate(nowDate.setDate(nowDate.getDate()));
    this.insertData.effdate_st = this.nowDate;
    this.insertData.effdate_en = "2000-01-01";

    this.$root.api.View_master_s_producttypeSearch({
      data: {},
      callback: (res) => {
        res.forEach(
          (x) => (x._label = GetObjValJoin(x, ["producttypecode", "desc1"]))
        );
        this.ProductTypeOptions.lists = res;
      },
    });

    this.$root.api.View_master_s_stretchingtypeSearch({
      data: {},
      callback: (res) => {
        res.forEach(
          (x) =>
            (x._label = GetObjValJoin(x, [
              "stretching_type",
              "stretching_desc",
            ]))
        );
        this.StretchingTypeOptions.lists = res;
      },
    });

    this.$root.api.View_master_s_labelSearchActive({
      data: {},
      callback: (res) => {
        res.forEach((x) => (x._label = GetObjValJoin(x, ["label_cd"])));
        this.LabelOptions.lists = res;
      },
    });
    this.$root.api.Master_s_BagSearchActive({
      data: {},
      callback: (res) => {
        res.forEach((x) => (x._label = GetObjValJoin(x, ["bag_cd"])));
        this.BagOptions.lists = res;
      },
    });
  },
  methods: {
    SearchData() {
      this.DataTable = [];
      this.DataCancelTable = [];
      this.dataActiveCount = 0;
      this.dataInactiveCount = 0;
      this.dataCancelCount = 0;
      this.checked = [];
      this.checkedItem = "";
      this.checkedSpec = [];
      this.DataSubTable = []

      this.$root.api.Master_s_BagSpecSearchCus({
        data: {
          codeSearch: this.search.codeSearch,
          txtSearch: this.search.txtSearch,
        },
        callback: (res) => {
          this.DataTable = res;
          for (let i = 0; i < res.length; i++) {
            if (res[i].status == "A") {
              this.dataActiveCount = this.dataActiveCount + 1;
            } else if (res[i].status == "I") {
              this.dataInactiveCount = this.dataInactiveCount + 1;
            } else if (res[i].status == "C") {
              this.dataCancelCount = this.dataCancelCount + 1;
            }
          }
          if (res.length == 0) {
            swal(
              "ไม่พบเงื่อนไขการถุงบรรจุสินค้าที่ค้นหา",
              "กรุณาใส่รหัสลูกค้าหรือรหัสถุงบรรจุสินค้าใหม่",
              "warning"
            );
            this.search = [];
          }
          this.getDataTable();
          this.setPagesActive();
          this.setPagesInactive();
          this.setPagesCancel();
        },
      });
    },
    getDataTable() {
      this.dataActive = [];
      this.dataInactive = [];
      this.dataCancel = [];
      for (let i = 0; i < this.DataTable.length; i++) {
        if (this.DataTable[i].status == "A") {
          this.dataActive.push({
            cus_cod: this.DataTable[i].cus_cod,
            cus_id: this.DataTable[i].cus_id,
            tsale: this.DataTable[i].tsale,
            cus_name: this.DataTable[i].cus_name,
            status: this.DataTable[i].status
          });
        } else if (this.DataTable[i].status == "I") {
          this.dataInactive.push({
            cus_cod: this.DataTable[i].cus_cod,
            cus_id: this.DataTable[i].cus_id,
            tsale: this.DataTable[i].tsale,
            cus_name: this.DataTable[i].cus_name,
            status: this.DataTable[i].status
          });
        } else if (this.DataTable[i].status == "C") {
          this.dataCancel.push({
            cus_cod: this.DataTable[i].cus_cod,
            cus_id: this.DataTable[i].cus_id,
            tsale: this.DataTable[i].tsale,
            cus_name: this.DataTable[i].cus_name,
            status: this.DataTable[i].status
          });
        }
      }
    },
    FindByCuscod(cuscod_id) {
      this.checkedSpec = [];
      this.$root.api.Master_s_BagSpecSearch({
        data: {
          cuscod: cuscod_id,
        },
        callback: (res) => {
          console.log(res);
          this.DataSubTable = res;
        },
      });
    },
    FindbyBagSpecID(val) {
      this.checkedItem = "";
      if ($("input.checkbox_check").is(":checked")) {
        this.checkedItem = $("input[type=checkbox][name=Choosed]:checked")
          .map(function(_, el) {
            return $(el).val();
          })
          .get();
      }
      // console.log(this.checkedItem);
    },
    InsertSaveButton() {
      var stretching_type = "";
      var stretching_desc = "";
      if (this.StretchingTypeOptions.selected == null) {
        stretching_type = "";
        stretching_desc = "";
      } else {
        stretching_type = this.StretchingTypeOptions.selected.stretching_type;
        stretching_desc = this.StretchingTypeOptions.selected.stretching_desc;
      }

      if (
        this.SaleValue != null &&
        this.CustomerOptions.selected != null &&
        this.ProductTypeOptions.selected != null &&
        this.LabelOptions.selected != null &&
        this.BagOptions.selected != null
      ) {
        if (
          this.insertData.effdate_en == "2000-01-01" ||
          (this.insertData.effdate_en > this.nowDate &&
            this.insertData.effdate_en > this.insertData.effdate_st)
        ) {
          this.$root.api.Master_s_BagSpecSave({
            data: {
              tsale: this.SaleValue.value,
              cus_cod: this.CustomerOptions.selected.cuscod,
              cus_name: this.CustomerOptions.selected.fname,
              produccttype_code: this.ProductTypeOptions.selected
                .producttypecode,
              producttype_desc: this.ProductTypeOptions.selected.desc1,
              stretchingtype_code: stretching_type,
              stretchingtype_desc: stretching_desc,
              label_id: this.LabelOptions.selected.label_id,
              label_cd: this.LabelOptions.selected.label_cd,
              label_name: this.LabelOptions.selected.label_name_th,
              bag_id: this.BagOptions.selected.bag_id,
              bag_cd: this.BagOptions.selected.bag_cd,
              bag_name: this.BagOptions.selected.bag_name_th,
              status: "A",
              effdate_st: this.insertData.effdate_st,
              effdate_en: this.insertData.effdate_en,
              user_id: localStorage.getItem("UserID"),
              user_name: localStorage.getItem("User"),
            },
            callback: (res) => {
              console.log(res);
              if (res.status == "S") {
                swal({
                  title: "สำเร็จ",
                  text:
                    "บันทึกข้อมูลข้อมูลมาตราฐานเงื่อนไขถุงบรรจุสินค้าเสร็จสิ้น",
                  icon: "success",
                  button: "ตกลง",
                });
                this.SearchData();
                this.resetData();
                this.openAddModal = false;
              } else if (res.status == "F") {
                swal({
                  title: "ผิดพลาด!",
                  text:
                    "ไม่สามารถบันทึกข้อมูลข้อมูลมาตราฐานเงื่อนไขถุงบรรจุสินค้าได้",
                  icon: "error",
                  button: "ตกลง",
                });
                this.resetData();
                this.openAddModal = false;
              }
            },
          });
        } else {
          swal({
            title: "ข้อมูลไม่ถูกต้อง!",
            text: "วันหมดอายุจะต้องมากกว่าวันที่ประกาศใช้และวันปัจจุบัน",
            icon: "error",
            button: "ตกลง",
          });
        }
      } else {
        swal({
          title: "ผิดพลาด",
          text:
            "กรุณากรอกข้อมูลมาตราฐานเงื่อนไข \n การใช้ถุงบรรจุสินค้าให้ครบถ้วน",
          icon: "error",
          button: "ตกลง",
        });
      }
    },
    EditButton() {
      this.openEditModal = true;
      var x = this.checkedItem[0];
      console.log(x);
      this.$root.api.Master_s_BagSpecSearch({
        data: {
          bagspec_id: x,
        },
        callback: (res) => {
          this.EditData = res[0]
          this.EditData.productype =
            res[0].produccttype_code.trim() +
            " : " +
            res[0].producttype_desc.trim();
          // this.EditData.producttype_code = res[0].produccttype_code;
          this.EditData.label_cd = res[0].label_cd;
          this.EditData.label_name = res[0].label_name;
          this.EditData.bag_cd = res[0].bag_cd;
          this.EditData.bag_name = res[0].bag_name;
          this.EditData.effdate_st = res[0].effdate_st.split("T")[0];
          this.EditData.effdate_en = res[0].effdate_en.split("T")[0];
          this.EditData.stretchingType =
            res[0].stretchingtype_code.trim() +
            " : " +
            res[0].stretchingtype_desc.trim();

          this.EditData.customer =
            res[0].cus_cod.trim() + " : " + res[0].cus_name.trim();

          if (res[0].tsale == 1) {
            this.SaleValue = { value: "1", name: "ขายต่างประเทศ" };
            this.EditData.tsaleType = "ขายต่างประเทศ";
          } else if (res[0].tsale == 0) {
            this.SaleValue = { value: "0", name: "ขายในประเทศ" };
            this.EditData.tsaleType = "ขายในประเทศ";
          }

          this.$root.api.View_master_s_cutomerSearch({
            data: { tsale : res[0].tsale },
            callback: (res) => {
              res.forEach(
                (x) => (x._label = GetObjValJoin(x, ["cuscod", "fname"]))
              );
              this.CustomerOptions.lists = res;
            },
          });

          this.$root.api.View_master_s_cutomerSearch({
            data: { cuscod: res[0].cus_cod },
            callback: (res) => {
              res.forEach(
                (x) => (x._label = GetObjValJoin(x, ["cuscod", "fname"]))
              );
              this.CustomerOptions.selected = res[0];
            },
          });

          this.$root.api.View_master_s_producttypeSearch({
            data: { producttype_code: res[0].produccttype_code },
            callback: (res) => {
              res.forEach(
                (x) =>
                  (x._label = GetObjValJoin(x, ["producttypecode", "desc1"]))
              );
              this.ProductTypeOptions.selected = res[0];
            },
          });

          if (res[0].stretchingtype_code.trim() == "") {
            this.StretchingTypeOptions.selected = null;
          } else {
            this.$root.api.View_master_s_stretchingtypeSearch({
              data: { stretchingtype_code: res[0].stretchingtype_code },
              callback: (res) => {
                res.forEach(
                  (x) =>
                    (x._label = GetObjValJoin(x, [
                      "stretching_type",
                      "stretching_desc",
                    ]))
                );
                this.StretchingTypeOptions.selected = res[0];
              },
            });
          }

          this.$root.api.View_master_s_labelSearchActive({
            data: { label_id: res[0].label_id },
            callback: (res) => {
              res.forEach((x) => (x._label = GetObjValJoin(x, ["label_cd"])));
              this.LabelOptions.selected = res[0];
            },
          });
          this.$root.api.Master_s_BagSearch({
            data: { bag_id: res[0].bag_id },
            callback: (res) => {
              res.forEach((x) => (x._label = GetObjValJoin(x, ["bag_cd"])));
              this.BagOptions.selected = res[0];
              this.EditData.path_pic = res[0].path_pic;
            },
          });
        },
      });
    },
    EditSaveButton() {
      console.log(this.EditData);
      var tsale = ""
      if (this.SaleValue == null)
      {
        tsale = this.EditData.tsale
      } else {
        tsale = this.SaleValue.value
      }
      console.log(this.EditData.cus_cod);
      var cuscod = ""
      var cusname = ""
      if (this.CustomerOptions.selected == null) {
        cuscod = this.EditData.cus_cod
        cusname = this.EditData.cus_name
      } else {
        cuscod = this.CustomerOptions.selected.cuscod
        cusname = this.CustomerOptions.selected.fname
      }

      var produccttype_code = ""
      var producttype_desc = ""
      if (this.ProductTypeOptions.selected == null) {
        produccttype_code = this.EditData.produccttype_code.trim()
        producttype_desc = this.EditData.producttype_desc.trim()
      } else {
        produccttype_code = this.ProductTypeOptions.selected.producttypecode
        producttype_desc = this.ProductTypeOptions.selected.desc1
      }

      var label_id = ""
      var label_cd = ""
      var label_name = ""
      if (this.LabelOptions.selected == null) {
        label_id = this.EditData.label_id
        label_cd = this.EditData.label_cd
        label_name = this.EditData.label_name
      } else {
        label_id = this.LabelOptions.selected.label_id
        label_cd = this.LabelOptions.selected.label_cd
        label_name = this.LabelOptions.selected.label_name_th
      }

      var bag_id = ""
      var bag_cd = ""
      var bag_name = ""
      if (this.BagOptions.selected == null) {
        bag_id = this.EditData.bag_id
        bag_cd = this.EditData.bag_cd
        bag_name = this.EditData.bag_name_th
      } else {
        bag_id = this.BagOptions.selected.bag_id
        bag_cd = this.BagOptions.selected.bag_cd
        bag_name = this.BagOptions.selected.bag_name_th
      }

      var stretching_type = "";
      var stretching_desc = "";
      if (this.StretchingTypeOptions.selected == null) {
        stretching_type = this.EditData.stretchingtype_code;
        stretching_desc = this.EditData.stretchingtype_desc;
      } else {
        stretching_type = this.StretchingTypeOptions.selected.stretching_type;
        stretching_desc = this.StretchingTypeOptions.selected.stretching_desc;
      }

      if (
        this.EditData.effdate_en == "2000-01-01" ||
        (this.EditData.effdate_en > this.nowDate &&
          this.EditData.effdate_en > this.EditData.effdate_st)
      ) {
        this.$root.api.Master_s_BagSpecSave({
          data: {
            bagspec_id : this.EditData.bagspec_id,
            tsale: tsale,
            cus_cod: cuscod,
            cus_name: cusname,
            produccttype_code: produccttype_code,
            producttype_desc: producttype_desc,
            stretchingtype_code: stretching_type,
            stretchingtype_desc: stretching_desc,
            label_id: label_id,
            label_cd: label_cd,
            label_name: label_name,
            bag_id: bag_id,
            bag_cd: bag_cd,
            bag_name:bag_name,
            status: "A",
            effdate_st: this.EditData.effdate_st,
            effdate_en: this.EditData.effdate_en,
            user_id: localStorage.getItem("UserID"),
            user_name: localStorage.getItem("User"),
          },
          callback: (res) => {
            console.log(res);
            if (res.status == "S") {
              swal({
                title: "สำเร็จ",
                text:
                  "บันทึกข้อมูลข้อมูลมาตราฐานเงื่อนไขถุงบรรจุสินค้าเสร็จสิ้น",
                icon: "success",
                button: "ตกลง",
              });
              this.SearchData();
              this.resetData();
              this.openEditModal = false;
            } else if (res.status == "F") {
              swal({
                title: "ผิดพลาด!",
                text:
                  "ไม่สามารถบันทึกข้อมูลข้อมูลมาตราฐานเงื่อนไขถุงบรรจุสินค้าได้",
                icon: "error",
                button: "ตกลง",
              });
              this.resetData();
              this.openEditModal = false;
            }
          },
        });
      } else {
        swal({
          title: "ข้อมูลไม่ถูกต้อง!",
          text: "วันหมดอายุจะต้องมากกว่าวันที่ประกาศใช้และวันปัจจุบัน",
          icon: "error",
          button: "ตกลง",
        });
      }
    },
    ViewButton() {
      var x = this.checkedItem[0];
      this.$root.api.Master_s_BagSpecSearch({
        data: {
          bagspec_id: x,
        },
        callback: (res) => {
          this.ViewData = res[0]
          if (res[0].tsale == 1) {
            this.ViewData.tsale = "ขายต่างประเทศ";
          } else if (res[0].tsale == 0) {
            this.ViewData.tsale = "ขายในประเทศ";
          }

          this.ViewData.productype =
            res[0].produccttype_code.trim() +
            " : " +
            res[0].producttype_desc.trim();
          this.ViewData.producttype_code = res[0].produccttype_code;
          this.ViewData.label_cd = res[0].label_cd;
          this.ViewData.label_name = res[0].label_name;
          this.ViewData.bag_cd = res[0].bag_cd;
          this.ViewData.bag_name = res[0].bag_name;
          this.ViewData.effdate_st = res[0].effdate_st.split("T")[0];
          this.ViewData.effdate_en = res[0].effdate_en.split("T")[0];
          this.ViewData.stretchingType =
            res[0].stretchingtype_code.trim() +
            " : " +
            res[0].stretchingtype_desc.trim();

          this.ViewData.customer =
            res[0].cus_cod.trim() + " : " + res[0].cus_name.trim();

          this.$root.api.Master_s_BagSearch({
            data: { bag_id: res[0].bag_id },
            callback: (res) => {
              res.forEach((x) => (x._label = GetObjValJoin(x, ["bag_cd"])));
              this.ViewData.path_pic = res[0].path_pic;
              this.openViewModal = true;
            },
          });

        },
      });
    },
    DeleteButton() {
      this.openDeleteModal = true;
      var x = this.checkedItem[0];
      this.$root.api.Master_s_BagSpecSearch({
        data: {
          bagspec_id: x,
        },
        callback: (res) => {
          this.DeleteData = res[0];
        },
      });
    },
    DeleteSaveButton() {
      this.$root.api.Master_s_BagSpecMove({
        data: {
          bagspec_move: [
            {
              bagspec_id: this.DeleteData.bagspec_id,
              user_id: localStorage.getItem("UserID"),
              user_name: localStorage.getItem("User"),
            },
          ],
        },
        callback: (res) => {
          if (res.status == "S") {
            swal({
              title: "สำเร็จ",
              text: "ลบข้อมูลมาตราฐานเงื่อนไข \n การใช้ถุงบรรจุสินค้าเสร็จสิ้น",
              icon: "success",
              button: "ตกลง",
            });
          } else {
            swal({
              title: "ผิดพลาด!",
              text:
                "ไม่สามารถลบข้อมูลมาตราฐานเงื่อนไข \n การใช้ถุงบรรจุสินค้าได้",
              icon: "error",
              button: "ตกลง",
            });
          }
          this.openDeleteModal = false;
          this.SearchData();
        },
      });
    },
    resetCheck() {
      this.checked = [];
      this.checkedItem = "";
      this.checkedSpec = [];
      this.DataSubTable = []
    },
    resetData() {
      this.SaleValue = null;
      this.CustomerOptions.selected = null;
      this.ProductTypeOptions.selected = null;
      this.StretchingTypeOptions.selected = null;
      this.LabelOptions.selected = null;
      this.BagOptions.selected = null;
      this.insertData.effdate_st = this.nowDate;
      this.insertData.effdate_en = "2000-01-01";
    },
    ChangeProductype() {
      this.StretchingTypeOptions.selected = null
      this.EditData.produccttype_code = this.ProductTypeOptions.selected.producttype_code
    },
    FindCustomer() {
      this.$root.api.View_master_s_cutomerSearch({
        data: { tsale: this.SaleValue.value },
        callback: (res) => {
          res.forEach(
            (x) => (x._label = GetObjValJoin(x, ["cuscod", "fname"]))
          );
          this.CustomerOptions.lists = res;
        },
      });
    },
    setPagesActive() {
      this.pagesA = [];
      let numberOfPages = Math.ceil(this.dataActiveCount / this.perPageA);
      for (let index = 0; index < numberOfPages; index++) {
        this.pagesA.push(index);
      }
      this.pageA = 1;
    },
    setPagesInactive() {
      this.pagesI = [];
      let numberOfPages = Math.ceil(this.dataInactiveCount / this.perPageI);
      for (let index = 0; index < numberOfPages; index++) {
        this.pagesI.push(index);
      }
      this.pageI = 1;
    },
    setPagesCancel() {
      this.pagesC = [];
      let numberOfPages = Math.ceil(this.dataCancelCount / this.perPageI);
      for (let index = 0; index < numberOfPages; index++) {
        this.pagesC.push(index);
      }
      this.pageC = 1;
    },
    paginate(Active) {
      let pageA = this.pageA;
      let perPageA = this.perPageA;
      let from = pageA * perPageA - perPageA;
      let to = pageA * perPageA;
      return Active.slice(from, to);
    },
    paginateInactive(Inactive) {
      let pageI = this.pageI;
      let perPageI = this.perPageI;
      let from = pageI * perPageI - perPageI;
      let to = pageI * perPageI;
      return Inactive.slice(from, to);
    },
    paginateCancel(Cancel) {
      let pageC = this.pageC;
      let perPageC = this.perPageC;
      let from = pageC * perPageC - perPageC;
      let to = pageC * perPageC;
      return Cancel.slice(from, to);
    },
  },
  computed: {
    displayedPostsActive() {
      return this.paginate(this.dataActive);
    },
    displayedPostsInactive() {
      return this.paginateInactive(this.dataInactive);
    },
    displayedPostsCancel() {
      return this.paginateCancel(this.dataCancel);
    },
  },
  watch: {
    Active() {
      this.setPagesActive();
    },
    Inactive() {
      this.setPagesInactive();
    },
    Cancel() {
      this.setPagesCancel();
    },
  },
};
