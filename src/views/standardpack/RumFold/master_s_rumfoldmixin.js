import imageMixins from "./imageMixins";
import swal from "sweetalert";
import DatePicker from "vue2-datepicker";
import "vue2-datepicker/index.css";
import { GetObjValJoin, GetDate, FileToBase64 } from "@/shared/utils";
import setupmixin from "@/shared/setupmixin";
import { loadPartialConfig } from "@babel/core";

export default {
  components: {
    DatePicker,
  },
  mixins: [imageMixins],
  data() {
    return {
      HReport: true,
      search_cd: "",
      search_txtsearch: "",

      DataTable: [],
      DataInactiveTable: [],
      DataCancelTable: [],

      // Active
      dataActive: [],
      dataActiveCount: 0,
      pageA: 1,
      perPageA: 10,
      pagesA: [],

      // InActive
      dataInactive: [],
      dataInactiveCount: 0,
      pageI: 1,
      perPageI: 5,
      pagesI: [],

      // Cancel
      dataCancel: [],
      dataCancelCount: 0,
      pageC: 1,
      perPageC: 5,
      pagesC: [],

      checked: [],
      checkedItem: "",

      itemsTabs1: true,
      itemsTabs2: false,
      itemsTabs3: false,

      openAddModal: false,
      openEditModal: false,
      openViewModal: false,
      openDeleteModal: false,

      insertData: {
        fold_code: "",
        fold_name: "",
        fold_information: "",
        effdate_st: "",
        effdate_en: "",
        path_pic: "",
      },
      EditData: {
        fold_id: "",
        fold_code: "",
        fold_name: "",
        fold_information: "",
        effdate_st: "",
        effdate_en: "",
        path_pic: "",
        old_code: "",
      },
      ViewData: {
        fold_code: "",
        fold_name: "",
        fold_information: "",
        effdate_st: "",
        effdate_en: "",
        path_pic: "",
      },
      DeleteData: {},
      nowDate: "",
      old_effdate_st: "",
    };
  },
  updated() {
    if (this.insertData.fold_code == "") {
      $("#fold_code").focus();
    }
  },
  created() {
    let nowDate = new Date();
    this.nowDate = GetDate(nowDate.setDate(nowDate.getDate()));
    this.insertData.effdate_st = this.nowDate;
    this.insertData.effdate_en = "2000-01-01";
  },
  methods: {
    SearchData() {
      this.DataTable = [];
      this.DataCancelTable = [];
      this.dataActiveCount = 0;
      this.dataInactiveCount = 0;
      this.dataCancelCount = 0;
      this.checked = [];
      this.checkedItem = "";

      this.$root.api.Master_s_RumFoldSearchView({
        data: {
          fold_code: this.search_cd,
          txtsearch: this.search_txtsearch,
        },
        callback: (res) => {
          if (res.length == 0) {
            swal({
              title: "ผิดพลาด",
              text: "ไม่พบข้อมูลการพับมัดอวนรุม อวนแปรรูป",
              icon: "warning",
              button: "ตกลง",
            });
          } else {
            this.DataTable = res;
            for (let i = 0; i < res.length; i++) {
              if (res[i].status == "A") {
                this.dataActiveCount = this.dataActiveCount + 1;
              } else if (res[i].status == "I") {
                this.dataInactiveCount = this.dataInactiveCount + 1;
              } else if (res[i].status == "C") {
                this.dataCancelCount = this.dataCancelCount + 1;
              }
            }
            this.GetDataTable();
          }
          this.$root.api.Master_s_RumFoldSearchCancel({
            data: {
              sticker_cd: this.search_cd,
              txtsearch: this.search_txtsearch,
            },
            callback: (res) => {
              this.DataCancelTable = res;
              this.GetCancelDataTable();
            },
          });
        },
      });
    },
    GetDataTable() {
      this.dataActive = [];
      this.dataInactive = [];
      for (let i = 0; i < this.DataTable.length; i++) {
        if (this.DataTable[i].status == "A") {
          this.dataActive.push({
            fold_id: this.DataTable[i].fold_id,
            fold_code: this.DataTable[i].fold_code,
            fold_name: this.DataTable[i].fold_name,
            fold_information: this.DataTable[i].fold_information,
            path_pic: this.DataTable[i].path_pic,
            status: this.DataTable[i].status,
            effdate_st: this.DataTable[i].effdate_st,
            effdate_en: this.DataTable[i].effdate_en,
            user_id: this.DataTable[i].user_id,
            user_name: this.DataTable[i].user_name,
            user_date: this.DataTable[i].user_date,
          });
        } else if (this.DataTable[i].status == "I") {
          this.dataInactive.push({
            fold_id: this.DataTable[i].fold_id,
            fold_code: this.DataTable[i].fold_code,
            fold_name: this.DataTable[i].fold_name,
            fold_information: this.DataTable[i].fold_information,
            path_pic: this.DataTable[i].path_pic,
            status: this.DataTable[i].status,
            effdate_st: this.DataTable[i].effdate_st,
            effdate_en: this.DataTable[i].effdate_en,
            user_id: this.DataTable[i].user_id,
            user_name: this.DataTable[i].user_name,
            user_date: this.DataTable[i].user_date,
          });
        }
      }
    },
    GetCancelDataTable() {
      this.dataCancel = [];
      for (let i = 0; i < this.DataCancelTable.length; i++) {
        this.dataCancel.push({
          fold_id: this.DataCancelTable[i].fold_id,
          fold_code: this.DataCancelTable[i].fold_code,
          fold_name: this.DataCancelTable[i].fold_name,
          fold_information: this.DataCancelTable[i].fold_information,
          path_pic: this.DataCancelTable[i].path_pic,
          status: this.DataCancelTable[i].status,
          effdate_st: this.DataCancelTable[i].effdate_st,
          effdate_en: this.DataCancelTable[i].effdate_en,
          user_id: this.DataCancelTable[i].user_id,
          user_name: this.DataCancelTable[i].user_name,
          user_date: this.DataCancelTable[i].user_date,
          cancel_id: this.DataCancelTable[i].cancel_id,
          cancel_name: this.DataCancelTable[i].cancel_name,
          cancel_date: this.DataCancelTable[i].cancel_date,
        });
      }
    },
    checkItems(val) {
      this.checkedItem = $("input[type=checkbox][name=Choosed]:checked")
        .map(function(_, el) {
          return $(el).val();
        })
        .get();
    },
    CheckDupCode() {
      var Dupvalue = [];
      if (this.openAddModal == true) {
        if (this.insertData.fold_code.length < 2) {
          swal({
            title: "ผิดพลาด!",
            text: "รหัสการพับมัดอวนรุม อวนแปรรูป ต้องมี 2 หลัก",
            icon: "error",
            button: "ตกลง",
          });
          $("#fold_code").focus();
        } else {
          this.$root.api.Master_s_RumFoldSearchView({
            data: {
              fold_code: this.insertData.fold_code,
            },
            callback: (res) => {
              if (res.length != 0) {
                swal({
                  title: "รหัสการพับมัดซ้ำ",
                  text: "กรุณากรอกรหัสการพับมัดอวนรุม อวนแปรรูปใหม่",
                  icon: "warning",
                  button: "ตกลง",
                });
                this.insertData.fold_code = "";
              }
            },
          });
        }
      } else if (this.openEditModal == true) {
        if (this.EditData.fold_code.length < 2) {
          swal({
            title: "ผิดพลาด!",
            text: "รหัสการพับมัดอวนรุม อวนแปรรูปต้องมี 2 หลัก",
            icon: "error",
            button: "ตกลง",
          });
          $("#fold_code_edit").focus();
        } else {
          this.$root.api.Master_s_RumFoldSearchView({
            data: {
              fold_code: this.EditData.fold_code,
            },
            callback: (res) => {
              if (res.length != 0) {
                if (this.EditData.fold_code != this.EditData.old_code) {
                  swal({
                    title: "รหัสการพับมัดซ้ำ",
                    text: "กรุณากรอกรหัสการพับมัดอวนรุม อวนแปรรูปใหม่",
                    icon: "warning",
                    button: "ตกลง",
                  });
                  this.EditData.fold_code = "";
                }
              }
            },
          });
        }
      }
    },
    InsertSaveButton() {
      if (this.insertData.fold_code != "" && this.insertData.fold_name != "") {
        if (
          this.insertData.effdate_en == "2000-01-01" ||
          (this.insertData.effdate_en > this.nowDate &&
            this.insertData.effdate_en > this.insertData.effdate_st)
        ) {
          this.$root.api.Master_s_RumFoldSave({
            data: {
              fold_code: this.insertData.fold_code.toUpperCase(),
              fold_name: this.insertData.fold_name,
              fold_information: this.insertData.fold_information,
              path_pic: this.insertData.path_pic,
              status: "A",
              effdate_st: this.insertData.effdate_st,
              effdate_en: this.insertData.effdate_en,
              user_id: localStorage.getItem("UserID"),
              user_name: localStorage.getItem("User"),
            },
            callback: (res) => {
              if (res.status == "S") {
                swal({
                  title: "สำเร็จ",
                  text: "บันทึกข้อมูลการพับมัดอวนรุม อวนแปรรูป เสร็จสิ้น",
                  icon: "success",
                  button: "ตกลง",
                });
                this.SearchData();
                this.resetData();
              } else if (res.status == "F") {
                swal({
                  title: "ผิดพลาด!",
                  text:
                    "ไม่สามารถบันทึกข้อมูลการพับมัดอวนรุม อวนแปรรูปได้",
                  icon: "error",
                  button: "ตกลง",
                });
                this.resetData();
              }
              this.SearchData();
            },
          });
        } else {
          swal({
            title: "ข้อมูลไม่ถูกต้อง!",
            text: "วันหมดอายุจะต้องมากกว่าวันที่ประกาศใช้และวันปัจจุบัน",
            icon: "error",
            button: "ตกลง",
          });
        }
      } else {
        swal({
          title: "ผิดพลาด",
          text: "กรุณากรอกข้อมูลการพับมัดอวนรุม อวนแปรรูปให้ครบถ้วน",
          icon: "error",
          button: "ตกลง",
        });
      }
    },
    resetCheck() {
        this.checked = [];
        this.checkedItem = "";
    },
    resetData() {
      this.checked = [];
      this.checkedItem = "";
      this.insertData.fold_code = "";
      this.insertData.fold_name = "";
      this.insertData.fold_information = "";
      this.insertData.path_pic = null;
      this.insertData.effdate_st = this.nowDate;
      this.insertData.effdate_en ='2000-01-01';
      this.openDeleteModal = false;
      this.openAddModal = false;
      this.openEditModal = false;

      document.getElementById("filename").value = "";
      document.getElementById("showImage").removeAttribute("src");
      document.getElementById("filename-edit").value = "";
    },
    focus() {
      if (this.EditData.fold_code != "") {
        $("#fold_code_edit").focus();
      }
    },
    EditView() {
      this.openEditModal = true;
      var x = this.checkedItem[0];

      this.$root.api.Master_s_RumFoldSearchView({
        data: {
          fold_id: x,
        },
        callback: (res) => {
          this.EditData.fold_id = res[0].fold_id;
          this.EditData.fold_code = res[0].fold_code;
          this.EditData.fold_name = res[0].fold_name;
          this.EditData.fold_information = res[0].fold_information;
          this.EditData.effdate_st = res[0].effdate_st.split("T")[0];
          this.EditData.effdate_en = res[0].effdate_en.split("T")[0];
          this.EditData.path_pic = res[0].path_pic;
          this.old_effdate_st = res[0].effdate_st.split("T")[0];
          this.EditData.old_code = res[0].fold_code;

          this.focus();
        },
      });
    },
    EditSaveButton() {
      if (
        this.EditData.effdate_en == "2000-01-01" ||
        (this.EditData.effdate_en > this.nowDate &&
          this.EditData.effdate_en > this.EditData.effdate_st)
      ) {
        this.$root.api.Master_s_RumFoldSave({
          data: {
            fold_id: this.EditData.fold_id,
            fold_code: this.EditData.fold_code.toUpperCase(),
            fold_name: this.EditData.fold_name,
            fold_information: this.EditData.fold_information,
            path_pic: this.EditData.path_pic,
            status: "A",
            effdate_st: this.EditData.effdate_st,
            effdate_en: this.EditData.effdate_en,
            user_id: localStorage.getItem("UserID"),
            user_name: localStorage.getItem("User"),
          },
          callback: (res) => {
            if (res.status == "S") {
              swal({
                title: "สำเร็จ",
                text: "บันทึกข้อมูลการพับมัดอวนรุม อวนแปรรูป เสร็จสิ้น",
                icon: "success",
                button: "ตกลง",
              });
              this.resetData();
            } else if (res.status == "F") {
              swal({
                title: "ผิดพลาด!",
                text: "ไม่สามารถบันทึกข้อมูลการพับมัดอวนรุม อวนแปรรูปได้",
                icon: "error",
                button: "ตกลง",
              });
              this.resetData();
            }
            this.SearchData();
          },
        });
      } else {
        swal({
          title: "ข้อมูลไม่ถูกต้อง!",
          text: "วันหมดอายุจะต้องมากกว่าวันที่ประกาศใช้และวันปัจจุบัน",
          icon: "error",
          button: "ตกลง",
        });
      }
    },
    ViewButton() {
      this.openViewModal = true;
      var x = this.checkedItem[0];
      this.$root.api.Master_s_RumFoldSearchView({
        data: {
          fold_id: x,
        },
        callback: (res) => {
          // this.EditData = res;
          this.ViewData.fold_code = res[0].fold_code;
          this.ViewData.fold_name = res[0].fold_name;
          this.ViewData.fold_information = res[0].fold_information;
          this.ViewData.effdate_st = res[0].effdate_st;
          this.ViewData.effdate_en = res[0].effdate_en;
          this.ViewData.path_pic = res[0].path_pic;
        },
      });
    },
    DeleteButton() {
      this.openDeleteModal = true;
      var x = this.checkedItem[0];
      this.$root.api.Master_s_RumFoldSearchView({
        data: {
          fold_id: x,
        },
        callback: (res) => {
          this.DeleteData = res;
        },
      });
    },
    DeleteSaveButton() {
      this.$root.api.Master_s_RumFoldMove({
        data: {
          rumfold_move: [
            {
              fold_id: this.DeleteData[0].fold_id,
              user_id: localStorage.getItem("UserID"),
              user_name: localStorage.getItem("User"),
            },
          ],
        },
        callback: (res) => {
          if (res.status == "S") {
            swal({
              title: "สำเร็จ",
              text: "ลบข้อมูลการพับมัดอวนรุม อวนแปรรูปเสร็จสิ้น",
              icon: "success",
              button: "ตกลง",
            });
            this.SearchData();
          } else {
            swal({
              title: "ผิดพลาด!",
              text:
                "ไม่สามารถลบข้อมูลการพับมัดอวนรุม อวนแปรรูปได้",
              icon: "error",
              button: "ตกลง",
            });
            this.SearchData();
          }
          this.openDeleteModal = false;
          this.SearchData();
        },
      });
    },
    setPagesActive() {
      this.pagesA = [];
      let numberOfPages = Math.ceil(this.dataActiveCount / this.perPageA);
      for (let index = 0; index < numberOfPages; index++) {
        this.pagesA.push(index);
      }
      this.pageA = 1;
    },
    setPagesInactive() {
      this.pagesI = [];
      let numberOfPages = Math.ceil(this.dataInactiveCount / this.perPageI);
      for (let index = 0; index < numberOfPages; index++) {
        this.pagesI.push(index);
      }
      this.pageI = 1;
    },
    setPagesCancel() {
      this.pagesC = [];
      let numberOfPages = Math.ceil(this.dataCStickerSpecCount / this.perPageI);
      for (let index = 0; index < numberOfPages; index++) {
        this.pagesC.push(index);
      }
      this.pageC = 1;
    },
    paginate(Active) {
      let pageA = this.pageA;
      let perPageA = this.perPageA;
      let from = pageA * perPageA - perPageA;
      let to = pageA * perPageA;
      return Active.slice(from, to);
    },
    paginateInactive(Inactive) {
      let pageI = this.pageI;
      let perPageI = this.perPageI;
      let from = pageI * perPageI - perPageI;
      let to = pageI * perPageI;
      return Inactive.slice(from, to);
    },
    paginateCancel(Cancel) {
      let pageC = this.pageC;
      let perPageC = this.perPageC;
      let from = pageC * perPageC - perPageC;
      let to = pageC * perPageC;
      return Cancel.slice(from, to);
    },
  },
  computed: {
    displayedPostsActive() {
      return this.paginate(this.dataActive);
    },
    displayedPostsInactive() {
      return this.paginateInactive(this.dataInactive);
    },
    displayedPostsCancel() {
      return this.paginateCancel(this.dataCancel);
    },
  },
  watch: {
    Active() {
      this.setPagesActive();
    },
    Inactive() {
      this.setPagesInactive();
    },
    Cancel() {
      this.setPagesCancel();
    },
  },
};
