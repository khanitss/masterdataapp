import Multiselect from "vue-multiselect";
import { GetDate, GetObjValJoin } from "@/shared/utils";
import swal from "sweetalert";
import DatePicker from "vue2-datepicker";
import "vue2-datepicker/index.css";

export default {
  name: "rumfoldspec",
  components: {
    Multiselect,
    DatePicker,
  },
  data() {
    return {
      search: {
        fold_code: "",
        rumtype_code: "",
        txtsearch: "",
      },
      HReport: true,

      itemsTabs1: true,
      itemsTabs2: false,
      itemsTabs3: false,

      openAddModal: false,
      openEditModal: false,
      openViewModal: false,
      openDeleteModal: false,

      checked: [],
      checkedItem: "",

      insertData: {
        effdate_st: "",
        effdate_en: "",
      },

      EditData: {
        old_effdate_st: "",
        effdate_st: "",
        effdate_en: "",
        fold_information: "",
        old_rum_cd: "",
        old_fold_cd: "",
        old_fold_id: "",
        path_pic: "",
        rumfoldspec_id: "",
      },
      ViewData: {
        effdate_st: "",
        effdate_en: "",
        fold_information: "",
        old_fold_id: "",
        path_pic: "",
      },

      DeleteData: {},
      nowDate: "",
      DataTable: [],
      DataCancelTable: [],

      // Active
      dataActive: [],
      dataActiveCount: 0,
      pageA: 1,
      perPageA: 10,
      pagesA: [],

      // InActive
      dataInactive: [],
      dataInactiveCount: 0,
      pageI: 1,
      perPageI: 5,
      pagesI: [],

      // Cancel
      dataCancel: [],
      dataCancelCount: 0,
      pageC: 1,
      perPageC: 5,
      pagesC: [],

      RumFoldOptions: {
        lists: [],
        selected: null,
      },
      RumTypeOptions: {
        lists: [],
        selected: null,
      },
    };
  },
  created() {
    let nowDate = new Date();
    this.nowDate = GetDate(nowDate.setDate(nowDate.getDate()));
    this.insertData.effdate_st = this.nowDate;
    this.insertData.effdate_en = "2000-01-01";

    this.$root.api.View_master_v_rumtypeSearch({
      data: {},
      callback: (res) => {
        res.forEach((x) => (x._label = GetObjValJoin(x, ["rumtypeid"])));
        this.RumTypeOptions.lists = res;
      },
    });

    this.$root.api.Master_s_RumFoldSearch({
      data: {},
      callback: (res) => {
        res.forEach((x) => (x._label = GetObjValJoin(x, ["fold_code"])));
        this.RumFoldOptions.lists = res;
      },
    });
  },
  mounted() {},
  updated() {},
  methods: {
    checkItems(val) {
      this.checkedItem = $("input[type=checkbox][name=Choosed]:checked")
        .map(function(_, el) {
          return $(el).val();
        })
        .get();
    },
    SearchData() {
      this.DataTable = [];
      this.DataCancelTable = [];
      this.dataActiveCount = 0;
      this.dataInactiveCount = 0;
      this.dataCancelCount = 0;
      this.checked = [];
      this.checkedItem = "";
      this.$root.api.Master_s_RumFoldSpecSearch({
        data: {
          rumtypeid: this.search.rumtype_code,
          fold_code: this.search.fold_code,
          txtSearch: this.search.txtsearch,
        },
        callback: (res) => {
          this.DataTable = res;
          for (let i = 0; i < res.length; i++) {
            if (res[i].status == "A") {
              this.dataActiveCount = this.dataActiveCount + 1;
            } else if (res[i].status == "I") {
              this.dataInactiveCount = this.dataInactiveCount + 1;
            }
          }
          this.getDataTabel();
        },
      });
      this.$root.api.Master_s_RumFoldSpecSearchCancel({
        data: {
          rumtypeid: this.search.rumtype_cod,
          fold_code: this.search.fold_code,
          txtSearch: this.search.txtsearch,
        },
        callback: (res) => {
          this.DataCancelTable = res;
          for (let i = 0; i < res.length; i++) {
            this.dataCancelCount = this.dataCancelCount + 1;
          }
          this.getDataCancelTable();
        },
      });
    },
    getDataTabel() {
      this.dataActive = [];
      this.dataInactive = [];
      for (let i = 0; i < this.DataTable.length; i++) {
        if (this.DataTable[i].status == "A") {
          this.dataActive.push({
            rumfoldspec_id: this.DataTable[i].rumfoldspec_id,
            fold_id: this.DataTable[i].fold_id,
            fold_code: this.DataTable[i].fold_code,
            fold_name: this.DataTable[i].fold_name,
            rumtypeid: this.DataTable[i].rumtypeid,
            rumtypename: this.DataTable[i].rumtypename,
            effdate_st: this.DataTable[i].effdate_st,
            effdate_en: this.DataTable[i].effdate_en,
            user_name: this.DataTable[i].user_name,
            user_date: this.DataTable[i].user_date,
          });
        } else if (this.DataTable[i].status == "I") {
          this.dataInactive.push({
            rumfoldspec_id: this.DataTable[i].rumfoldspec_id,
            fold_id: this.DataTable[i].fold_id,
            fold_code: this.DataTable[i].fold_code,
            fold_name: this.DataTable[i].fold_name,
            rumtypeid: this.DataTable[i].rumtypeid,
            rumtypename: this.DataTable[i].rumtypename,
            effdate_st: this.DataTable[i].effdate_st,
            effdate_en: this.DataTable[i].effdate_en,
            user_name: this.DataTable[i].user_name,
            user_date: this.DataTable[i].user_date,
          });
        }
      }
    },
    getDataCancelTable() {
      this.dataCancel = [];
      for (let i = 0; i < this.DataCancelTable.length; i++) {
        this.dataCancel.push({
          rumfoldspec_id: this.DataCancelTable[i].rumfoldspec_id,
          fold_id: this.DataCancelTable[i].fold_id,
          fold_code: this.DataCancelTable[i].fold_code,
          fold_name: this.DataCancelTable[i].fold_name,
          rumtypeid: this.DataCancelTable[i].rumtypeid,
          rumtypename: this.DataCancelTable[i].rumtypename,
          effdate_st: this.DataCancelTable[i].effdate_st,
          effdate_en: this.DataCancelTable[i].effdate_en,
          cancel_name: this.DataCancelTable[i].cancel_name,
          cancel_date: this.DataCancelTable[i].cancel_date,
        });
      }
    },
    CheckDupCode() {
      if (
        this.RumTypeOptions.selected != null &&
        this.RumFoldOptions.selected != null
      ) {
        if (this.openAddModal == true) {
          this.$root.api.Master_s_RumFoldSpecSearch({
            data: {
              rumtypeid: this.RumTypeOptions.selected.rumtypeid,
              fold_code: this.RumFoldOptions.selected.fold_code,
            },
            callback: (res) => {
              if (res.length > 0) {
                swal({
                  title: "รหัสซ้ำ ! ",
                  text: "รหัสซ้ำกรุณากรอกข้อมูลใหม่",
                  icon: "error",
                  button: "ตกลง",
                });
                this.RumTypeOptions.selected = null;
                this.RumFoldOptions.selected = null;
              }
            },
          });
        }
      }
    },
    CheckDupCodeEdit() {
      if (
        this.RumFoldOptions.selected.fold_code != this.EditData.old_fold_cd ||
        this.RumTypeOptions.selected.rumtypeid != this.EditData.old_rum_cd
      ) {
        this.$root.api.Master_s_RumFoldSpecSearch({
          data: {
            rumtypeid: this.RumTypeOptions.selected.rumtypeid,
            fold_code: this.RumFoldOptions.selected.fold_code,
          },
          callback: (res) => {
            if (res.length > 0) {
              swal({
                title: "รหัสซ้ำ ! ",
                text: "รหัสซ้ำกรุณากรอกข้อมูลใหม่",
                icon: "error",
                button: "ตกลง",
              });
              this.$root.api.View_master_v_rumtypeSearch({
                data: {
                  rumtypeid: this.EditData.old_rum_cd,
                },
                callback: (res) => {
                  res.forEach(
                    (x) => (x._label = GetObjValJoin(x, ["rumtypeid"]))
                  );
                  this.RumTypeOptions.selected = res[0];
                },
              });

              this.$root.api.Master_s_RumFoldSearch({
                data: {
                  fold_id: this.EditData.old_fold_id,
                },
                callback: (res) => {
                  res.forEach(
                    (x) => (x._label = GetObjValJoin(x, ["fold_code"]))
                  );
                  this.RumFoldOptions.selected = res[0];
                },
              });
            }
          },
        });
      }
    },
    InsertSaveButton() {
      if (
        this.RumFoldOptions.selected != null &&
        this.RumTypeOptions.selected != null
      ) {
        if (
          this.insertData.effdate_en == "2000-01-01" ||
          (this.insertData.effdate_en > this.nowDate &&
            this.insertData.effdate_en > this.insertData.effdate_st)
        ) {
          this.$root.api.Master_s_RumFoldSpecSave({
            data: {
              rumtypeid: this.RumTypeOptions.selected.rumtypeid,
              rumtypename: this.RumTypeOptions.selected.rumtypename,
              fold_id: this.RumFoldOptions.selected.fold_id,
              fold_code: this.RumFoldOptions.selected.fold_code,
              fold_name: this.RumFoldOptions.selected.fold_name,
              status: "A",
              effdate_st: this.insertData.effdate_st,
              effdate_en: this.insertData.effdate_en,
              user_id: localStorage.getItem("UserID"),
              user_name: localStorage.getItem("User"),
            },
            callback: (res) => {
              if (res.status == "S") {
                swal({
                  title: "สำเร็จ",
                  text:
                    "บันทึกข้อมูลมาตรฐานเงื่อนไขการพับมัดของอวนรุม อวนแปรรูป เสร็จสิ้น",
                  icon: "success",
                  button: "ตกลง",
                });
                this.SearchData();
                this.resetData();
              } else if (res.status == "F") {
                swal({
                  title: "ผิดพลาด!",
                  text:
                    "ไม่สามารถบันทึกข้อมูลมาตรฐานเงื่อนไขการพับมัดของอวนรุม อวนแปรรูปได้",
                  icon: "error",
                  button: "ตกลง",
                });
                this.resetData();
              }
              this.openAddModal = false;
            },
          });
        } else {
          swal({
            title: "ข้อมูลไม่ถูกต้อง!",
            text: "วันหมดอายุจะต้องมากกว่าวันที่ประกาศใช้และวันปัจจุบัน",
            icon: "error",
            button: "ตกลง",
          });
        }
      } else {
        swal({
          title: "ผิดพลาด",
          text:
            "กรุณากรอกข้อมูลมาตรฐานเงื่อนไขการพับมัด \n ของอวนรุม อวนแปรรูปให้ครบถ้วน",
          icon: "error",
          button: "ตกลง",
        });
      }
    },
    EditButton() {
      this.openEditModal = true;
      var x = this.checkedItem[0];
      this.$root.api.Master_s_RumFoldSpecSearch({
        data: {
          rumfoldspec_id: x,
        },
        callback: (res) => {
          this.EditData = res[0];
          this.EditData.rumfoldspec_id = res[0].rumfoldspec_id;
          this.EditData.old_effdate_st = res[0].effdate_st.split("T")[0];
          this.EditData.effdate_st = res[0].effdate_st.split("T")[0];
          this.EditData.effdate_en = res[0].effdate_en.split("T")[0];
          this.EditData.old_rum_cd = res[0].rumtypeid;
          this.EditData.old_fold_cd = res[0].fold_code;
          this.EditData.old_fold_id = res[0].fold_id;
          this.$root.api.View_master_v_rumtypeSearch({
            data: {
              rumtypeid: res[0].rumtypeid,
            },
            callback: (res) => {
              res.forEach((x) => (x._label = GetObjValJoin(x, ["rumtypeid"])));
              this.RumTypeOptions.selected = res[0];
            },
          });

          this.$root.api.Master_s_RumFoldSearch({
            data: {
              fold_id: res[0].fold_id,
            },
            callback: (res) => {
              res.forEach((x) => (x._label = GetObjValJoin(x, ["fold_code"])));
              this.RumFoldOptions.selected = res[0];
              this.EditData.fold_information = res[0].fold_information;
              this.EditData.path_pic = res[0].path_pic;
            },
          });
        },
      });
    },
    EditSaveButton() {
      if (
        this.RumFoldOptions.selected != null &&
        this.RumTypeOptions.selected != null
      ) {
        if (
          this.EditData.effdate_en == "2000-01-01" ||
          (this.EditData.effdate_en > this.nowDate &&
            this.EditData.effdate_en > this.EditData.effdate_st)
        ) {
          this.$root.api.Master_s_RumFoldSpecSave({
            data: {
              rumfoldspec_id: this.EditData.rumfoldspec_id,
              rumtypeid: this.RumTypeOptions.selected.rumtypeid,
              rumtypename: this.RumTypeOptions.selected.rumtypename,
              fold_id: this.RumFoldOptions.selected.fold_id,
              fold_code: this.RumFoldOptions.selected.fold_code,
              fold_name: this.RumFoldOptions.selected.fold_name,
              status: "A",
              effdate_st: this.EditData.effdate_st,
              effdate_en: this.EditData.effdate_en,
              user_id: localStorage.getItem("UserID"),
              user_name: localStorage.getItem("User"),
            },
            callback: (res) => {
              if (res.status == "S") {
                swal({
                  title: "สำเร็จ",
                  text:
                    "บันทึกข้อมูลมาตรฐานเงื่อนไขการพับมัดของอวนรุม อวนแปรรูปเสร็จสิ้น",
                  icon: "success",
                  button: "ตกลง",
                });
                this.SearchData();
                this.resetData();
              } else if (res.status == "F") {
                swal({
                  title: "ผิดพลาด!",
                  text:
                    "ไม่สามารถบันทึกข้อมูลมาตรฐานเงื่อนไขการพับมัดของอวนรุม อวนแปรรูปได้",
                  icon: "error",
                  button: "ตกลง",
                });
                this.resetData();
              }
              this.openEditModal = false;
            },
          });
        } else {
          swal({
            title: "ข้อมูลไม่ถูกต้อง!",
            text: "วันหมดอายุจะต้องมากกว่าวันที่ประกาศใช้และวันปัจจุบัน",
            icon: "error",
            button: "ตกลง",
          });
        }
      } else {
        swal({
          title: "ผิดพลาด",
          text:
            "กรุณากรอกข้อมูลมาตรฐานเงื่อนไขการพับมัด \n ของอวนรุม อวนแปรรูปให้ครบถ้วน",
          icon: "error",
          button: "ตกลง",
        });
      }
    },
    ViewButton() {
      this.openViewModal = true;
      var x = this.checkedItem[0];
      if (this.itemsTabs3 == false) {
        this.$root.api.Master_s_RumFoldSpecSearch({
          data: {
            rumfoldspec_id: x,
          },
          callback: (res) => {
            this.ViewData = res[0];
            this.ViewData.effdate_st = res[0].effdate_st.split("T")[0];
            this.ViewData.effdate_en = res[0].effdate_en.split("T")[0];
            this.ViewData.old_fold_id = res[0].fold_id;
            this.$root.api.Master_s_RumFoldSearch({
              data: {
                fold_id: res[0].fold_id,
              },
              callback: (res) => {
                this.RumFoldOptions.selected = res[0];
                this.ViewData.fold_information = res[0].fold_information;
                this.ViewData.path_pic = res[0].path_pic;
              },
            });
          },
        });
      } else if (this.itemsTabs3 == true) {
        this.$root.api.Master_s_RumFoldSpecSearchCancel({
          data: {
            rumfoldspec_id: x,
          },
          callback: (res) => {
            this.ViewData = res[0];
            this.ViewData.effdate_st = res[0].effdate_st.split("T")[0];
            this.ViewData.effdate_en = res[0].effdate_en.split("T")[0];
            this.ViewData.old_fold_id = res[0].fold_id;
            this.$root.api.Master_s_RumFoldSearch({
              data: {
                fold_id: res[0].fold_id,
              },
              callback: (res) => {
                this.RumFoldOptions.selected = res[0];
                this.ViewData.fold_information = res[0].fold_information;
                this.ViewData.path_pic = res[0].path_pic;
              },
            });
          },
        });
      }
    },
    DeleteButton() {
      this.openDeleteModal = true;
      var x = this.checkedItem[0];
      this.$root.api.Master_s_RumFoldSpecSearch({
        data: {
          rumfoldspec_id: x,
        },
        callback: (res) => {
          this.DeleteData = res[0];
        },
      });
    },
    DeleteSaveButton() {
      this.$root.api.Master_s_RumFoldSpecMove({
        data: {
          rumfoldspec_move: [
            {
              rumfoldspec_id: this.DeleteData.rumfoldspec_id,
              user_id: localStorage.getItem("UserID"),
              user_name: localStorage.getItem("User"),
            },
          ],
        },
        callback: (res) => {
          if (res.status == "S") {
            swal({
              title: "สำเร็จ",
              text:
                "ลบข้อมูลมาตรฐานเงื่อนไขการพับมัดของอวนรุม อวนแปรรูปเสร็จสิ้น",
              icon: "success",
              button: "ตกลง",
            });
            this.SearchData();
          } else {
            swal({
              title: "ผิดพลาด!",
              text:
                "ไม่สามารถลบข้อมูลมาตรฐานเงื่อนไขการพับมัดของอวนรุม อวนแปรรูปได้",
              icon: "error",
              button: "ตกลง",
            });
            this.SearchData();
          }
          this.openDeleteModal = false;
          this.SearchData();
        },
      });
    },
    resetData() {
      this.RumFoldOptions.selected = null;
      this.RumTypeOptions.selected = null;
      this.insertData.effdate_st = this.nowDate;
      this.insertData.effdate_en = "2000-01-01";
      this.checked = [];
      this.checkedItem = "";
    },
    setPagesActive() {
      this.pagesA = [];
      let numberOfPages = Math.ceil(this.dataActiveCount / this.perPageA);
      for (let index = 0; index < numberOfPages; index++) {
        this.pagesA.push(index);
      }
      this.pageA = 1;
    },
    setPagesInactive() {
      this.pagesI = [];
      let numberOfPages = Math.ceil(this.dataInactiveCount / this.perPageI);
      for (let index = 0; index < numberOfPages; index++) {
        this.pagesI.push(index);
      }
      this.pageI = 1;
    },
    setPagesCancel() {
      this.pagesC = [];
      let numberOfPages = Math.ceil(this.dataCStickerSpecCount / this.perPageI);
      for (let index = 0; index < numberOfPages; index++) {
        this.pagesC.push(index);
      }
      this.pageC = 1;
    },
    paginate(Active) {
      let pageA = this.pageA;
      let perPageA = this.perPageA;
      let from = pageA * perPageA - perPageA;
      let to = pageA * perPageA;
      return Active.slice(from, to);
    },
    paginateInactive(Inactive) {
      let pageI = this.pageI;
      let perPageI = this.perPageI;
      let from = pageI * perPageI - perPageI;
      let to = pageI * perPageI;
      return Inactive.slice(from, to);
    },
    paginateCancel(Cancel) {
      let pageC = this.pageC;
      let perPageC = this.perPageC;
      let from = pageC * perPageC - perPageC;
      let to = pageC * perPageC;
      return Cancel.slice(from, to);
    },
  },
  computed: {
    displayedPostsActive() {
      return this.paginate(this.dataActive);
    },
    displayedPostsInactive() {
      return this.paginateInactive(this.dataInactive);
    },
    displayedPostsCancel() {
      return this.paginateCancel(this.dataCancel);
    },
  },
  watch: {
    Active() {
      this.setPagesActive();
    },
    Inactive() {
      this.setPagesInactive();
    },
    Cancel() {
      this.setPagesCancel();
    },
  },
};
