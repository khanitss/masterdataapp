import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

const state = {
  DataSet: [],
  DataSetInactive: [],
  DataSetCancel: [],
  dataActiveCount : 0,
  dataInactiveCount : 0,
  dataCancelCount : 0,
  HideShowBtnVal : true
}


const getters = {
  DataSets: state => state.DataSet,
  DataSetInactives: state => state.DataSetInactive,
  DataSetCancels: state => state.DataSetCancel,
  dataActiveCounts: state => state.dataActiveCount,
  dataInactiveCounts: state => state.dataInactiveCount,
  dataCancelCounts: state => state.dataCancelCount, 
  HideShowBtn: state => state.HideShowBtnVal
}

const actions = {
  addDataSet: (state, data) => {
    store.commit('setDataSet', data)
  },
  addDataSetinactive : (state, data) => {
    store.commit('setDataSetInactive', data)
  },
  addDataSetCancel : (state, data) => {
    store.commit('setDataSetCancel', data)
  },
  addDataActiveCount: (state, data) => {
    store.commit('setActiveCount', data)
  },
  addDataInctiveCount: (state, data) => {
    store.commit('setInactiveCount', data)
  },
  addDataCancelCount: (state, data) => {
    store.commit('setCancelCount', data)
  },
  changeHideShowBtn: (state, data) => {
    store.commit('setHideShowBtnVal', data)
  }
}


const mutations = {
  setDataSet: (state, data) => {
    state.DataSet = data
  },
  setDataSetInactive: (state, data) => {
    state.DataSetInactive = data
  },
  setDataSetCancel: (state, data) => {
    state.DataSetCancel = data
  },
  setHideShowBtnVal: (state, data) => {
    state.HideShowBtnVal = data
  },
  setActiveCount: (state,data) => {
    state.dataActiveCount = data
  },
  setInactiveCount: (state,data) => {
    state.dataInactiveCount = data
  },
  setCancelCount: (state,data) => {
    state.dataCancelCount = data
  }

}

let store = new Vuex.Store({
  state: state,
  mutations: mutations,
  getters: getters,
  actions: actions
})

export default store