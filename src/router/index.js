import Vue from 'vue'
import Router from 'vue-router'

// Containers
const TheContainer = () => import('@/containers/TheContainer')

// Views
const Dashboard = () => import('@/views/Dashboard')

// Standardpack
const Label = () => import('@/views/standardpack/Label/master_s_label')
const LabelSpec = () => import('@/views/standardpack/LabelSpec/master_s_labelspec')
const LineLabel = () => import('@/views/standardpack/LineLabel/master_s_linelabel')
const Canvas = () => import('@/views/standardpack/Canvas/master_s_canvas')
const Barcolor = () => import('@/views/standardpack/Barcolor/master_s_barcolor')
const CanvasnBarcolor = () => import('@/views/standardpack/CanvasBar/master_s_canvasnbar')
const CanvasnBarSpec = () => import('@/views/standardpack/CanvasBarSpec/master_s_canvasnbarspec')
const Strap = () => import('@/views/standardpack/Strap/master_s_strap')
const StrapSpec = () => import('@/views/standardpack/StapSpec/master_s_strapspec')
const Sticker = () => import('@/views/standardpack/Sticker/master_s_sticker')
const StickerSpec = () => import('@/views/standardpack/StickerSpec/master_s_stickerspec')
const Fold = () => import('@/views/standardpack/Fold/master_s_fold')
const FoldSpec = () => import('@/views/standardpack/FoldSpec/master_s_foldspec')
const RumFold = () => import('@/views/standardpack/RumFold/master_s_rumfold')
const RumFoldSpec = () => import('@/views/standardpack/RumFoldSpec/master_s_rumfoldspec')
const Bag = () => import('@/views/standardpack/Bag/master_s_bag')
const BagSpec = () => import('@/views/standardpack/BagSpec/master_s_bagspec')
const Rope = () => import('@/views/standardpack/Rope/master_s_rope')
const RopeSpec = () => import('@/views/standardpack/RopeSpec/master_s_ropespec')

// Views -> member
const Login = () => import('@/views/member/Login')


Vue.use(Router)

export default new Router({
  mode: 'history', // https://router.vuejs.org/api/#mode
  linkActiveClass: 'open active',
  scrollBehavior: () => ({ y: 0 }),
  routes: [
    {
      path: '',
      name: 'Home',
      redirect: 'dashboard',
      component: TheContainer,
      children: [
        {
          path: 'dashboard',
          name: 'Dashboard',
          component: Dashboard
        },
      ]
    },
    {
      path: '/standardpack',
      redirect: '/standardpack/master_s_label',
      name: 'Standardpack',
      component: TheContainer,
      children: [
        {
          path: 'master_s_label',
          name: 'Label',
          component: Label
        },
        {
          path: 'master_s_labelspec',
          name: 'LabelSpec',
          component: LabelSpec
        },
        {
          path: 'master_s_linelabel',
          name: 'LineLabel',
          component: LineLabel
        },
        {
          path: 'master_s_canvas',
          name: 'Canvas',
          component: Canvas
        },
        {
          path: 'master_s_barcolor',
          name: 'Barcolor',
          component: Barcolor
        },
        {
          path: 'master_s_canvasnbar',
          name: 'CanvasandBarcolor',
          component: CanvasnBarcolor
        },
        {
          path: 'master_s_canvasnbarspec',
          name: 'CanvasandBarcolorSpec',
          component: CanvasnBarSpec
        },
        {
          path: 'master_s_strap',
          name: 'Strap',
          component: Strap
        },
        {
          path: 'master_s_strapspec',
          name: 'StrapSpec',
          component: StrapSpec
        },
        {
          path: 'master_s_sticker',
          name: 'Sticker',
          component: Sticker
        },
        {
          path: 'master_s_stickerspec',
          name: 'StickerSpec',
          component: StickerSpec
        },
        {
          path: 'master_s_fold',
          name: 'Fold',
          component: Fold
        },
        {
          path: 'master_s_foldspec',
          name: 'FoldSpec',
          component: FoldSpec
        },
        {
          path: 'master_s_rumfold',
          name: 'RumFold',
          component: RumFold
        },
        {
          path: 'master_s_rumfoldspec',
          name: 'RumFoldSpec',
          component: RumFoldSpec
        },
        {
          path: 'master_s_bag',
          name: 'Bag',
          component: Bag
        },
        {
          path: 'master_s_bagspec',
          name: 'BagSpec',
          component: BagSpec
        },
        {
          path: 'master_s_rope',
          name: 'Rope',
          component: Rope
        },
        {
          path: 'master_s_ropespec',
          name: 'RopeSpec',
          component: RopeSpec
        },
      ]
    },
    {
      path: '/user',
      redirect: '/user/login',
      component: {
        render(c) { return c('router-view') }
      },
      children: [
        {
          path: 'login',
          name: 'Login',
          component: Login
        },
      ]
    }
  ]
})


