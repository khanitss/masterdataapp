export default [
  {
    _name: 'CSidebarNav',
    _children: [
      {
        _name: 'CSidebarNavItem',
        name: 'หน้าหลัก',
        to: '/dashboard',
        icon: 'cil-home',
        // badge: {
        //   color: 'primary',
        //   text: 'NEW'
        // }
      },
      {
        _name: 'CSidebarNavDropdown',
        name: 'มตฐ.การบรรจุสินค้า',
        icon: 'cil-inbox',
        show: true,
        _children: [
          {
            _name: 'CSidebarNavDropdown',
            style: '',
            name: '1 ป้ายสินค้า',
            items: [
              {
                name: '1.1 ข้อมูลป้ายสินค้า',
                to: '/standardpack/master_s_label',
                class: 'pl-3'
              },
              {
                name: '1.2 มตฐ.เงื่อนไขการนำไปใช้',
                to: '/standardpack/master_s_labelspec',
                class: 'ml-3'
              },
              {
                name: '1.3 รายละเอียดการพิมพ์ในป้าย',
                to: '/standardpack/master_s_linelabel',
                class: 'ml-3'
              },
            ]
          },
          {
            _name: 'CSidebarNavDropdown',
            name: '2 ผ้าใบและแถบสี',
            items: [
              {
                name: '2.1 ข้อมูลผ้าใบ',
                to: '/standardpack/master_s_canvas',
                class: 'ml-3'
              },
              {
                name: '2.2 ข้อมูลแถบสี',
                to: '/standardpack/master_s_barcolor',
                class: 'ml-3'
              },
              {
                name: '2.3 ความสัมพันธ์ผ้าใบและแถบสี',
                to: '/standardpack/master_s_canvasnbar',
                class: 'ml-3'
              },
              {
                name: '2.4 มตฐ.เงื่อนไขการนำไปใช้',
                to: '/standardpack/master_s_canvasnbarspec',
                class: 'ml-3'
              },
            ]
          },
          {
            _name: 'CSidebarNavDropdown',
            name: '3 สายรัดกระสอบ',
            items: [
              {
                name: '3.1 ข้อมูลสายรัดกระสอบ',
                to: '/standardpack/master_s_strap',
                class: 'ml-3'
              },
              {
                name: '3.2 มตฐ.เงื่อนไขการนำไปใช้',
                to: '/standardpack/master_s_strapspec',
                class: 'ml-3'
              }
            ]
          },
          {
            _name: 'CSidebarNavDropdown',
            name: '4 STICKER',
            items: [
              {
                name: '4.1 ข้อมูล Sticker',
                to: '/standardpack/master_s_sticker',
                class: 'ml-3'
              },
              {
                name: '4.2 มตฐ.เงื่อนไขการนำไปใช้',
                to: '/standardpack/master_s_stickerspec',
                class: 'ml-3'
              }
            ]
          },
          {
            _name: 'CSidebarNavDropdown',
            name: '5 การพับมัดอวนโมโน ไนล่อน โพลี',
            items: [
              {
                name : '5.1 ข้อมูลการพับมัดอวนโมโน ไนล่อน โพลี',
                to: '/standardpack/master_s_fold',
                class: 'ml-3'
              },
              {
                name: '5.2 มตฐ.เงื่อนไขการนำไปใช้',
                to: '/standardpack/master_s_foldspec',
                class: 'ml-3'
              }
            ]
          } ,
          {
            _name: 'CSidebarNavDropdown',
            name: '6 การพับมัดของอวนรุม',
            items: [
              {
                name : '6.1 ข้อมูลการพับมัดของอวนรุม',
                to: '/standardpack/master_s_rumfold',
                class: 'ml-3'
              },
              {
                name: '6.2 มตฐ.เงื่อนไขการนำไปใช้',
                to: '/standardpack/master_s_rumfoldspec',
                class: 'ml-3'
              }
            ]
          },
          {
            _name: 'CSidebarNavDropdown',
            name: '7 ถุงบรรจุสินค้า',
            items: [
              {
                name : '7.1 ข้อมูลถุงบรรจุสินค้า',
                to: '/standardpack/master_s_bag',
                class: 'ml-3'
              },
              {
                name: '7.2 มตฐ.เงื่อนไขการนำไปใช้',
                to: '/standardpack/master_s_bagspec',
                class: 'ml-3'
              }
            ]
          },
          {
            _name: 'CSidebarNavDropdown',
            name: '8 เชือกและฟาง',
            items: [
              {
                name : '8.1 ข้อมูลถุงเชือกและฟาง',
                to: '/standardpack/master_s_rope',
                class: 'ml-3'
              },
              {
                name: '8.2 มตฐ.เงื่อนไขการนำไปใช้',
                to: '/standardpack/master_s_ropespec',
                class: 'ml-3'
              }
            ]
          }
        ]
      },
    ]
  }
]
//  ! nav แบบเก่า
  // {
  //   _name: 'CSidebarNavTitle',
  //   _children: ['สายรัดกระสอบ']
  // },
  // {
  //   _name: 'CSidebarNavItem',
  //   name: 'การบันทึก/แก้ไขข้อมูลลักษณะสายรัดกระสอบ',
  //   to: '/standardpack/master_s_strap',
  //   icon: 'cilStream'
  // },
  // {
  //   _name: 'CSidebarNavItem',
  //   name: 'การกำหนดมาตรฐานเงื่อนไขการใช้สายรัดห่อสินค้า',
  //   to: '/standardpack/master_s_strapspec',
  //   icon: 'cilSettings'
  // },