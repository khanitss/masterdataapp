const PROTOCAL = 'http://'
const DOMAIN = 'localhost'
const PORT = '44345'
const API = `${PROTOCAL}${DOMAIN}:${PORT}`

module.exports = {
  API
}
